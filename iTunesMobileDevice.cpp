/********************************************************************
	created:	2015/09/23
	created:	23:9:2015   18:10
	filename: 	J:\Works\iDeviceUtilCore\iDeviceUtilCore\iTunesMobileDevice.cpp
	file path:	J:\Works\iDeviceUtilCore\iDeviceUtilCore
	file base:	iTunesMobileDevice
	file ext:	cpp
	author:		Jeffery Zhang
	
	purpose:	iTuneMobileDevice.dll interface
*********************************************************************/
#include "stdafx.h"
#include "iDeviceUtil.h"
#include "iTunesMobileDevice.h"

_iTunesMobileDevice_t g_iTunesMobileDevice;

//#define ASSERT_OR_EXIT(_cnd_, ...) do { if(!(_cnd_)) { fprintf(stderr, __VA_ARGS__); g_iTunesMobileDevice.unregister_device_notification(1); } }

DWORD WINAPI capture_log(LPVOID lpParameter)
{
	int err=0;
	void* service=NULL;
	char buf[256];
	err=g_iTunesMobileDevice.AMDeviceConnect(lpParameter);
	if (err==0)
	{
		err=g_iTunesMobileDevice.AMDeviceValidatePairing(lpParameter);
		if (err==0)
		{
			err=g_iTunesMobileDevice.AMDeviceStartSession(lpParameter);
			if (err==0)
			{
				err=g_iTunesMobileDevice.AMDeviceSecureStartService(lpParameter,__CFStringMakeConstantString("com.apple.syslog_relay"), 0, &service);
				g_iTunesMobileDevice.AMDeviceStopSession(lpParameter);
			}
		}
		err=g_iTunesMobileDevice.AMDeviceDisconnect(lpParameter);
	}
	while (service!=NULL)
	{
		ZeroMemory(buf,sizeof(buf));
		if (g_iTunesMobileDevice.AMDServiceConnectionReceive(service, buf, sizeof(buf)-8)>0)
		{
			logIt("%s", buf);
		}
	}
	return 0;
}

void handle_status(int operation, int progress)
{
	char buf[MAX_PATH]={0};
	if (operation==13)
	{
		// start flash main DMG
	}
	else if (operation==205)
	{
		// device gone, wait for device come back.
		logIt("[operation]: device gone, wait for device come back.\n");
	}
	else if (operation==208)
	{
		// restore started.

	}
	else
	{

	}
}

void __cdecl progressCallback(void* device, CFDictionaryRef data, void *arg) 
{
	BOOL done=FALSE;
	if (data!=NULL)
	{
		CFStringRef status = (CFStringRef)CFDictionaryGetValue(data, CFSTR("Status"));
		CFNumberRef progress = (CFNumberRef )CFDictionaryGetValue(data, CFSTR("Progress"));
		CFNumberRef operation = (CFNumberRef )CFDictionaryGetValue(data, CFSTR("Operation"));
		char buf[1024];
		ZeroMemory(buf, sizeof(buf));
		if (status!=NULL)
		{
			CFStringGetCString(status, buf, 1000, kCFStringEncodingUTF8);
		}
		else
		{
			strcpy_s(buf,"No Status!");
		}
		int p=-1,o=-1;
		if (progress!=NULL)
		{
			CFNumberGetValue(progress, kCFNumberSInt32Type, &p);
		}
		if (operation!=NULL)
		{
			CFNumberGetValue(operation, kCFNumberSInt32Type, &o);
		}
		logIt("status=%s, progress=%d, op=%d\n", buf, p,o);
		handle_status(o,p);

		if (_stricmp(buf,"Successful")==0)
		{
			done=TRUE;
		}
		else if (_stricmp(buf,"Failed")==0)
		{
			CFErrorRef error = (CFErrorRef) CFDictionaryGetValue(data, CFSTR("Error"));
			if (error!=NULL)
			{
				//fd_CFShow(error);
				g_args.error_code=CFErrorGetCode(error);
				CFStringRef s=CFErrorCopyDescription(error);
				CFStringGetCString(s, buf, 1000, kCFStringEncodingUTF8);
				logIt("[Error]: %s\n", buf);
				char* p = strchr(buf,':');
				if (p!=NULL)
				{
					p++;
					g_args.error_code=atoi(p);
				}				
				CF_RELEASE_CLEAR(s);
			}
			done=TRUE;
		}
	}
}

DWORD WINAPI restorable_device_restore(LPVOID lpParameter)
{
	CFMutableDictionaryRef dict=g_iTunesMobileDevice.AMRestorableDeviceCopyDefaultRestoreOptions();
	if (dict!=NULL)
	{
		CFDictionarySetValue(dict,__CFStringMakeConstantString("WaitForDeviceConnectionToFinishStateMachine"), g_iTunesMobileDevice.my_kCFBooleanFalse);
		CFDictionarySetValue(dict,__CFStringMakeConstantString("AuthInstallRestoreBehavior"), __CFStringMakeConstantString("Erase"));
		//CFDictionarySetValue(dict,__CFStringMakeConstantString("RestoreBundlePath"), __CFStringMakeConstantString(g_args.ipsw_folder));
		CFDictionarySetValue(dict,__CFStringMakeConstantString("UserLocale"), __CFStringMakeConstantString("en_US"));
		void* err=g_iTunesMobileDevice.AMRestorableDeviceRestore(lpParameter, dict, progressCallback, (void*) 0xdeadbeef);
		//if (err!=NULL)
		//{
		//	int error_code=CFErrorGetCode(err);
		//	if (error_code!=0)
		//	{
		//		g_args.error_code=error_code;
		//		SetEvent(g_args.done_event);
		//	}
		//}
	}
	return 0;
}

void __cdecl progressCallback_v1(void* device, int a2, int a3) 
{
	logIt("a1=%x, a2=%d, a3=%d\n", device, a2,a3);

}

void eventHandler(void* _device, int type, void *arg)
{// type 0 is connected, type 1 is disconnected
#define DEV_CONNECTED		0
#define DEV_DISCONNECTED	1
#define DEV_STATE_DFUMODE		1
#define DEV_STATE_RECOVERY		2
#define DEV_STATE_RESTORABLE	3
#define DEV_STATE_NORMAL		4

	logIt("eventHandler ++ ");
	if (type==DEV_CONNECTED)
	{
		// connect
		int device_state=g_iTunesMobileDevice.AMRestorableDeviceGetState(_device);
		switch(device_state)
		{
		case DEV_STATE_DFUMODE:// Dfu mode
			break;
		case DEV_STATE_RECOVERY:
			{
				TCHAR buffer[MAX_PATH]={0};
				CFStringRef srnm=g_iTunesMobileDevice.AMRestorableDeviceCopySerialNumber(_device);
				AMRecoveryModeDeviceRef device = g_iTunesMobileDevice.AMRestorableDeviceCopyRecoveryModeDevice(_device);
				if (srnm!=NULL)
				{
					CFStringGetCString(srnm, buffer, MAX_PATH, kCFStringEncodingUTF8);
					logIt(buffer);
					if (device!=NULL)
					{
						g_args.deivces_recovery.add_device(buffer, device);
					}
				}
			}
			break;
		case DEV_STATE_RESTORABLE:
			{
				TCHAR buffer[MAX_PATH]={0};
				CFStringRef srnm=g_iTunesMobileDevice.AMRestorableDeviceCopySerialNumber(_device);
				AMRestoreModeDeviceRef device=g_iTunesMobileDevice.AMRestorableDeviceCopyRestoreModeDevice(_device);
				if (srnm!=NULL)
				{
					CFStringGetCString(srnm, buffer, MAX_PATH, kCFStringEncodingUTF8);
					logIt(buffer);
					if (device!=NULL)
					{
						g_args.deivces_restore.add_device(buffer, device);
					}
				}
			}
			break;
		case DEV_STATE_NORMAL:
			{
				char _udid[MAX_PATH];
				ZeroMemory(_udid,sizeof(_udid));
				void* device=g_iTunesMobileDevice.AMRestorableDeviceCopyAMDevice(_device);
				if (device!=NULL)
				{
					CFStringRef udid=g_iTunesMobileDevice.AMDeviceCopyDeviceIdentifier(device);
					if(udid!=NULL)
					{
						CFStringGetCString(udid, _udid, 128, kCFStringEncodingUTF8);
						g_args.deivces.add_device(_udid, device);
						CF_RELEASE_CLEAR(udid);
					}
				}
			}
			break;
		default:
			break;
		}
	}
	else if (type==DEV_DISCONNECTED)
	{
		// disconnect
		int device_state=g_iTunesMobileDevice.AMRestorableDeviceGetState(_device);
		switch(device_state)
		{
		case DEV_STATE_DFUMODE:// Dfu mode
			break;
		case DEV_STATE_RECOVERY:
			{
				TCHAR buffer[MAX_PATH]={0};
				CFStringRef srnm=g_iTunesMobileDevice.AMRestorableDeviceCopySerialNumber(_device);
				if (srnm!=NULL)
				{
					CFStringGetCString(srnm, buffer, MAX_PATH, kCFStringEncodingUTF8);
					//g_args.deivces_recovery.remove_device(buffer);
				}
			}
			break;
		case DEV_STATE_RESTORABLE:
			{
				TCHAR buffer[MAX_PATH]={0};
				CFStringRef srnm=g_iTunesMobileDevice.AMRestorableDeviceCopySerialNumber(_device);
				if (srnm!=NULL)
				{
					CFStringGetCString(srnm, buffer, MAX_PATH, kCFStringEncodingUTF8);
					//g_args.deivces_restore.remove_device(buffer);
				}
			}
			break;
		case DEV_STATE_NORMAL:
			{
				char _udid[MAX_PATH];
				ZeroMemory(_udid,sizeof(_udid));
				HANDLE device=g_iTunesMobileDevice.AMRestorableDeviceCopyAMDevice(_device);
				if (device!=NULL)
				{
					CFStringRef udid=g_iTunesMobileDevice.AMDeviceCopyDeviceIdentifier(device);
					if(udid!=NULL)
					{
						CFStringGetCString(udid, _udid, 128, kCFStringEncodingUTF8);
						//g_args.deivces.remove_device(_udid);
						CF_RELEASE_CLEAR(udid);
					}
				}
			}
			break;
		default:
			break;
		}
	}
	else
	{
		// ??
	}
}

void RecoveryConnectedCallBack(AMRecoveryModeDeviceRef device, void* ctx)
{
	TCHAR buffer[MAX_PATH]={0};
	_sntprintf_s(buffer, MAX_PATH, _T("%016llx"), g_iTunesMobileDevice.AMRecoveryModeDeviceGetECID(device));
	logIt(buffer);
}

void DFUModeConnectedCallBack(AMDFUModeDeviceRef device, void* ctx)
{
	TCHAR buffer[MAX_PATH]={0};
	_sntprintf_s(buffer, MAX_PATH, _T("%016llx"), g_iTunesMobileDevice.AMDFUModeDeviceGetECID(device));
	logIt(buffer);
}

void DFUModeDisConnectedCallBack(AMDFUModeDeviceRef device, void* ctx)
{
	TCHAR buffer[MAX_PATH]={0};
	_sntprintf_s(buffer, MAX_PATH, _T("%016llx"), g_iTunesMobileDevice.AMDFUModeDeviceGetECID(device));
	logIt(buffer);
}

void RecoveryDisConnectedCallBack(AMRecoveryModeDeviceRef device, void* ctx)
{
	TCHAR buffer[MAX_PATH]={0};
	_sntprintf_s(buffer, MAX_PATH, _T("%016llx"), g_iTunesMobileDevice.AMRecoveryModeDeviceGetECID(device));
	logIt(buffer);
}

//void deviceNotificationCallback(AMDeviceNotificationCallbackInformationRef info, void *user)
//{
//	CFStringRef udid;
//	PARAMETERS_T *pThis = (PARAMETERS_T  *)user;
//	char c_value[512];
//	switch (info->msgType)
//	{
//	case ADNCI_MSG_CONNECTED:
//		{
//			udid=g_iTunesMobileDevice.AMDeviceCopyDeviceIdentifier(info->deviceHandle);
//			CFStringGetCString(udid, c_value, 510, kCFStringEncodingUTF8);
//			//pThis->deivces.add_device(c_value, info->deviceHandle);
//			if(pThis->hEevntFound != NULL && pThis->device_udid.compare(c_value) == 0)
//			{
//				SetEvent(pThis->hEevntFound);
//			}
//			//pThis->deivces.PrintList();
//		}
//		break;
//	case ADNCI_MSG_DISCONNECTED:
//		{
//			udid=g_iTunesMobileDevice.AMDeviceCopyDeviceIdentifier(info->deviceHandle);
//			CFStringGetCString(udid, c_value, 510, kCFStringEncodingUTF8);
//			//pThis->deivces.remove_device(c_value);
//		}
//		break;
//	case ADNCI_MSG_UNSUBSCRIBED:// 驱动被卸载时会走到这里 （该信号值未完全确定是代表Itunes环境被卸载）
//		break;
//	default:break;
//	}
//}


BOOL RestorableDevice_Register_CallBack()
{
	BOOL ret = TRUE;
	CFErrorRef err = NULL;
	ENTER_FUNC_BEGIN;

	if (g_iTunesMobileDevice.AMRestorableDeviceRegisterForNotificationsForDevices) {
		g_iTunesMobileDevice._AMRestorableDeviceRegisterForNotificationsId = g_iTunesMobileDevice.AMRestorableDeviceRegisterForNotificationsForDevices(eventHandler, (void *)&g_args, 79, &err);
		if (err == NULL)
			return ret;
		else {
			CFIndex i = CFErrorGetCode(err);
			g_args.error_code = i;
			//CFStringRef desc=(err);
			ret = FALSE;
		}
	}


	g_iTunesMobileDevice._AMRestorableDeviceRegisterForNotificationsId = g_iTunesMobileDevice.AMRestorableDeviceRegisterForNotifications(eventHandler, (void *)&g_args, &err);
	if (err != NULL)
	{
		CFIndex i = CFErrorGetCode(err);
		g_args.error_code = i;
		//CFStringRef desc=CFErrorCopyDescription(err);
		ret = FALSE;
	}
	//logIt("AMRestorableDeviceRegisterForNotifications: -- ret=%d\n", g_iTunesMobileDevice._AMRestorableDeviceRegisterForNotificationsId);
	ENTER_FUNC_ENDRET(g_iTunesMobileDevice._AMRestorableDeviceRegisterForNotificationsId);
	return ret;
}

void uninit_iTunesMobileDevice()
{
	if (g_iTunesMobileDevice._notification!=NULL)
	{
		g_iTunesMobileDevice.AMDeviceNotificationUnsubscribe(g_iTunesMobileDevice._notification);
	}
	if (g_iTunesMobileDevice._AMRestorableDeviceRegisterForNotificationsId>0)
	{
		g_iTunesMobileDevice.AMRestorableDeviceUnregisterForNotifications(g_iTunesMobileDevice._AMRestorableDeviceRegisterForNotificationsId);
	}

	if (g_iTunesMobileDevice.hDll)
	{
		FreeLibrary(g_iTunesMobileDevice.hDll);
	}
}
BOOL init_iTunesMobileDevice(AMModeNotifications notification)
{
	ENTER_FUNC_BEGIN;
	BOOL ret=FALSE;
	SetCurrentDirectory(g_args.m_sPathMDSupport.c_str());
	SetDllDirectory(g_args.m_sPathAASupport.c_str());

	HMODULE h = LoadLibrary("CoreFoundation.dll");
	if (h != NULL)
	{
		g_iTunesMobileDevice.my_kCFTypeDictionaryKeyCallBacks = *(CFDictionaryKeyCallBacks*)GetProcAddress(h, "kCFTypeDictionaryKeyCallBacks");
		g_iTunesMobileDevice.my_kCFTypeDictionaryValueCallBacks = *(CFDictionaryValueCallBacks*)GetProcAddress(h, "kCFTypeDictionaryValueCallBacks");
		g_iTunesMobileDevice.my_kCFTypeArrayCallBacks = *(CFArrayCallBacks*)GetProcAddress(h, "kCFTypeArrayCallBacks");
		g_iTunesMobileDevice.my_kCFStreamPropertyDataWritten = *(CFStringRef*)GetProcAddress(h, "kCFStreamPropertyDataWritten");
		g_iTunesMobileDevice.my_kCFBooleanTrue = *(CFBooleanRef*)GetProcAddress(h, "kCFBooleanTrue");
		g_iTunesMobileDevice.my_kCFBooleanFalse = *(CFBooleanRef*)GetProcAddress(h, "kCFBooleanFalse");

		//FreeLibrary(h);
	}

	g_iTunesMobileDevice.hDll = LoadLibrary(_T("MobileDevice.dll"));
	if (g_iTunesMobileDevice.hDll == NULL) {
		g_iTunesMobileDevice.hDll = LoadLibrary(_T("iTunesMobileDevice.dll"));
	}
	if (g_iTunesMobileDevice.hDll!=NULL)
	{
		SetDllDirectory(NULL);
#define resolve_entry(f)	g_iTunesMobileDevice.f = (f)GetProcAddress(g_iTunesMobileDevice.hDll, #f);if(g_iTunesMobileDevice.f==NULL){logIt(_T("can not load api %s"),#f);}
		resolve_entry(AMRestoreRegisterForDeviceNotifications);
		resolve_entry(AMRestoreRegisterForDeviceNotifications);
		resolve_entry(AMDeviceNotificationSubscribe);
		resolve_entry(AMDeviceNotificationUnsubscribe);
		resolve_entry(AMDeviceConnect);
		resolve_entry(AMDeviceEnterRecovery);
		resolve_entry(AMRecoveryModeDeviceReboot);
		resolve_entry(AMRecoveryModeDeviceSetAutoBoot);
		resolve_entry(AMRecoveryModeDeviceGetECID);
		resolve_entry(AMRecoveryModeDeviceCopySerialNumber);
		resolve_entry(AMRecoveryModeDeviceCopyIMEI);
		resolve_entry(AMRecoveryModeDeviceGetLocationID);
		resolve_entry(AMRecoveryModeDeviceGetProductID);
		resolve_entry(AMRecoveryModeDeviceGetProductType);
		resolve_entry(AMRecoveryModeGetSoftwareBuildVersion);
		resolve_entry(AMRecoveryModeDeviceGetBoardID);
		resolve_entry(AMRecoveryModeDeviceGetChipID);


		resolve_entry(AMRecoveryModeDeviceSendCommandToDevice);
		resolve_entry(AMRecoveryModeDeviceSendBlindCommandToDevice);


		resolve_entry(AMDFUModeDeviceGetECID);
		resolve_entry(AMRestorableDeviceGetECID);
		resolve_entry(AMDeviceDisconnect);
		resolve_entry(AMDeviceValidatePairing);
		resolve_entry(AMDevicePair);
		resolve_entry(AMDeviceExtendedPairWithOptions);
		resolve_entry(AMDeviceIsPaired);
		resolve_entry(AMDeviceStartSession);
		resolve_entry(AMDeviceStopSession);
		resolve_entry(AMDServiceConnectionGetSecureIOContext);
		resolve_entry(AMDServiceConnectionGetSocket);

		resolve_entry(AMDeviceSecureStartService);
		resolve_entry(AMDServiceConnectionReceive);
		resolve_entry(AMDServiceConnectionSend);
		resolve_entry(AMDServiceConnectionReceiveMessage);
		resolve_entry(AMDServiceConnectionSendMessage);
		resolve_entry(AMDeviceCopyDeviceIdentifier);
		resolve_entry(AMRestorableDeviceCopySerialNumber);
		resolve_entry(AMRestorableDeviceCopySerialDevicePath);
		resolve_entry(AMRestorableDeviceRegisterForNotifications);
		resolve_entry(AMRestorableDeviceRegisterForNotificationsForDevices);
		resolve_entry(AMRestorableDeviceUnregisterForNotifications);
		resolve_entry(AMRestorableDeviceGetState);
		resolve_entry(AMRestorableDeviceCopyDefaultRestoreOptions);
		resolve_entry(AMRestoreCreateDefaultOptions);
		resolve_entry(AMRestorableDeviceRestore);
		resolve_entry(AMRestorePerformRecoveryModeRestore);
		resolve_entry(AMRestorableDeviceCopyAMDevice);
		resolve_entry(AMRestorableDeviceCopyRestoreModeDevice);
		resolve_entry(AMRestorableDeviceCopyRecoveryModeDevice);
		resolve_entry(AMRestoreModeDeviceReboot);
		resolve_entry(AMDeviceCopyValue);
		resolve_entry(AMDeviceSetValue);
		resolve_entry(AMDeviceRemoveValue);

		resolve_entry(AMDeviceStartService);
		resolve_entry(AMDeviceDeactivate);
		resolve_entry(AMDeviceActivate);
		resolve_entry(AMDeviceCreateActivationSessionInfo);//(HANDLE device, int *errcode);
		resolve_entry(AMDeviceActivateWithOptions);//(HANDLE device, CFTypeRef pl_p, CFTypeRef pl_header);
		resolve_entry(AMDeviceCreateActivationInfoWithOptions);//(HANDLE device, CFTypeRef pdata, int zero, int *errcode);
		resolve_entry(AMDeviceCreateActivationInfo);//(HANDLE device, CFTypeRef pdata, int *errcode);

		
		resolve_entry(AFCConnectionOpen);
		resolve_entry(AFCConnectionClose);
		resolve_entry(AFCDeviceInfoOpen);
		resolve_entry(AFCKeyValueRead);
		resolve_entry(AFCKeyValueClose);
		resolve_entry(AFCFileInfoOpen);

		resolve_entry(AFCFileRefTell);
		resolve_entry(AFCFlushData);


		/* mode 2 = read, mode 3 = write */
		resolve_entry(AFCFileRefOpen);
		resolve_entry(AFCFileRefSeek);
		resolve_entry(AFCFileRefRead);
		resolve_entry(AFCFileRefSetFileSize);
		resolve_entry(AFCFileRefWrite);
		resolve_entry(AFCFileRefClose);

		resolve_entry(AFCDirectoryOpen);
		resolve_entry(AFCDirectoryClose);
		resolve_entry(AFCDirectoryCreate);
		resolve_entry(AFCDirectoryRead );
		resolve_entry(AFCRemovePath );
		resolve_entry(AFCRenamePath );
		resolve_entry(AFCLinkPath );



		resolve_entry(AMDeviceSecureTransferPath);
		resolve_entry(AMDeviceSecureInstallApplication);
		resolve_entry(AMDeviceSecureUninstallApplication);
		resolve_entry(AMDeviceLookupApplications);
		resolve_entry(AMDeviceLookupApplicationArchives);
		resolve_entry(AMDeviceArchiveApplication);


		//provisioning profile
		resolve_entry(AMDeviceCopyProvisioningProfiles);
		resolve_entry(AMDeviceInstallProvisioningProfile);
		resolve_entry(AMDeviceRemoveProvisioningProfile);


		resolve_entry(AMDeviceStartHouseArrestService);
		resolve_entry(AMDeviceCreateHouseArrestService);

		resolve_entry(AMDeviceGetConnectionID );
		resolve_entry(USBMuxConnectByPort );
		resolve_entry(AMDGetPairingRecordDirectoryPath);
		resolve_entry(AMRestoreSetLogLevel );
		resolve_entry(AMRestoreEnableFileLogging );
		resolve_entry(AMDSetLogLevel );
		//resolve_entry(AMDAddLogFileDescriptor );

		resolve_entry(AMDCopyErrorText);

		resolve_entry(AMDPostNotification );
		resolve_entry(AMDObserveNotification );
		resolve_entry(AMDListenForNotifications );
		resolve_entry(AMDShutdownNotificationProxy );

		//MISProfile
		resolve_entry(MISProfileCreateWithFile);
		resolve_entry(MISProfileCreateWithData);
		resolve_entry(MISProfileWriteToFile);
		resolve_entry(MISProfileGetValue);
		resolve_entry(MISProvisioningProfileGetProvisionedDevices);
		resolve_entry(MISProvisioningProfileGetCreationDate);
		resolve_entry(MISProvisioningProfileGetDeveloperCertificates);
		resolve_entry(MISProvisioningProfileGetExpirationDate);
		resolve_entry(MISProvisioningProfileGetName);
		resolve_entry(MISProvisioningProfileGetUUID);
		resolve_entry(MISProvisioningProfileGetVersion);
		resolve_entry(MISProvisioningProfileIncludesDevice);
		resolve_entry(MISProvisioningProfileProvisionsAllDevices);
		resolve_entry(MISProvisioningProfileValidateSignature);
		resolve_entry(MISProvisioningProfileCheckValidity);

		resolve_entry(_InitializeMobileDevice);

#undef	resolve_entry

		if (g_iTunesMobileDevice._InitializeMobileDevice != NULL) {
			ret = g_iTunesMobileDevice._InitializeMobileDevice();
			logIt("_InitializeMobileDevice ret = %d", ret);
		}


		if (notification == kAMNormalNotification)
		{
			//ret=Register_iTunesMobileDevice_Callback();
		}
		else
		{
			//ret = RestorableDevice_Register_CallBack();
		}
	}
	ENTER_FUNC_END;
	return TRUE;
}

void SetVerbose(BOOL b)
{
	extern PARAMETERS_T g_args;
	g_args.bVerbose = b;
	if (b)
	{
		g_iTunesMobileDevice.AMDSetLogLevel(INT_MAX);
		//g_iTunesMobileDevice.AMDAddLogFileDescriptor(_fileno(stderr));
		g_iTunesMobileDevice.AMRestoreSetLogLevel(INT_MAX);
		g_iTunesMobileDevice.AMRestoreEnableFileLogging("CON");
	} else {
		g_iTunesMobileDevice.AMDSetLogLevel(0);
		g_iTunesMobileDevice.AMRestoreSetLogLevel(0);
	}

}

STRING GetAMDErrorText(int errorCode)
{
	STRING error;
	CFStringRef errorPtr = (CFStringRef)g_iTunesMobileDevice.AMDCopyErrorText(errorCode);
	if (errorPtr != NULL)
	{
		if (CFGetTypeID(errorPtr) == CFStringGetTypeID())
		{
			CFIndex length = CFStringGetLength(errorPtr);
			CFIndex maxSize =
				CFStringGetMaximumSizeForEncoding(length,
				kCFStringEncodingUTF8);
			char *buffer = (char *)malloc(maxSize);
			if (NULL != buffer)
			{
				if (CFStringGetCString(errorPtr, buffer, maxSize, kCFStringEncodingUTF8))
				{
					error.append(buffer);
				}
				SAFE_DELETE(buffer);
			}

		}
		CF_RELEASE_CLEAR(errorPtr);
	}
	if (!error.empty())
	{
		return error;
	}

	char *result = NULL;
	switch (errorCode)
	{
	case -402653183:
		result = "kAMDUndefinedError";
		break;
	case -402653182:
		result = "kAMDBadHeaderError";
		break;
	case -402653181:
		result = "kAMDNoResourcesError";
		break;
	case -402652992:
		result = "kAMDNoSpaceError";
		break;
	case -402653180:
		result = "kAMDReadError";
		break;
	case -402653179:
		result = "kAMDWriteError";
		break;
	case -402653178:
		result = "kAMDUnknownPacketError";
		break;
	case -402653177:
		result = "kAMDInvalidArgumentError";
		break;
	case -402653176:
		result = "kAMDNotFoundError";
		break;
	case -402653175:
		result = "kAMDIsDirectoryError";
		break;
	case -402653174:
		result = "kAMDPermissionError";
		break;
	case -402653173:
		result = "kAMDNotConnectedError";
		break;
	case -402653172:
		result = "kAMDTimeOutError";
		break;
	case -402653171:
		result = "kAMDOverrunError";
		break;
	case -402653170:
		result = "kAMDEOFError";
		break;
	case -402653169:
		result = "kAMDUnsupportedError";
		break;
	case -402653168:
		result = "kAMDFileExistsError";
		break;
	case -402653167:
		result = "kAMDBusyError";
		break;
	case -402653166:
		result = "kAMDCryptoError";
		break;
	case -402653051:
		result = "kAMDTooBigError";
		break;
	case -402653165:
		result = "kAMDInvalidResponseError";
		break;
	case -402653164:
		result = "kAMDMissingKeyError";
		break;
	case -402653163:
		result = "kAMDMissingValueError";
		break;
	case -402653162:
		result = "kAMDGetProhibitedError";
		break;
	case -402653161:
		result = "kAMDSetProhibitedError";
		break;
	case -402653160:
		result = "kAMDRemoveProhibitedError";
		break;
	case -402653159:
		result = "kAMDImmutableValueError";
		break;
	case -402653158:
		result = "kAMDPasswordProtectedError";
		break;
	case -402653035:
		result = "kAMDUserDeniedPairingError";
		break;
	case -402653034:
		result = "kAMDPairingDialogResponsePendingError";
		break;
	case -402653157:
		result = "kAMDMissingHostIDError";
		break;
	case -402653156:
		result = "kAMDInvalidHostIDError";
		break;
	case -402653155:
		result = "kAMDSessionActiveError";
		break;
	case -402653154:
		result = "kAMDSessionInactiveError";
		break;
	case -402653153:
		result = "kAMDMissingSessionIDError";
		break;
	case -402653152:
		result = "kAMDInvalidSessionIDError";
		break;
	case -402653151:
		result = "kAMDMissingServiceError";
		break;
	case -402653150:
		result = "kAMDInvalidServiceError";
		break;
	case -402653093:
		result = "kAMDServiceLimitError";
		break;
	case -402653090:
		result = "kAMDCheckinSetupFailedError";
		break;
	case -402653149:
		result = "kAMDInvalidCheckinError";
		break;
	case -402653148:
		result = "kAMDCheckinTimeoutError";
		break;
	case -402653089:
		result = "kAMDCheckinConnectionFailedError";
		break;
	case -402653088:
		result = "kAMDCheckinReceiveFailedError";
		break;
	case -402653087:
		result = "kAMDCheckinResponseFailedError";
		break;
	case -402653079:
		result = "kAMDCheckinOutOfMemoryError";
		break;
	case -402653086:
		result = "kAMDCheckinSendFailedError";
		break;
	case -402653147:
		result = "kAMDMissingPairRecordError";
		break;
	case -402653092:
		result = "kAMDInvalidPairRecordError";
		break;
	case -402653080:
		result = "kAMDSavePairRecordFailedError";
		break;
	case -402653146:
		result = "kAMDInvalidActivationRecordError";
		break;
	case -402653145:
		result = "kAMDMissingActivationRecordError";
		break;
	case -402653091:
		result = "kAMDServiceProhibitedError";
		break;
	case -402653055:
		result = "kAMDEscrowLockedError";
		break;
	case -402653054:
		result = "kAMDPairingProhibitedError";
		break;
	case -402653053:
		result = "kAMDProhibitedBySupervision";
		break;
	case -402653144:
		result = "kAMDWrongDroidError";
		break;
	case -402653143:
		result = "kAMDSUVerificationError";
		break;
	case -402653142:
		result = "kAMDSUPatchError";
		break;
	case -402653141:
		result = "kAMDSUFirmwareError";
		break;
	case -402653140:
		result = "kAMDProvisioningProfileNotValid";
		break;
	case -402653139:
		result = "kAMDSendMessageError";
		break;
	case -402653138:
		result = "kAMDReceiveMessageError";
		break;
	case -402653137:
		result = "kAMDMissingOptionsError";
		break;
	case -402653136:
		result = "kAMDMissingImageTypeError";
		break;
	case -402653135:
		result = "kAMDDigestFailedError";
		break;
	case -402653134:
		result = "kAMDStartServiceError";
		break;
	case -402653133:
		result = "kAMDInvalidDiskImageError";
		break;
	case -402653132:
		result = "kAMDMissingDigestError";
		break;
	case -402653131:
		result = "kAMDMuxError";
		break;
	case -402653130:
		result = "kAMDApplicationAlreadyInstalledError";
		break;
	case -402653129:
		result = "kAMDApplicationMoveFailedError";
		break;
	case -402653128:
		result = "kAMDApplicationSINFCaptureFailedError";
		break;
	case -402653127:
		result = "kAMDApplicationSandboxFailedError";
		break;
	case -402653126:
		result = "kAMDApplicationVerificationFailedError";
		break;
	case -402653125:
		result = "kAMDArchiveDestructionFailedError";
		break;
	case -402653124:
		result = "kAMDBundleVerificationFailedError";
		break;
	case -402653123:
		result = "kAMDCarrierBundleCopyFailedError";
		break;
	case -402653122:
		result = "kAMDCarrierBundleDirectoryCreationFailedError";
		break;
	case -402653121:
		result = "kAMDCarrierBundleMissingSupportedSIMsError";
		break;
	case -402653120:
		result = "kAMDCommCenterNotificationFailedError";
		break;
	case -402653119:
		result = "kAMDContainerCreationFailedError";
		break;
	case -402653118:
		result = "kAMDContainerP0wnFailedError";
		break;
	case -402653117:
		result = "kAMDContainerRemovalFailedError";
		break;
	case -402653116:
		result = "kAMDEmbeddedProfileInstallFailedError";
		break;
	case -402653115:
		result = "kAMDErrorError";
		break;
	case -402653114:
		result = "kAMDExecutableTwiddleFailedError";
		break;
	case -402653113:
		result = "kAMDExistenceCheckFailedError";
		break;
	case -402653112:
		result = "kAMDInstallMapUpdateFailedError";
		break;
	case -402653111:
		result = "kAMDManifestCaptureFailedError";
		break;
	case -402653110:
		result = "kAMDMapGenerationFailedError";
		break;
	case -402653109:
		result = "kAMDMissingBundleExecutableError";
		break;
	case -402653108:
		result = "kAMDMissingBundleIdentifierError";
		break;
	case -402653107:
		result = "kAMDMissingBundlePathError";
		break;
	case -402653106:
		result = "kAMDMissingContainerError";
		break;
	case -402653105:
		result = "kAMDNotificationFailedError";
		break;
	case -402653104:
		result = "kAMDPackageExtractionFailedError";
		break;
	case -402653103:
		result = "kAMDPackageInspectionFailedError";
		break;
	case -402653102:
		result = "kAMDPackageMoveFailedError";
		break;
	case -402653101:
		result = "kAMDPathConversionFailedError";
		break;
	case -402653100:
		result = "kAMDRestoreContainerFailedError";
		break;
	case -402653099:
		result = "kAMDSeatbeltProfileRemovalFailedError";
		break;
	case -402653098:
		result = "kAMDStageCreationFailedError";
		break;
	case -402653097:
		result = "kAMDSymlinkFailedError";
		break;
	case -402653096:
		result = "kAMDiTunesArtworkCaptureFailedError";
		break;
	case -402653095:
		result = "kAMDiTunesMetadataCaptureFailedError";
		break;
	case -402653094:
		result = "kAMDAlreadyArchivedError";
		break;
	case -402653082:
		result = "kAMDUnknownCommandError";
		break;
	case -402653081:
		result = "kAMDAPIInternalError";
		break;
	case -402653085:
		result = "kAMDMuxCreateListenerError";
		break;
	case -402653084:
		result = "kAMDMuxGetListenerError";
		break;
	case -402653083:
		result = "kAMDMuxConnectError";
		break;
	case -402653078:
		result = "kAMDDeviceTooNewError";
		break;
	case -402653077:
		result = "kAMDDeviceRefNoGood";
		break;
	case -402653052:
		result = "kAMDDeviceDisconnectedError";
		break;
	case -402653076:
		result = "kAMDCannotTranslateError";
		break;
	case -402653075:
		result = "kAMDMobileImageMounterMissingImageSignature";
		break;
	case -402653074:
		result = "kAMDMobileImageMounterResponseCreationFailed";
		break;
	case -402653073:
		result = "kAMDMobileImageMounterMissingImageType";
		break;
	case -402653072:
		result = "kAMDMobileImageMounterMissingImagePath";
		break;
	case -402653071:
		result = "kAMDMobileImageMounterImageMapLoadFailed";
		break;
	case -402653070:
		result = "kAMDMobileImageMounterAlreadyMounted";
		break;
	case -402653069:
		result = "kAMDMobileImageMounterImageMoveFailed";
		break;
	case -402653068:
		result = "kAMDMobileImageMounterMountPathMissing";
		break;
	case -402653067:
		result = "kAMDMobileImageMounterMountPathNotEmpty";
		break;
	case -402653066:
		result = "kAMDMobileImageMounterImageMountFailed";
		break;
	case -402653065:
		result = "kAMDMobileImageMounterTrustCacheLoadFailed";
		break;
	case -402653064:
		result = "kAMDMobileImageMounterDigestFailed";
		break;
	case -402653063:
		result = "kAMDMobileImageMounterDigestCreationFailed";
		break;
	case -402653062:
		result = "kAMDMobileImageMounterImageVerificationFailed";
		break;
	case -402653061:
		result = "kAMDMobileImageMounterImageInfoCreationFailed";
		break;
	case -402653060:
		result = "kAMDMobileImageMounterImageMapStoreFailed";
		break;
	case -402652958:
		result = "kAMDMobileImageMounterDeviceLocked";
		break;
	case -402653059:
		result = "kAMDBonjourSetupError";
		break;
	case -402653057:
		result = "kAMDNoWifiSyncSupportError";
		break;
	case -402653058:
		result = "kAMDDeviceOSVersionTooLow";
		break;
	case -402653056:
		result = "kAMDDeviceFamilyNotSupported";
		break;
	case -402653050:
		result = "kAMDPackagePatchFailedError";
		break;
	case -402653049:
		result = "kAMDIncorrectArchitectureError";
		break;
	case -402653048:
		result = "kAMDPluginCopyFailedError";
		break;
	case -402653047:
		result = "kAMDBreadcrumbFailedError";
		break;
	case -402653046:
		result = "kAMDBreadcrumbUnlockError";
		break;
	case -402653045:
		result = "kAMDGeoJSONCaptureFailedError";
		break;
	case -402653044:
		result = "kAMDNewsstandArtworkCaptureFailedError";
		break;
	case -402653043:
		result = "kAMDMissingCommandError";
		break;
	case -402653042:
		result = "kAMDNotEntitledError";
		break;
	case -402653041:
		result = "kAMDMissingPackagePathError";
		break;
	case -402653040:
		result = "kAMDMissingContainerPathError";
		break;
	case -402653039:
		result = "kAMDMissingApplicationIdentifierError";
		break;
	case -402653038:
		result = "kAMDMissingAttributeValueError";
		break;
	case -402653037:
		result = "kAMDLookupFailedError";
		break;
	case -402653036:
		result = "kAMDDictCreationFailedError";
		break;
	case -402653033:
		result = "kAMDInstallProhibitedError";
		break;
	case -402653032:
		result = "kAMDUninstallProhibitedError";
		break;
	case -402653028:
		result = "kAMDMissingBundleVersionError";
		break;
	case -402652993:
		result = "kAMDInvalidSymlinkError";
		break;
	case -402653031:
		result = "kAMDFMiPProtectedError";
		break;
	case -402653030:
		result = "kAMDMCProtected";
		break;
	case -402653029:
		result = "kAMDMCChallengeRequired";
		break;
	case -402653027:
		result = "kAMDAppBlacklistedError";
		break;
	case -402653005:
		result = "kMobileHouseArrestMissingCommand";
		break;
	case -402653004:
		result = "kMobileHouseArrestUnknownCommand";
		break;
	case -402653003:
		result = "kMobileHouseArrestMissingIdentifier";
		break;
	case -402653002:
		result = "kMobileHouseArrestDictionaryFailed";
		break;
	case -402653001:
		result = "kMobileHouseArrestInstallationLookupFailed";
		break;
	case -402653000:
		result = "kMobileHouseArrestApplicationLookupFailed";
		break;
	case -402652999:
		result = "kMobileHouseArrestMissingContainer";
		break;
	case -402652997:
		result = "kMobileHouseArrestPathConversionFailed";
		break;
	case -402652996:
		result = "kMobileHouseArrestPathMissing";
		break;
	case -402652995:
		result = "kMobileHouseArrestInvalidPath";
		break;
	case -402652994:
		result = "kAMDMismatchedApplicationIdentifierEntitlementError";
		break;
	case -402652989:
		result = "kAMDBundleiTunesMetadataVersionMismatchError";
		break;
	case -402652988:
		result = "kAMDInvalidiTunesMetadataPlistError";
		break;
	case -402652987:
		result = "kAMDMismatchedBundleIDSigningIdentifierError";
		break;
	case -402652984:
		result = "kAMDDeviceNotSupportedByThinningError";
		break;
	case -402652981:
		result = "kAMDAppexBundleIDConflictWithOtherIdentifierError";
		break;
	case -402652980:
		result = "kAMDBundleIDConflictWithOtherIdentifierError";
		break;
	case -402652957:
		result = "kAMDInvalidSINFError";
		break;
	case -402653026:
		result = "This app contains an app extension with an illegal bundle identifier. App extension bundle identifiers must have a prefix consisting of their containing application's bundle identifier followed by a '.'.";
		break;
	case -402653025:
		result = "If an app extension defines the XPCService key in its Info.plist, it must have a dictionary value.";
		break;
	case -402653024:
		result = "App extensions must define the NSExtension key with a dictionary value in their Info.plist.";
		break;
	case -402653023:
		result = "If an app extension defines the CFBundlePackageType key in its Info.plist, it must have the value \"XPC!\".";
		break;
	case -402653022:
		result = "App extensions must define either NSExtensionMainStoryboard or NSExtensionPrincipalClass keys in the NSExtension dictionary in their Info.plist.";
		break;
	case -402653021:
		result = "If an app extension defines the NSExtensionContextClass key in the NSExtension dictionary in its Info.plist, it must have a string value containing one or more characters.";
		break;
	case -402653020:
		result = "If an app extension defines the NSExtensionContextHostClass key in the NSExtension dictionary in its Info.plist, it must have a string value containing one or more characters.";
		break;
	case -402653019:
		result = "If an app extension defines the NSExtensionViewControllerHostClass key in the NSExtension dictionary in its Info.plist, it must have a string value containing one or more characters.";
		break;
	case -402653018:
		result = "This app contains an app extension that does not define the NSExtensionPointIdentifier key in its Info.plist. This key must have a reverse-DNS format string value.";
		break;
	case -402653017:
		result = "This app contains an app extension that does not define the NSExtensionPointIdentifier key in its Info.plist with a valid reverse-DNS format string value.";
		break;
	case -402653016:
		result = "If an app extension defines the NSExtensionAttributes key in the NSExtension dictionary in its Info.plist, it must have a dictionary value.";
		break;
	case -402653015:
		result = "If an app extension defines the NSExtensionPointName key in the NSExtensionAttributes dictionary in the NSExtension dictionary in its Info.plist, it must have a string value containing one or more characters.";
		break;
	case -402653014:
		result = "If an app extension defines the NSExtensionPointVersion key in the NSExtensionAttributes dictionary in the NSExtension dictionary in its Info.plist, it must have a string value containing one or more characters.";
		break;
	case -402653013:
		result = "This app or a bundle it contains does not define the CFBundleName key in its Info.plist with a string value containing one or more characters.";
		break;
	case -402653012:
		result = "This app or a bundle it contains does not define the CFBundleDisplayName key in its Info.plist with a string value containing one or more characters.";
		break;
	case -402653011:
		result = "This app or a bundle it contains defines the CFBundleShortVersionStringKey key in its Info.plist with a non-string value or a zero-length string value.";
		break;
	case -402653010:
		result = "This app or a bundle it contains defines the RunLoopType key in the XPCService dictionary in its Info.plist with a non-string value or a zero-length string value.";
		break;
	case -402653009:
		result = "This app or a bundle it contains defines the ServiceType key in the XPCService dictionary in its Info.plist with a non-string value or a zero-length string value.";
		break;
	case -402653008:
		result = "This application or a bundle it contains has the same bundle identifier as this application or another bundle that it contains. Bundle identifiers must be unique.";
		break;
	case -402653007:
		result = "This app contains an app extension that specifies an extension point identifier that is not supported on this version of iOS for the value of the NSExtensionPointIdentifier key in its Info.plist.";
		break;
	case -402653006:
		result = "This app contains multiple app extensions that are file providers. Apps are only allowed to contain at most a single file provider app extension.";
		break;
	case -402652990:
		result = "This app is not a valid AppleTV Stub App";
		break;
	case -402652965:
		result = "The device is out of storage for apps. Please remove some apps from the device and try again.";
		break;
	case -402652986:
		result = "This app contains multiple WatchKit app extensions. Only a single WatchKit extension is allowed.";
		break;
	case -402652985:
		result = "A WatchKit app within this app is not a valid bundle.";
		break;
	case -402652983:
		result = "The UISupportedDevices key in this app's Info.plist does not specify a valid set of supported devices.";
		break;
	case -402652982:
		result = "This app contains an app extension with an illegal bundle identifier. App extension bundle identifiers must have a prefix consisting of their containing application's bundle identifier followed by a '.', with no further '.' characters after the prefix.";
		break;
	case -402652979:
		result = "This app contains multiple WatchKit 1.0 apps. Only a single WatchKit 1.0 app is allowed.";
		break;
	case -402652978:
		result = "This app contains multiple WatchKit 2.0 apps. Only a single WatchKit 2.0 app is allowed.";
		break;
	case -402652977:
		result = "The WatchKit app has an invalid stub executable.";
		break;
	case -402652976:
		result = "The WatchKit app has multiple app extensions. Only a single WatchKit extension is allowed in a WatchKit app, and only if this is a WatchKit 2.0 app.";
		break;
	case -402652975:
		result = "The WatchKit 2.0 app contains non-WatchKit app extensions. Only WatchKit app extensions are allowed in WatchKit apps.";
		break;
	case -402652974:
		result = "The WatchKit app has one or more embedded frameworks. Frameworks are only allowed in WatchKit app extensions in WatchKit 2.0 apps.";
		break;
	case -402652973:
		result = "This app contains a WatchKit 1.0 app with app extensions. This is not allowed.";
		break;
	case -402652972:
		result = "This app contains a WatchKit 2.0 app without an app extension. WatchKit 2.0 apps must contain a WatchKit app extension.";
		break;
	case -402652971:
		result = "The WatchKit app's Info.plist must have a WKCompanionAppBundleIdentifier key set to the bundle identifier of the companion app.";
		break;
	case -402652970:
		result = "The WatchKit app's Info.plist contains a non-string key.";
		break;
	case -402652969:
		result = "The WatchKit app's Info.plist contains a key that is not in the whitelist of allowed keys for a WatchKit app.";
		break;
	case -402652968:
		result = "The WatchKit 1.0 and a WatchKit 2.0 apps within this app must have have the same bundle identifier.";
		break;
	case -402652967:
		result = "This app contains a WatchKit app with an invalid bundle identifier. The bundle identifier of a WatchKit app must have a prefix consisting of the companion app's bundle identifier, followed by a '.'.";
		break;
	case -402652966:
		result = "This app contains a WatchKit app where the UIDeviceFamily key in its Info.plist does not specify the value 4 to indicate that it's compatible with the Apple Watch device type.";
		break;
	case -402652991:
		result = "The WatchKit app extension must have, in its Info.plist's NSExtension dictionary's NSExtensionAttributes dictionary, the key WKAppBundleIdentifier with a value equal to the associated WatchKit app's bundle identifier.";
		break;
	case -402652964:
		result = "This app or an app that it contains has a Siri Intents app extension that is missing the IntentsSupported array in the NSExtensionAttributes dictionary in the NSExtension dictionary in its Info.plist.";
		break;
	case -402652963:
		result = "This app or an app that it contains has a Siri Intents app extension that does not correctly define the IntentsRestrictedWhileLocked key in the NSExtensionAttributes dictionary in the NSExtension dictionary in its Info.plist. The key's value must be an array of strings.";
		break;
	case -402652962:
		result = "This app or an app that it contains has a Siri Intents app extension that declares values in its IntentsRestrictedWhileLocked key's array value that are not in its IntentsSupported key's array value (in the NSExtensionAttributes dictionary in the NSExtension dictionary in its Info.plist).";
		break;
	case -402652961:
		result = "This app or an app that it contains declares multiple Siri Intents app extensions that declare one or more of the same values in the IntentsSupported array in the NSExtensionAttributes dictionary in the NSExtension dictionary in their Info.plist. IntentsSupported must be distinct among a given Siri Intents extension type within an app.";
		break;
	case -402652960:
		result = "The WatchKit 2.0 app, which expects to be compatible with watchOS versions earlier than 3.0, contains a non-WatchKit extension in a location that's not compatible with watchOS versions earlier than 3.0.";
		break;
	case -402652959:
		result = "The WatchKit 2.0 app, which expects to be compatible with watchOS versions earlier than 3.0, contains a framework in a location that's not compatible with watchOS versions earlier than 3.0.";
		break;
	case -402652956:
		result = "Multiple iMessage app extensions were found in this app. Only one is allowed.";
		break;
	case -402652955:
		result = "This iMessage application is missing its required iMessage app extension.";
		break;
	case -402652954:
		result = "This iMessage application contains an app extension type other than an iMessage app extension. iMessage applications may only contain one iMessage app extension and may not contain other types of app extensions.";
		break;
	default:
		result = "kAMDSuccess";
	}
	error.append(result);

	return error;
}