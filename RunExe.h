#pragma once
#include "DeviceInfo.h"
using namespace System;
using namespace System::Collections::Generic;
using namespace System::IO;

ref class CRunExe
{
private:
	static List<String^>^ ret;
	static DateTime last_output;
	static System::Threading::ManualResetEvent^ ev;
	static int Exitcode = -1;
	static  void OnOutputDataReceived(System::Object^ sender, System::Diagnostics::DataReceivedEventArgs^ args)
	{
		last_output = DateTime::Now;
		if (!String::IsNullOrEmpty(args->Data))
		{
			logItcli(String::Format("[runExe]: {0}", args->Data));
			ret->Add(args->Data);

			if (args->Data->StartsWith("Activation Lock"))
			{
				ev->Set();		
				Exitcode = iCloudLock;
			}
		}
		if (args->Data == nullptr)
			ev->Set();
	}
public:

	static List<String^>^ runExe(String^ exeFilename, String^ param, int &exitCode, int timeout, bool output)
	{
		ret = gcnew List<String^>();
		exitCode = 1;
		logItcli(String::Format("[runExe]: ++ exe={0}, param={1}", exeFilename, param));
		try
		{
			if (File::Exists(exeFilename))
			{
				last_output = DateTime::Now;
				DateTime _start = DateTime::Now;
				ev = gcnew System::Threading::ManualResetEvent(false);
				System::Diagnostics::Process^ p = gcnew System::Diagnostics::Process();
				p->StartInfo->FileName = exeFilename;
				p->StartInfo->Arguments = param;
				p->StartInfo->UseShellExecute = false;
				p->StartInfo->WindowStyle = System::Diagnostics::ProcessWindowStyle::Hidden;
				p->StartInfo->RedirectStandardOutput = true;
				//p->StartInfo->RedirectStandardError = true;
				p->StartInfo->CreateNoWindow = true;
				p->OutputDataReceived += gcnew System::Diagnostics::DataReceivedEventHandler(OnOutputDataReceived);
				//p->ErrorDataReceived += gcnew System::Diagnostics::DataReceivedEventHandler(OnOutputDataReceived);

				p->Start();
				_start = DateTime::Now;
				//p->BeginErrorReadLine();
				p->BeginOutputReadLine();
				bool process_terminated = false;
				bool proces_stdout_cloded = false;
				bool proces_has_killed = false;
				while (!proces_stdout_cloded || !process_terminated)
				{
					if (p->HasExited)
					{
						// process is terminated
						process_terminated = true;
						logItcli(String::Format("[runExe]: process is going to terminate."));
					}
					if (ev->WaitOne(1000))
					{
						// stdout is colsed
						proces_stdout_cloded = true;
						Sleep(1000);
						logItcli(String::Format("[*#message#*]={0}", Convert::ToBase64String(Encoding::UTF8->GetBytes(ret[ret->Count - 1]))), output);
						logItcli(String::Format("[runExe]: stdout pipe is going to close."));
						if (!p->HasExited)
						{
							p->Kill();
						}
						break;
					}
					if ((DateTime::Now - last_output).TotalMilliseconds > timeout)
					{
						logItcli(String::Format("[runExe]: there are {0} milliseconds no response. timeout?", timeout));
						// no output received within timeout milliseconds
						if (!p->HasExited)
						{
							exitCode = 1460;
							p->Kill();
							proces_has_killed = true;
							logItcli(String::Format("[runExe]: process is going to be killed due to timeout."));
						}
						break;
					}
				}
				if (!proces_has_killed)
					exitCode = p->ExitCode;

				if (Exitcode != -1){
					exitCode = Exitcode;
				}
			}
		}
		catch (Exception^ ex)
		{
			logItcli(String::Format("[runExe]: {0}", ex->Message));
			logItcli(String::Format("[runExe]: {0}", ex->StackTrace));
		}
		logItcli(String::Format("[runExe]: -- ret={0}", exitCode));
		return ret;
	}

};

