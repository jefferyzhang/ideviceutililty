#include "stdafx.h"
#include <CoreFoundation/CFDictionary.h>
#include "iDeviceUtil.h"
#include "iDeviceClass.h"
#include "DeviceInfo.h"
#include "iDeviceRecoveryMode.h"


using namespace System;

int DeviceInfo::getDeviceInfoDetect(System::Configuration::Install::InstallContext^ args)
{
	int ret = 1167;
	int delay = 10 * 1000;
	if (args->Parameters->ContainsKey("delay"))
	{
		int a;
		if (Int32::TryParse(args->Parameters["delay"], a))
		{
			delay = a * 1000;
		}
	}
	logItcli(String::Format("getDeviceInfo: ++ delay={0}", delay), false);

	if (args->Parameters->ContainsKey("udid"))
	{
		using namespace Runtime::InteropServices;
		const char* lpudid =
			(const char*)(Marshal::StringToHGlobalAnsi(args->Parameters["udid"]).ToPointer());
		iDeviceClass::start(lpudid);
		{
			if (iDeviceClass::idDeviceReady(delay))
			{
				iDeviceClass* dc = iDeviceClass::getDeviceInstanceByUdid(lpudid);
				if (dc != NULL)
				{
					if ((ret = dc->connect()) == ERROR_SUCCESS)
					{
						std::vector<STRING> info = dc->getDeviceInfoDetect();
						// dump info 
						for each(auto s in info)
						{
							_tprintf("%s\n", s.c_str());
							fflush(stdout);
						}
						dc->disconnect();
						_tprintf("TrustError=false");
						ret = 0;
					}
					else
					{
						logItcli(String::Format("Fail to connect iDevice: error code {0}", ret.ToString("X8")));
						if (((DWORD)ret == 0xe800001a) || ((DWORD)ret == 0xe800001c))
						{
							dc->GetInfoWithoutTrust();
							if (!dc->checkTrust())
							{
								// from ios 7 iphone need trust confirm.
								logItcli(String::Format("Fail to connect iDevice: please trust the connection"));
								_tprintf("TrustError=true");
								fflush(stdout);
								ret = iErrorTrust;
							}
							else
							{
								logItcli(String::Format("Fail to connect iDevice: please unlock the connection"));
								_tprintf("LockError=True");
								fflush(stdout);
								ret = iPSWLock;
							}
							ret = iErrorTrust;
						}
					}
				}
				else
				{
					logItcli("udid is empty", false);
				}
			}
			else
				logItcli("udid is not ready", false);

		}
		iDeviceClass::stop();
	}
	else
		logItcli("udid is not found", false);
	logItcli(String::Format("getDeviceInfo: -- {0}", ret), false);
	return ret;

}

int DeviceInfo::getDeviceInfoBundle(System::Configuration::Install::InstallContext^ args)
{
	int ret = 1167;
	int delay = 10 * 1000;
	if (args->Parameters->ContainsKey("delay"))
	{
		int a;
		if (Int32::TryParse(args->Parameters["delay"], a))
		{
			delay = a * 1000;
		}
	}
	logItcli(String::Format("getDeviceInfoBundle: ++ delay={0}", delay), false);

	if (args->Parameters->ContainsKey("udid"))
	{
		using namespace Runtime::InteropServices;
		const char* lpudid =
			(const char*)(Marshal::StringToHGlobalAnsi(args->Parameters["udid"]).ToPointer());
		iDeviceClass::start(lpudid);
		{
			if (iDeviceClass::idDeviceReady(delay))
			{
				iDeviceClass* dc = iDeviceClass::getDeviceInstanceByUdid(lpudid);
				if (dc != NULL)
				{
					if ((ret = dc->connect()) == ERROR_SUCCESS)
					{
						std::vector<STRING> info = dc->getDeviceInfoBundle();
						// dump info
						for each(auto s in info)
						{
							_tprintf("%s\r\n", s.c_str());
							fflush(stdout);
						}
						//StringDictionary sDict = iactive.InfoToDict(dc);
						//if (!sDict.ContainsKey("udid"))
						//{
						//	sDict.Add("udid", args->Parameters["udid"]);
						//}
						dc->disconnect();

						/*if (args->Parameters->ContainsKey("carrierlock"))
						{
						string aStatus = sDict["ActivationState"];
						if (String::IsNullOrEmpty(aStatus) || String::Compare(aStatus, "Unactivated", true) == 0)
						{
						logItcli("iDevice is Unactivated!", false);
						}

						ret = iactive.Check_CarrirerLock(false, "", sDict);
						}
						else*/
							ret = 0;
					}
					else
					{
						logItcli(String::Format("Fail to connect iDevice: error code {0}", ret.ToString("X8")));
						if (/*(uint)ret==iDeviceUtil.Properties.Settings.Default.AMD_ERR_iDeviceLocked ||*/
							(DWORD)ret == 0xe800001a || (DWORD)ret == 0xe800001c)
						{
							if (!dc->checkTrust())
							{
								// from ios 7 iphone need trust confirm.
								logItcli(String::Format("Fail to connect iDevice: please trust the connection"));
								System::Console::WriteLine("TrustError=true");
								ret = iErrorTrust;
							}
							else
							{
								logItcli(String::Format("Fail to connect iDevice: please unlock the connection"));
								System::Console::WriteLine("LockError=True");
								ret = iPSWLock;
							}
							ret = iErrorTrust;
						}
						// done = true;
					}

				}
				else
				{
					logItcli("udid is empty", false);
				}
			}
			else
				logItcli("udid is not ready", false);
		}
		iDeviceClass::stop();
	}
	else
		logItcli("udid is not found", false);
	logItcli(String::Format("getDeviceInfoBundle: -- {0}", ret), false);
	return ret;

}

int DeviceInfo::getDeviceRecoveryInfo(System::Configuration::Install::InstallContext^ args)
{
	int ret = 1167;
	int delay = 10 * 1000;
	if (args->Parameters->ContainsKey("delay"))
	{
		int a;
		if (Int32::TryParse(args->Parameters["delay"], a))
		{
			delay = a * 1000;
		}
	}
	logItcli(String::Format("getDeviceRecoveryInfo: ++ delay={0}", delay), false);

	if (args->Parameters->ContainsKey("id"))
	{
		INT64 id=0;
		if (INT64::TryParse(args->Parameters["id"], System::Globalization::NumberStyles::AllowHexSpecifier, System::Globalization::CultureInfo::InvariantCulture, id))
		{
			ret = iDeviceRecoveryMode::getDeviceRecoveryInfo(id, delay);
		}
		else
			ret = ERROR_INVALID_PARAMETER;
		/*iDeviceRecoveryMode.start(args->Parameters["id"]);
		if (iDeviceRecoveryMode.idDeviceReady(delay))
		{
		ret = iDeviceRecoveryMode.getDeviceInfo(iDeviceRecoveryMode.curDevice.ptr_curdev);
		}
		else
		logItcli("Wait recovery Device timeout.", false);*/
	}
	return ret;
}

int DeviceInfo::getDeviceInfo(System::Configuration::Install::InstallContext^ args)
{
	int ret = 1167;
	int delay = 10 * 1000;
	if (args->Parameters->ContainsKey("delay"))
	{
		int a;
		if (Int32::TryParse(args->Parameters["delay"],  a))
		{
			delay = a * 1000;
		}
	}
	logItcli(String::Format("getDeviceInfo: ++ delay={0}", delay), false);

	if (args->Parameters->ContainsKey("udid"))
	{
		using namespace Runtime::InteropServices;
		const char* lpudid =
			(const char*)(Marshal::StringToHGlobalAnsi(args->Parameters["udid"]).ToPointer());
		iDeviceClass::start(lpudid);
		{
			if (iDeviceClass::idDeviceReady(delay))
			{
				iDeviceClass* dc = iDeviceClass::getDeviceInstanceByUdid(lpudid);
				if (dc != NULL)
				{
					if ((ret = dc->connect()) == ERROR_SUCCESS)
					{
						std::vector<STRING> info = dc->getDeviceInfo(!args->IsParameterTrue("igorermn"), args->IsParameterTrue("single"));
						// dump info 
						for each(auto s in info)
						{
							_tprintf("%s\r\n", s.c_str());
							fflush(stdout);
						}
						std::vector<STRING> value = dc->GetSysInfo();
						for each(auto s in value)
						{
							_tprintf("%s\r\n", s.c_str());
							fflush(stdout);
						}
						
						BOOL bjail = dc->IsJailbreak();
						dc->disconnect();
						//System::Console::WriteLine(String::Format("\njailbreak={0}", bjail?"true":"false"));
						//System::Console::WriteLine("\nTrustError=false");
						_tprintf("\njailbreak=%s\r\n", bjail ? "true" : "false");
						fflush(stdout);
						_tprintf("TrustError=false");
						fflush(stdout);
						ret = 0;
					}
					else
					{
						logItcli(String::Format("Fail to connect iDevice: error code {0}", ret.ToString("X8")));
						if (((DWORD)ret == 0xe800001a) || ((DWORD)ret == 0xe800001c))
						{
							//dc->GetInfoWithoutTrust();
							if (!dc->checkTrust())
							{
								// from ios 7 iphone need trust confirm.
								logItcli(String::Format("Fail to connect iDevice: please trust the connection"));
								_tprintf("TrustError=true");
								ret = iErrorTrust;
							}
							else
							{
								logItcli(String::Format("Fail to connect iDevice: please unlock the connection"));
								_tprintf("LockError=True");
								ret = iPSWLock;
							}
							ret = iErrorTrust;
						}
					}

				}
				else
				{
					logItcli("udid is empty", false);
				}
			}
			else
				logItcli("udid is not ready", false);

		}
		iDeviceClass::stop();
	}
	else
		logItcli("udid is not found", false);
	logItcli(String::Format("getDeviceInfo: -- {0}", ret), false);
	return ret;
}

int DeviceInfo::getDeviceANumber(System::Configuration::Install::InstallContext^ args)
{
	int ret = 1167;
	int delay = 10 * 1000;
	if (args->Parameters->ContainsKey("delay"))
	{
		int a;
		if (Int32::TryParse(args->Parameters["delay"], a))
		{
			delay = a * 1000;
		}
	}
	logItcli(String::Format("getDeviceInfo: ++ delay={0}", delay), false);

	if (args->Parameters->ContainsKey("udid"))
	{
		using namespace Runtime::InteropServices;
		const char* lpudid =
			(const char*)(Marshal::StringToHGlobalAnsi(args->Parameters["udid"]).ToPointer());
		iDeviceClass::start(lpudid);
		{
			if (iDeviceClass::idDeviceReady(delay))
			{
				iDeviceClass* dc = iDeviceClass::getDeviceInstanceByUdid(lpudid);
				if (dc != NULL)
				{
					if ((ret = dc->connect()) == ERROR_SUCCESS)
					{
						std::vector<STRING> info = dc->getDeviceANumber();
						// dump info 
						for each(auto s in info)
						{
							_tprintf("%s\r\n", s.c_str());
							fflush(stdout);
						}
						
						dc->disconnect();
						System::Console::WriteLine("\nTrustError=false");
						ret = 0;
					}
					else
					{
						logItcli(String::Format("Fail to connect iDevice: error code {0}", ret.ToString("X8")));
						if (((DWORD)ret == 0xe800001a) || ((DWORD)ret == 0xe800001c))
						{
							dc->GetInfoWithoutTrust();
							if (!dc->checkTrust())
							{
								// from ios 7 iphone need trust confirm.
								logItcli(String::Format("Fail to connect iDevice: please trust the connection"));
								System::Console::WriteLine("TrustError=true");
								ret = iErrorTrust;
							}
							else
							{
								logItcli(String::Format("Fail to connect iDevice: please unlock the connection"));
								System::Console::WriteLine("LockError=True");
								ret = iPSWLock;
							}
							ret = iErrorTrust;
						}
					}

				}
				else
				{
					logItcli("udid is empty", false);
				}
			}
			else
				logItcli("udid is not ready", false);

		}
		iDeviceClass::stop();
	}
	else
		logItcli("udid is not found", false);
	logItcli(String::Format("getDeviceInfo: -- {0}", ret), false);
	return ret;
}