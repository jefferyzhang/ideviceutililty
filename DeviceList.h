#pragma once

using namespace std;
class CDeviceList
{
public:
	CDeviceList();
	~CDeviceList();
public:
	void dump_device();
	void add_device(STRING device, HANDLE h);
	BOOL check_device(STRING device);
	void remove_device(STRING device);
	HANDLE get_device(STRING udid);
	void PrintList();

	STRING getAnyOne();

	map<STRING, HANDLE> device_list;
private:
	CRITICAL_SECTION m_CriticalSection;
};