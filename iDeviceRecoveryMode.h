#pragma once

typedef struct _tagReoveryDevice
{
public:
	INT64 id_current;
	HANDLE ptr_curdev;
}ReoveryDevice, *PReoveryDevice;


struct irecv_device {
	const char* product_type;
	const char* hardware_model;
	unsigned int board_id;
	unsigned int chip_id;
	const char* display_name;
};
typedef struct irecv_device* irecv_device_t;

class iDeviceRecoveryMode
{
public:
	iDeviceRecoveryMode();
	~iDeviceRecoveryMode();

	static int EnterRecoveryMode(STRING id, int delay);
	static int ExitRecoveryMode(INT64 id, int delay);
	static int getDeviceRecoveryInfo(INT64 id, int delay);
	static int ExitRestoreMode(STRING srnm, int delay);
private:
	static int getDeviceInfo(HANDLE device);
	static STRING getPhoneModel(int chipid, int broadid);

public:
	static ReoveryDevice RcoveryDev;
	static STRING sDeviceid;
	static int ExitRecoveryError;
};

