/********************************************************************
created:	2015/09/28
created:	28:9:2015   10:03
filename: 	J:\Works\iDeviceUtilCore\iDeviceUtilCore\UtilTools.cpp
file path:	J:\Works\iDeviceUtilCore\iDeviceUtilCore
file base:	UtilTools
file ext:	cpp
author:		Jeffery Zhang

purpose:	some tools
*********************************************************************/
#include "StdAfx.h"
#include <CoreFoundation/CFString.h>
#include <CoreFoundation/CFNumber.h>
#include <CoreFoundation/CFPropertyList.h>
#include <CoreFoundation/CFURLAccess.h>
#include "UtilTools.h"
#include <iostream>
#include <time.h>
#include "resource.h"
#include "iDeviceUtil.h"

#include <sstream>

STRING ConvertDataA(CFArrayRef val);

#define SSTR( x ) dynamic_cast< std::ostringstream & >( \
	( std::ostringstream() /*<< std::dec*/ << x ) ).str()

CUtilTools::CUtilTools(void)
{
}


CUtilTools::~CUtilTools(void)
{
}

static const std::string base64_chars =
"ABCDEFGHIJKLMNOPQRSTUVWXYZ"
"abcdefghijklmnopqrstuvwxyz"
"0123456789+/";


static inline bool is_base64(unsigned char c) {
	return (isalnum(c) || (c == '+') || (c == '/'));
}

std::string CUtilTools::base64_encode(unsigned char const* bytes_to_encode, unsigned int in_len) {
	std::string ret;
	int i = 0;
	int j = 0;
	unsigned char char_array_3[3];
	unsigned char char_array_4[4];

	while (in_len--) {
		char_array_3[i++] = *(bytes_to_encode++);
		if (i == 3) {
			char_array_4[0] = (char_array_3[0] & 0xfc) >> 2;
			char_array_4[1] = ((char_array_3[0] & 0x03) << 4) + ((char_array_3[1] & 0xf0) >> 4);
			char_array_4[2] = ((char_array_3[1] & 0x0f) << 2) + ((char_array_3[2] & 0xc0) >> 6);
			char_array_4[3] = char_array_3[2] & 0x3f;

			for (i = 0; (i <4); i++)
				ret += base64_chars[char_array_4[i]];
			i = 0;
		}
	}

	if (i)
	{
		for (j = i; j < 3; j++)
			char_array_3[j] = '\0';

		char_array_4[0] = (char_array_3[0] & 0xfc) >> 2;
		char_array_4[1] = ((char_array_3[0] & 0x03) << 4) + ((char_array_3[1] & 0xf0) >> 4);
		char_array_4[2] = ((char_array_3[1] & 0x0f) << 2) + ((char_array_3[2] & 0xc0) >> 6);
		char_array_4[3] = char_array_3[2] & 0x3f;

		for (j = 0; (j < i + 1); j++)
			ret += base64_chars[char_array_4[j]];

		while ((i++ < 3))
			ret += '=';

	}

	return ret;

}

std::string CUtilTools::base64_decode(std::string const& encoded_string) {
	int in_len = encoded_string.size();
	int i = 0;
	int j = 0;
	int in_ = 0;
	unsigned char char_array_4[4], char_array_3[3];
	std::string ret;

	while (in_len-- && (encoded_string[in_] != '=') && is_base64(encoded_string[in_])) {
		char_array_4[i++] = encoded_string[in_]; in_++;
		if (i == 4) {
			for (i = 0; i <4; i++)
				char_array_4[i] = base64_chars.find(char_array_4[i]);

			char_array_3[0] = (char_array_4[0] << 2) + ((char_array_4[1] & 0x30) >> 4);
			char_array_3[1] = ((char_array_4[1] & 0xf) << 4) + ((char_array_4[2] & 0x3c) >> 2);
			char_array_3[2] = ((char_array_4[2] & 0x3) << 6) + char_array_4[3];

			for (i = 0; (i < 3); i++)
				ret += char_array_3[i];
			i = 0;
		}
	}

	if (i) {
		for (j = i; j <4; j++)
			char_array_4[j] = 0;

		for (j = 0; j <4; j++)
			char_array_4[j] = base64_chars.find(char_array_4[j]);

		char_array_3[0] = (char_array_4[0] << 2) + ((char_array_4[1] & 0x30) >> 4);
		char_array_3[1] = ((char_array_4[1] & 0xf) << 4) + ((char_array_4[2] & 0x3c) >> 2);
		char_array_3[2] = ((char_array_4[2] & 0x3) << 6) + char_array_4[3];

		for (j = 0; (j < i - 1); j++) ret += char_array_3[j];
	}

	return ret;
}

std::string CUtilTools::UnixTimeToStr(__int64 ll)
{
	struct tm *tm = NULL;
	errno_t err = gmtime_s(tm, &ll);
	if (err != ERROR_SUCCESS)
	{
		return _T("");
	}
	char date[200] = { 0 };
	strftime(date, sizeof(date), "%Y-%m-%dT%H:%M:%S.%Z", tm);
	return std::string(date);
}


BOOL CUtilTools::IsDirectory(const char *pDir)
{
	char szCurPath[500];
	ZeroMemory(szCurPath, 500);
	sprintf_s(szCurPath, 500, "%s//*", pDir);
	WIN32_FIND_DATAA FindFileData;
	ZeroMemory(&FindFileData, sizeof(WIN32_FIND_DATAA));

	HANDLE hFile = FindFirstFileA(szCurPath, &FindFileData); /**< find first file by given path. */

	if (hFile == INVALID_HANDLE_VALUE)
	{
		FindClose(hFile);
		return FALSE; /** 如果不能找到第一个文件，那么没有目录 */
	}
	else
	{
		FindClose(hFile);
		return TRUE;
	}

}

BOOL CUtilTools::DeleteDirectory(const char * DirName)
{
	//	CFileFind tempFind;		//声明一个CFileFind类变量，以用来搜索
	char szCurPath[MAX_PATH];		//用于定义搜索格式
	_snprintf_s(szCurPath, MAX_PATH, "%s\\*.*", DirName);	//匹配格式为*.*,即该目录下的所有文件
	WIN32_FIND_DATAA FindFileData;
	ZeroMemory(&FindFileData, sizeof(WIN32_FIND_DATAA));
	HANDLE hFile = FindFirstFileA(szCurPath, &FindFileData);
	BOOL IsFinded = TRUE;
	while (IsFinded)
	{
		IsFinded = FindNextFileA(hFile, &FindFileData);	//递归搜索其他的文件
		if (strcmp(FindFileData.cFileName, ".") && strcmp(FindFileData.cFileName, "..")) //如果不是"." ".."目录
		{
			string strFileName = "";
			strFileName = strFileName + DirName + "\\" + FindFileData.cFileName;
			string strTemp;
			strTemp = strFileName;
			if (IsDirectory(strFileName.c_str())) //如果是目录，则递归地调用
			{
				printf("目录为:%s\n", strFileName.c_str());
				DeleteDirectory(strTemp.c_str());
			}
			else
			{
				DeleteFileA(strTemp.c_str());
			}
		}
	}
	FindClose(hFile);

	BOOL bRet = RemoveDirectoryA(DirName);
	if (bRet == 0) //删除目录
	{
		printf("删除%s目录失败！\n", DirName);
		return FALSE;
	}
	return TRUE;
}

int CUtilTools::Split(const STRING& str, vector<STRING>& ret_, STRING sep)
{
	if (str.empty())
	{
		return 0;
	}

	STRING tmp;
	STRING::size_type pos_begin = str.find_first_not_of(sep);
	STRING::size_type comma_pos = 0;

	while (pos_begin != STRING::npos)
	{
		comma_pos = str.find(sep, pos_begin);
		if (comma_pos != STRING::npos)
		{
			tmp = str.substr(pos_begin, comma_pos - pos_begin);
			pos_begin = comma_pos + sep.length();
		}
		else
		{
			tmp = str.substr(pos_begin);
			pos_begin = comma_pos;
		}

		if (!tmp.empty())
		{
			ret_.push_back(tmp);
			tmp.clear();
		}
	}
	return 0;
}

wchar_t *CUtilTools::Convert_Char2Wchar(const char *str_utf8, UINT codepage)
{
	wchar_t *str_w = NULL;

	if (str_utf8) {
		int str_w_len = MultiByteToWideChar(codepage, MB_ERR_INVALID_CHARS,
			str_utf8, -1, NULL, 0);
		if (str_w_len > 0) {
			str_w = (wchar_t *)malloc((str_w_len + 1) * sizeof(wchar_t));
			if (str_w) {
				if (MultiByteToWideChar(codepage, 0, str_utf8, -1, str_w,
					str_w_len) == 0) {
					free(str_w);
					return NULL;
				}
			}
		}
	}

	return str_w;
}

char *CUtilTools::Convert_Wchar2Char(const wchar_t *str_w, UINT codepage)
{
	char *str_utf8 = NULL;

	if (str_w) {
		int str_utf8_len = WideCharToMultiByte(codepage, 0, str_w, -1, NULL,
			0, NULL, NULL);
		if (str_utf8_len > 0) {
			str_utf8 = (char *)malloc((str_utf8_len + 1) * sizeof(wchar_t));
			if (str_utf8) {
				if (WideCharToMultiByte(codepage, 0, str_w, -1, str_utf8, str_utf8_len,
					NULL, FALSE) == 0) {
					free(str_utf8);
					return NULL;
				}
			}
		}
	}

	return str_utf8;
}

//多字节转Utf8
std::string CUtilTools::Convert_MBToUTF8(const char* pmb, size_t mLen)
{
	// convert an MBCS string to widechar   
	std::string sRet;
	int nLen = MultiByteToWideChar(CP_ACP, 0, pmb, (int)mLen, NULL, 0);

	WCHAR* lpszW = NULL;
	try
	{
		lpszW = new WCHAR[nLen];
	}
	catch (bad_alloc)
	{
		return sRet;
	}

	int nRtn = MultiByteToWideChar(CP_ACP, 0, pmb, mLen, lpszW, nLen);

	if (nRtn != nLen)
	{
		delete[] lpszW;
		return false;
	}
	// convert an widechar string to utf8  
	int utf8Len = WideCharToMultiByte(CP_UTF8, 0, lpszW, nLen, NULL, 0, NULL, NULL);
	if (utf8Len <= 0)
	{
		return sRet;
	}

	char* pu8 = NULL;
	try
	{
		pu8 = new char[utf8Len+1];
	}
	catch (bad_alloc)
	{
		return sRet;
	}
	nRtn = WideCharToMultiByte(CP_UTF8, 0, lpszW, nLen, pu8, utf8Len, NULL, NULL);
	delete[] lpszW;
	*(pu8 + utf8Len) = '\0';

	sRet.assign(pu8, nRtn);

	return sRet;
}

STRING ConvertDataS(CFStringRef val)
{
	STRING sV;
	if (val == NULL) return sV;
	CFIndex length = CFStringGetLength(val);
	CFIndex maxSize =
		CFStringGetMaximumSizeForEncoding(length,
		kCFStringEncodingUTF8);
	char *buffer = (char *)malloc(maxSize);
	if (NULL != buffer)
	{
		if (CFStringGetCString(val, buffer, maxSize, kCFStringEncodingUTF8))
		{
			sV.append(buffer);
		}
		SAFE_DELETE(buffer);
	}
	return sV;
}

STRING ConvertDataN(CFNumberRef val)
{
	STRING sVal;
	if (val == NULL) return sVal;
	void* buffer = malloc(CFNumberGetByteSize((CFNumberRef)val));
	Boolean scs = CFNumberGetValue((CFNumberRef)val, CFNumberGetType((CFNumberRef)val), buffer);
	if (scs)
	{
		int type = (int)CFNumberGetType((CFNumberRef)val);
		/*
		kCFNumberSInt8Type = 1,
		kCFNumberSInt16Type = 2,
		kCFNumberSInt32Type = 3,
		kCFNumberSInt64Type = 4,
		kCFNumberFloat32Type = 5,
		kCFNumberFloat64Type = 6,
		*/
		switch (type)
		{
		case kCFNumberSInt8Type:
		{
			BYTE ba = BYTE(*(BYTE*)buffer);
			sVal = SSTR(ba);
		}
		break;
		case kCFNumberSInt16Type:
		{
			INT16 a16 = INT16(*(INT16*)buffer);
			sVal = SSTR(a16);
		}
		break;
		case kCFNumberSInt32Type:
		{
			INT32 a32 = INT32(*(INT32*)buffer);
			sVal = SSTR(a32);
		}
		break;
		case kCFNumberSInt64Type:
		{
			INT64 a64 = INT64(*(INT64*)buffer);
			sVal = SSTR(a64);
		}
		break;
		case kCFNumberFloat32Type:
		{
			Float32 af32 = Float32(*(Float32*)buffer);
			int prec = numeric_limits<double>::digits10; // 18
			ostringstream out;
			out.precision(prec);//覆盖默认精度
			out << af32;
			sVal = out.str();
			//sVal = SSTR(af32);
		}
		break;
		case kCFNumberFloat64Type:
		{
			Float64 af64 = Float64(*(Float64*)buffer);
			int prec = numeric_limits<long double>::digits10; // 18
			ostringstream out;
			out.precision(prec);
			out << af64;
			sVal = out.str();
			/*UNIX time convert String.
			struct tm tm;
			char s[100];
			time_t tick = af64;
			tm = *localtime(&tick);
			strftime(s, sizeof(s), "%Y-%m-%d %H:%M:%S", &tm);
			*/
			//sVal =  SSTR(af64); 
		}
		break;
		case kCFNumberCharType:
		{
			char ac = char(*(char*)buffer);
			sVal = SSTR(ac);
		}
		break;
		case kCFNumberShortType:
		{
			short as = short(*(short*)buffer);
			sVal = SSTR(as);
		}
		break;
		case kCFNumberIntType:
		{
			int ai = int(*(int*)buffer);
			sVal = SSTR(ai);
		}
		break;
		case kCFNumberLongType:
		{
			long al = long(*(long*)buffer);
			sVal = SSTR(al);
		}
		break;
		case kCFNumberLongLongType:
		{
			LONGLONG all = LONGLONG(*(LONGLONG*)buffer);
			sVal = SSTR(all);
		}
		break;
		case kCFNumberFloatType:
		{
			float af = float(*(float*)buffer);
			sVal = SSTR(af);
		}
		break;
		case kCFNumberDoubleType:
		{
			double ad = double(*(double*)buffer);
			sVal = SSTR(ad);
		}
		break;
		default:
			break;
		}
	}

	return sVal;
}

STRING ConvertDataB(CFBooleanRef val)
{
	STRING sV;
	if (val == NULL) return sV;
	Boolean bval = CFBooleanGetValue((CFBooleanRef)val);
	sV.append(bval ? _T("true") : _T("false"));
	return sV;
}

STRING ConvertDataD(CFDataRef val)
{
	STRING sV;
	if (val == NULL) return sV;
	CFIndex dataLen = CFDataGetLength(val);
	const byte* pdata = CFDataGetBytePtr(val);
	TCHAR hexstr[10] = { 0 };
	for (CFIndex i = 0; i<dataLen; i++) {
		sprintf_s(hexstr, _T("%02X\0"), *(pdata + i));
		sV.append(hexstr);
		if (i > 8 && i + 1<dataLen)
		{
			sV.append(_T("..."));
			break;
		}
	}
	return sV;
}

map<STRING, STRING> ConvertDataDict(CFMutableDictionaryRef dict, STRING &svvv)
{
	CFShow(dict);
	map<STRING, STRING> sV;
	if (dict == NULL) return sV;
	CFDataRef pdata = CFPropertyListCreateXMLData(NULL, dict);
	svvv.assign((char *)CFDataGetBytePtr(pdata), CFDataGetLength(pdata));
	std::replace(svvv.begin(), svvv.end(), '\n', ' ');
	svvv.append("\n\n");

	CFIndex n = CFDictionaryGetCount(dict);
	void** pKeys = (void**)malloc(n*sizeof(CFTypeRef));
	void** pValues = (void**)malloc(n*sizeof(CFTypeRef));
	if (pKeys && pValues)
	{
		CFDictionaryGetKeysAndValues(dict, (const void**)pKeys, (const void**)pValues);
		for (int i = 0; i<n; i++)
		{
			char c_key[512];
			int t = CFGetTypeID(pKeys[i]);
			t = CFGetTypeID(pValues[i]);
			CFStringGetCString((CFStringRef)pKeys[i], c_key, 510, kCFStringEncodingUTF8);
			if (CFStringGetTypeID() == t)
			{
				STRING c_value = ConvertDataS((CFStringRef)pValues[i]);
				sV.insert(std::pair<STRING, STRING>(c_key, c_value));
				logIt(_T("%s=%s\n"), c_key, c_value.c_str());
			}
			else if (t == CFNumberGetTypeID())
			{
				STRING c_value = ConvertDataN((CFNumberRef)pValues[i]);
				logIt("%s=%s\n", c_key, c_value.c_str());
				sV.insert(std::pair<STRING, STRING>(c_key, c_value));
			}
			else if (t == CFBooleanGetTypeID())
			{
				STRING c_value = ConvertDataB((CFBooleanRef)pValues[i]);
				logIt("%s=%d\n", c_key, c_value.c_str());
				sV.insert(std::pair<STRING, STRING>(c_key, c_value));
			}
			else if (t == CFDictionaryGetTypeID())
			{
				STRING c_value;
				map<STRING, STRING> map_value = ConvertDataDict((CFMutableDictionaryRef)pValues[i], c_value);
				for (map<STRING, STRING>::iterator it = map_value.begin(); it != map_value.end(); ++it)
				{
					c_value.append(it->first);
					c_value.append(_T("["));
					c_value.append(it->second);
					c_value.append(_T("]"));
					c_value.append(_T("\t"));
				}
				logIt("%s=%d\n", c_key, c_value.c_str());
				sV.insert(std::pair<STRING, STRING>(c_key, c_value));
			}
			else if (t == CFArrayGetTypeID())
			{
				STRING c_value = ConvertDataA((CFArrayRef)pValues[i]);
				logIt("%s=%s\n", c_key, c_value.c_str());
				sV.insert(std::pair<STRING, STRING>(c_key, c_value));

			}
			else if (t == CFDataGetTypeID())
			{
				sV.insert(std::pair<STRING, STRING>(c_key, ConvertDataD((CFDataRef)pValues[i])));
			}
			else
				logIt("%s={value id is: %d}\n", c_key, t);
		}

		SAFE_DELETE(pKeys);
		SAFE_DELETE(pValues);
	}

	return sV;
}

STRING ConvertDataA(CFArrayRef val)
{
	STRING sV;
	if (val == NULL) return sV;
	CFIndex cntArray = CFArrayGetCount(val);
	//CFDataGetBytePtr(data), dataLen
	for (CFIndex i = 0; i < cntArray; i++)
	{
		const void* arrayindex = CFArrayGetValueAtIndex(val, i);
		CFTypeID typid = CFGetTypeID(arrayindex);
		sV.append(_T("\t"));
		if (typid == CFDictionaryGetTypeID())
		{
			map<STRING, STRING> mss = ConvertDataDict((CFMutableDictionaryRef)arrayindex, sV);
			for (map<STRING, STRING>::iterator it = mss.begin(); it != mss.end(); ++it)
			{
				sV.append(it->first);
				sV.append(_T("="));
				sV.append(it->second);
				//sV.append(_T(""));
				sV.append(_T("\n"));
			}
		}
		else if (typid == CFArrayGetTypeID())
		{
			sV.append(ConvertDataA((CFArrayRef)arrayindex));
		}
		else if (typid == CFStringGetTypeID())
		{
			sV.append(ConvertDataS((CFStringRef)arrayindex));
		}
		else if (typid == CFNumberGetTypeID())
		{
			sV.append(ConvertDataN((CFNumberRef)arrayindex));
		}
		else if (typid == CFBooleanGetTypeID())
		{
			sV.append(ConvertDataB((CFBooleanRef)arrayindex));
		}
		else if (typid == CFDataGetTypeID())
		{
			sV.append(ConvertDataD((CFDataRef)arrayindex));
		}
		else
		{
			CFShow(arrayindex);
		}
	}
	return sV;
}


STRING CUtilTools::ConvertData(CFTypeRef val)
{
	STRING sV;
	if (val ==NULL) return sV;
	if (CFStringGetTypeID() == CFGetTypeID(val))
	{
		sV = ConvertDataS((CFStringRef)val);
	}
	else if (CFBooleanGetTypeID() == CFGetTypeID(val))
	{
		sV = ConvertDataB((CFBooleanRef)val);
	}
	else if (CFDataGetTypeID() == CFGetTypeID(val))
	{
		sV = ConvertDataD((CFDataRef)val);
	}
	else if (CFDictionaryGetTypeID() == CFGetTypeID(val))
	{
		map<STRING, STRING> map_value = ConvertDataDict((CFMutableDictionaryRef)val, sV);
		
		for (map<STRING, STRING>::iterator it = map_value.begin(); it != map_value.end(); ++it)
		{
			sV.append(it->first);
			sV.append(_T("="));
			sV.append(it->second);
			//sV.append(_T(""));
			sV.append(_T("\n"));
		}
	}
	else if (CFNumberGetTypeID() == CFGetTypeID(val)){
		sV = ConvertDataN((CFNumberRef)val);
	}
	else if (CFArrayGetTypeID() == CFGetTypeID(val))
	{
		sV = ConvertDataA((CFArrayRef)val);
	}
	else{
		char buffer[MAX_PATH] = { 0 };
		_sntprintf_s(buffer, MAX_PATH, _T("%d"), val);
		sV.append(buffer);
	}
	return sV;
}

STRING& CUtilTools::trim(STRING &s)
{
	if (s.empty())
	{
		return s;
	}

	s.erase(0, s.find_first_not_of(" "));
	s.erase(s.find_last_not_of(" ") + 1);
	return s;
}