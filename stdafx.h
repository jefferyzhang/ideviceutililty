// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once


#include <SDKDDKVer.h>


#include <stdio.h>
#include <tchar.h>

#include <stdlib.h>

#include <Windows.h>
#include <shlwapi.h>
#include <string>
#include <map>
#include <iostream>
#include <algorithm>
#include <WinUser.h>
#include <vector>

#ifdef _UNICODE
#define STRING	std::wstring
#else
#define STRING	std::string
#endif
