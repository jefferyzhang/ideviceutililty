#pragma once
using namespace System;
using namespace System::Collections::Specialized;
using namespace System::Text;
using namespace System::IO;
using namespace System::Net;
using namespace System::Collections::Generic;

ref class iactive
{
public:
	iactive();
public:
	static int Deactive(String^ udid, int delay);
	static int Active(StringDictionary^ _param, int delay);
	static int ActiveV1(StringDictionary^ _param, int delay);
	static int ActiveV2(StringDictionary^ _param, int delay);
	static int CarrierLock(StringDictionary^ _param, int delay);
private:
	static StringDictionary^ InfoToDict(iDeviceClass* dc);
	static int do_active_V2(BOOL verbose, String^ sUrl, StringDictionary^ sd, BOOL bFirst);
	static String^ makeCarrierLockDataFile(StringBuilder^ sb, StringDictionary^ sdict);
	static StringCollection^ getUrlsFromConfig(String^ sUrlparam);
	static int do_access_apple(String^ sHost, StringDictionary^ sd, StringBuilder^ sb, BOOL verbose, BOOL bFirst);
	static int CheckActiveReturn(String^ s, String^ sUrl, StringDictionary^ infos, BOOL verbose, BOOL bFirst);
	static int doReturnPlist(String^ sUrl, BOOL verbose, StringDictionary^ infos, String^ sPlist, BOOL bFirst);
	static int do_active_stage_2(String^ ticketFilename, BOOL verbose, StringDictionary^ sDict);
	static int do_active_stage_3(String^ ticketFilename, BOOL verbose);
	static int do_active_V3(BOOL verbose, String^ sUrl, StringDictionary^ sd, BOOL bFirst);
	static int do_active_V4(BOOL verbose, String^ sUrl, StringDictionary^ sd, BOOL bFirst);
	static int do_active_Carrier(BOOL verbose, String^ sUrl, StringDictionary^ sd, BOOL bFirst);
public://for test
	static String^ transfer_encoding_chunked_recv(StreamReader^ reader);
	static String^ ParseHTML_controllerConfig(String^ htmldoc);
	static String^ ParseHTML_Refresh_Next(String^ ss);
	static int do_send_active_Redirect(array<byte>^ data, StringBuilder^ headers, StringBuilder^ dataList, BOOL verbose);
	static int http_send_poll(String^ url, StringBuilder^ headers, StringBuilder^ shtml, BOOL verbose);
	static int http_get_refreshAction(String^ url, StringBuilder^ headers, StringBuilder^ shtml, BOOL verbose);
	static int do_send_carrierstatus(array<byte>^ data, StringBuilder^ headers, StringBuilder^ dataList, BOOL verbose);
public:
	static int do_session_app(String^ sHost, StringBuilder^ sb, StringBuilder^ sbret, BOOL verbose);
private:
	static int do_send_active_info(array<byte>^ data, StringBuilder^ headers, StringBuilder^ dataList, BOOL verbose);
	static int do_send_active_info_FMI(StringBuilder^ headers, StringBuilder^ dataList, BOOL verbose);
	static int do_send_active_info_FMI(StringDictionary^ sd, BOOL verbose);
	static array<byte>^  getHttpBody2(array<byte>^ data, String^ boundary);
	static array<byte>^  getHttpBody3(Dictionary<String^, String^>^ sdPostItems);
	static int CheckActiveReturnV1(String^ s, StringBuilder^ retplist, BOOL verbose);

	static String^ ParseHTML_controllerConfig(HtmlAgilityPack::HtmlDocument^ htmldoc);
	static String^ ParseHTML_PlistData(HtmlAgilityPack::HtmlDocument^ htmldoc);
	static Tuple<int, String^, String^, String^>^ ParserHtml(String^ sHtml);
	static Tuple<int, String^, String^>^ postdatatoServer(String^ url, String^ postdata, String^ referurl, StringBuilder^ headers);
	static int Parse_Error(HtmlAgilityPack::HtmlDocument^ htmldoc);

	static String^  useName;
	static String^  passWord;
	static Dictionary<String^, String^>^	fmiResponeItem;

	static int RequestTimeOut = 300 * 1000;
public:
	//for redirect
	static String^	sReferer;

	static String^ UserAgent = "iTunes/12.11 (Windows; Microsoft Windows 10 Professional Edition (Build 19041)) AppleWebKit/7610.2011.1003.3 (dt:2)";
	static CookieContainer^ CContainer = gcnew CookieContainer();
	static BOOL verbose = false;
};

