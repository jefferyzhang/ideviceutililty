// iDeviceUtility.cpp : main project file.

#include "stdafx.h"
#include "iDeviceUtil.h"
#include "iDeviceClass.h"
#include "iDeviceRecoveryMode.h"
#include "DeviceInfo.h"
#include "iactive.h"
#include "RunExe.h"


using namespace System;
using namespace System::Web;
using namespace System::Web::Script::Serialization;

static void usage()
{
	System::Console::WriteLine("iDeviceUtil Command:");
	System::Console::WriteLine(" -delay=3; delay 3 seconds");
	System::Console::WriteLine(" -list; list udid of iphone");
	System::Console::WriteLine(" -info -udid=...; get iphone information, -igorermn   igore info read  RegulatoryModelNumber via activation");
	System::Console::WriteLine(" -ainfo -udid=...; get iphone ANumber information");
	System::Console::WriteLine(" -enterrecoverymode -id=xxx; enter recovery mode, id can be udid or ecid");
	System::Console::WriteLine(" -active -udid=...  [-login=email -password=]; active iphone");
}

char * get_apple_support_path(char* module)
{
	char* ret = NULL;
	if (module != NULL)
	{
		HKEY hKey;
		char buf[1024];
		sprintf_s(buf, "SOFTWARE\\Apple Inc.\\%s", module);
		if (RegOpenKeyEx(HKEY_LOCAL_MACHINE, buf, 0, KEY_READ, &hKey) == ERROR_SUCCESS)
		{
			DWORD size = MAX_PATH;
			ZeroMemory(buf, sizeof(buf));
			if (RegQueryValueEx(hKey, "InstallDir", NULL, NULL, (LPBYTE)buf, &size) == ERROR_SUCCESS)
			{
				ret = _strdup(buf);
			}
			RegCloseKey(hKey);
		}
	}
	return ret;
}

void set_path()
{
	char *p;
	size_t len;
	errno_t err = _dupenv_s(&p, &len, "PATH");
	if (err) return;
	//free(tmp);

	int nLen = strlen(p) + 2 * MAX_PATH;
	char *pp = (char*)malloc(nLen);
	if (pp != NULL)
	{
		strcpy_s(pp, nLen, "PATH=");
		strcat_s(pp, nLen, p);
		char* pAppleApplicationSupport = get_apple_support_path("Apple Application Support");
		logIt("Apple Application Support=%s\n", pAppleApplicationSupport);
		char* pAppleMobileDeviceSupport = get_apple_support_path("Apple Mobile Device Support");
		logIt("Apple Mobile Device Support=%s\n", pAppleMobileDeviceSupport);
		if (pAppleApplicationSupport != NULL)
		{
			g_args.m_sPathAASupport.assign(pAppleApplicationSupport);
			strcat_s(pp, nLen, ";");
			strcat_s(pp, nLen, pAppleApplicationSupport);
			SAFE_FREE(pAppleApplicationSupport);
		}
		if (pAppleMobileDeviceSupport != NULL)
		{
			g_args.m_sPathMDSupport.assign(pAppleMobileDeviceSupport);
			strcat_s(pp, nLen, ";");
			strcat_s(pp, nLen, pAppleMobileDeviceSupport);
			SAFE_FREE(pAppleMobileDeviceSupport);
		}
		_putenv(pp);
		SAFE_FREE(pp);
	}
	SAFE_FREE(p);
}

LONG WINAPI my_UnhandledExceptionFilter(struct _EXCEPTION_POINTERS* ExceptionInfo)
{
	logIt(_T("iDeviceUtil.exe. exceptionCode: %x, exceptionAddress: %x \n"), ExceptionInfo->ExceptionRecord->ExceptionCode, ExceptionInfo->ExceptionRecord->ExceptionAddress);
	return EXCEPTION_EXECUTE_HANDLER;
}

void logItcli(String^ s, BOOL b){
	if (b) Console::WriteLine(s);
	System::Diagnostics::Trace::WriteLine(s);
}

void logIt(System::String^ msg)
{
	System::Diagnostics::Trace::WriteLine(msg);
}

void logIt(
	String^ format,
	... cli::array<Object^>^ arg
	)
{
	String^ msg = String::Format(format, arg);
	System::Diagnostics::Trace::WriteLine(msg);
}

BOOL bNoProxy = false;
int main(array<System::String ^> ^args)
{
	int ret = 87;
	SetUnhandledExceptionFilter(my_UnhandledExceptionFilter);
	set_path();

	try
	{
		int _delay = 30 * 1000;
		System::Configuration::Install::InstallContext^ _param =gcnew System::Configuration::Install::InstallContext(nullptr, args);
		if (_param->IsParameterTrue("debug"))
		{
			System::Console::WriteLine("Wait for attach, press any key to continue...");
			System::Console::ReadKey();
		}
		
		if (_param->Parameters->ContainsKey("delay"))
		{
			int a;
			if (Int32::TryParse(_param->Parameters["delay"], a))
			{
				_delay = a * 1000;
			}
		}

		logItcli(String::Format("{0} start: ++ version: {1}",
			System::IO::Path::GetFileName(System::Diagnostics::Process::GetCurrentProcess()->MainModule->FileName),
			System::Diagnostics::Process::GetCurrentProcess()->MainModule->FileVersionInfo->FileVersion), false);
		logItcli("Dump parameters:", false);

		for each(String^ key in _param->Parameters->Keys)
		{
			logItcli(String::Format("{0} = {1}", key, _param->Parameters[key]), false);
		}
		if (_param->IsParameterTrue("noproxy"))
		{
			bNoProxy = true;
		}
		if (_param->IsParameterTrue("verbose"))
		{
			extern PARAMETERS_T g_args;
			g_args.bVerbose = TRUE;
		}

		bool bCalcUdid = _param->IsParameterTrue("calcudid");
		if (_param->Parameters->ContainsKey("udid")){
			int extocde = -1;
			String^ sudid = _param->Parameters["udid"];
			if (sudid->Length < 40) {
				String^ sPath = System::AppDomain::CurrentDomain->BaseDirectory;
				sPath = Path::Combine(sPath, "iDeviceRealUDID.exe");
				if (File::Exists(sPath))
				{
					String^ sParam = String::Format("-udid={0} {1}", _param->Parameters["udid"], bCalcUdid?"-calcudid":"");//Path::Combine(System::AppDomain::CurrentDomain->BaseDirectory, "Activation.exe")
					logItcli(sParam);
					List<String^>^ retss = CRunExe::runExe(sPath, sParam, extocde, 180 * 1000, false);
					for each (String^ s in retss)
					{
						if (s->StartsWith(_param->Parameters["udid"] + "=")) {
							_param->Parameters["udid"] = s->Replace(_param->Parameters["udid"] + "=", "");
							break;
						}
					}
				}
			}
		}
		else if (_param->IsParameterTrue("enterrecoverymode")){
			if (_param->Parameters->ContainsKey("id")){
				int extocde = -1;
				String^ sPath = System::AppDomain::CurrentDomain->BaseDirectory;
				sPath = Path::Combine(sPath, "iDeviceRealUDID.exe");
				if (File::Exists(sPath))
				{
					//String^ sParam = String::Format("-udid={0}", _param->Parameters["id"]);//Path::Combine(System::AppDomain::CurrentDomain->BaseDirectory, "Activation.exe")
					String^ sParam = String::Format("-udid={0} {1}", _param->Parameters["udid"], bCalcUdid ? "-calcudid" : "");
					logItcli(sParam);
					List<String^>^ retss = CRunExe::runExe(sPath, sParam, extocde, 180 * 1000, false);
					for each (String^ s in retss)
					{
						if (s->StartsWith(_param->Parameters["id"] + "=")){
							_param->Parameters["id"] = s->Replace(_param->Parameters["id"] + "=", "");
							break;
						}
					}
				}
			}
		}

		// start check command
		if (_param->IsParameterTrue("list"))
		{
			if (_param->Parameters->ContainsKey("udid"))
			{
				using namespace Runtime::InteropServices;
				const char* lpudid =
					(const char*)(Marshal::StringToHGlobalAnsi(_param->Parameters["udid"]).ToPointer());
				iDeviceClass::start(lpudid);
				if (iDeviceClass::idDeviceReady(_delay))
				{
					System::Console::WriteLine(_param->Parameters["udid"]);
				}
				iDeviceClass::stop();
			}
			else
			{
				iDeviceClass::start("");
				iDeviceClass::idDeviceReady(_delay);
				std::vector<STRING> devices = iDeviceClass::getUdids();
				for (std::vector<STRING>::iterator it = devices.begin(); it != devices.end(); ++it) {
					//_tprintf(it->c_str()); _tprintf("\n");
					std::cout << *it << std::endl;
					//fflush(stdout);
				}
				iDeviceClass::stop();
			}
			ret = 0;
		}
		else if (_param->IsParameterTrue("ainfo"))
		{
			if (_param->Parameters->ContainsKey("udid"))
			{
				ret = DeviceInfo::getDeviceANumber(_param);
			}
			else
				logIt("parameter does not include udid");
		}
		else if (_param->IsParameterTrue("info"))
		{
			if (_param->Parameters->ContainsKey("udid"))
			{
				ret = DeviceInfo::getDeviceInfo(_param);
			}
			else if (_param->Parameters->ContainsKey("id"))
			{
				ret = DeviceInfo::getDeviceRecoveryInfo(_param);
			}
			else
				logIt("parameter does not include udid");
		}
		else if (_param->IsParameterTrue("infobundle"))
		{
			if (_param->Parameters->ContainsKey("udid"))
			{
				ret = DeviceInfo::getDeviceInfoBundle(_param);
			}
			else
				logIt("parameter does not include udid");
		}
		else if (_param->IsParameterTrue("infodetect"))
		{
			if (_param->Parameters->ContainsKey("udid"))
			{
				ret = DeviceInfo::getDeviceInfoDetect(_param);
			}
			else
				logIt("parameter does not include udid");
		}
		else if (_param->IsParameterTrue("enterrecoverymode"))
		{
			if (_param->Parameters->ContainsKey("id"))
			{
				using namespace Runtime::InteropServices;
				const char* lpudid =
					(const char*)(Marshal::StringToHGlobalAnsi(_param->Parameters["id"]).ToPointer());
				ret = iDeviceRecoveryMode::EnterRecoveryMode(lpudid, _delay);
				ret = iDeviceRecoveryMode::ExitRecoveryError;
			}
			else
				logIt("parameter does not include id");
		}
		else if (_param->IsParameterTrue("exitrecoverymode"))
		{
			if (_param->Parameters->ContainsKey("id"))
			{
				//ret = iDeviceRecoveryMode.ExitRecoveryMode(_param.Parameters["id"], _delay);
				INT64 nn(0);
				if (INT64::TryParse(_param->Parameters["id"], System::Globalization::NumberStyles::AllowHexSpecifier, System::Globalization::CultureInfo::InvariantCulture, nn))
				{
					ret = iDeviceRecoveryMode::ExitRecoveryMode(nn, _delay);
				}
				else
					ret = ERROR_INVALID_PARAMETER;

			}
			else
				logIt("parameter does not include id");
		}
		else if (_param->IsParameterTrue("exitrestoremode"))
		{
			if (_param->Parameters->ContainsKey("srnm"))
			{
				using namespace Runtime::InteropServices;
				const char* lpsrnm =
					(const char*)(Marshal::StringToHGlobalAnsi(_param->Parameters["srnm"]).ToPointer());
				ret = iDeviceRecoveryMode::ExitRestoreMode(lpsrnm, _delay);
			}
			else
				logIt("parameter does not include srnm");
		}
		else if (_param->IsParameterTrue("activev1"))
		{
			String^ path = System::IO::Path::Combine(System::AppDomain::CurrentDomain->BaseDirectory, "HtmlAgilityPack.dll");
			if (!System::IO::File::Exists(path))
			{
				logItcli(path + "is not exist! install please!!");
			}

			if (_param->Parameters->ContainsKey("udid"))
			{
				ret = iactive::Active(_param->Parameters, _delay);
			}
			else
				logIt("parameter does not include udid");
		}
		else if (_param->IsParameterTrue("active"))
		{
			String^ path = System::IO::Path::Combine(System::AppDomain::CurrentDomain->BaseDirectory, "HtmlAgilityPack.dll");
			if (!System::IO::File::Exists(path))
			{
				logItcli(path + "is not exist! install please!!");
			}

			if (_param->Parameters->ContainsKey("udid"))
			{
//#define iErrorSelectCarrier 102
//#define iErrorDemoPhone 103
//#define iErrorProblem	104
				ret =iactive::ActiveV2(_param->Parameters, _delay);
				if (ret == iErrorAlreadyActive || ret == 0 || ret == iCloudLock ||
					ret == iErrorSIM || ret == iErrorTrust || ret == iErrorSelectCarrier || ret == iErrorDemoPhone || ret == iErrorProblem) {
					return ret;
				}
				Sleep(5000);
				ret = iactive::ActiveV1(_param->Parameters, _delay);
			}
			else
				logIt("parameter does not include udid");
		}
		else if (_param->IsParameterTrue("carrier")) 
		{
			String^ path = System::IO::Path::Combine(System::AppDomain::CurrentDomain->BaseDirectory, "HtmlAgilityPack.dll");
			if (!System::IO::File::Exists(path))
			{
				logItcli(path + "is not exist! install please!!");
			}

			if (_param->Parameters->ContainsKey("udid"))
			{
				ret = iactive::CarrierLock(_param->Parameters, _delay);
			}
			else
				logIt("parameter does not include udid");
		}
		else if (_param->IsParameterTrue("activev2"))
		{
			String^ path = System::IO::Path::Combine(System::AppDomain::CurrentDomain->BaseDirectory, "HtmlAgilityPack.dll");
			if (!System::IO::File::Exists(path))
			{
				logItcli(path + "is not exist! install please!!");
			}

			if (_param->Parameters->ContainsKey("udid"))
			{
				ret = iactive::ActiveV2(_param->Parameters, _delay);
			}
			else
				logIt("parameter does not include udid");
		}
		else if (_param->IsParameterTrue("deactive"))
		{
			if (_param->Parameters->ContainsKey("udid"))
			{
				ret = iactive::Deactive(_param->Parameters["udid"], _delay);
			}
			else
				logIt("parameter does not include udid");
		}
		else
		{
			System::Console::WriteLine("wrong parameters.");
			usage();
		}
	}
	catch (Exception^ ex)
	{
		logItcli(String::Format("some exception happen - {0}", ex->ToString()));
	}
	return ret;

}
