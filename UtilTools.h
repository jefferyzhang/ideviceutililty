#pragma once
#include <string>
#include <vector>
using namespace std;
#include <CoreFoundation/CFBase.h>
#include <CoreFoundation/CFString.h>

class CUtilTools
{
public:
	CUtilTools(void);
	~CUtilTools(void);

	static std::string base64_encode(unsigned char const*, unsigned int len);
	static std::string base64_decode(std::string const& s);
	static BOOL IsDirectory(const char *pDir);
	static BOOL DeleteDirectory(const char * DirName);

	static 	int Split(const STRING& str, vector<STRING>& ret_, STRING sep = _T(","));
	static  wchar_t* Convert_Char2Wchar(const char *str_utf8, UINT codepage = CP_UTF8);//need free return string
	static  char* Convert_Wchar2Char(const wchar_t *str_w, UINT codepage = CP_UTF8);//need free return string
	static std::string CUtilTools::Convert_MBToUTF8(const char* pmb, size_t mLen);

	static std::string UnixTimeToStr(__int64 ll);
	static std::string ConvertData(CFTypeRef s);

	static STRING& trim(STRING &s);

};

#include <cstdio>
#include <string>
#include <iostream>

struct CVersion
{
	int major, minor, revision, build;

	CVersion(const STRING& version)
	{
		sscanf_s(version.c_str(), "%d.%d.%d.%d", &major, &minor, &revision, &build);
		if (major < 0) major = 0;
		if (minor < 0) minor = 0;
		if (revision < 0) revision = 0;
		if (build < 0) build = 0;
	}

	bool operator < (const CVersion& other)
	{
		if (major < other.major)
			return true;
		if (minor < other.minor)
			return true;
		if (revision < other.revision)
			return true;
		if (build < other.build)
			return true;
		return false;
	}

	bool operator >= (const CVersion& other)
	{
		if (major < other.major)
			return false;
		if (minor < other.minor)
			return false;
		if (revision < other.revision)
			return false;
		if (build < other.build)
			return false;
		return true;
	}

	bool operator == (const CVersion& other)
	{
		return major == other.major
			&& minor == other.minor
			&& revision == other.revision
			&& build == other.build;
	}

	friend std::ostream& operator << (std::ostream& stream, const CVersion& ver)
	{
		stream << ver.major;
		stream << '.';
		stream << ver.minor;
		stream << '.';
		stream << ver.revision;
		stream << '.';
		stream << ver.build;
		return stream;
	}
};