#include "stdafx.h"
#include "iTunesMobileDevice.h"
#include <time.h>

#include "UtilTools.h"
#include "iDeviceUtil.h"
#include "iDeviceClass.h"

#include "iactive.h"
using namespace System::Text;
using namespace System;
using namespace Runtime::InteropServices;


PARAMETERS_T g_args;

extern _iTunesMobileDevice_t g_iTunesMobileDevice;
HANDLE iDeviceClass::quitlisten = NULL;
CFRunLoopRef  iDeviceClass::currentLoopRef = NULL;
CurrentDevice iDeviceClass::curDevice;
std::map<STRING, HANDLE> iDeviceClass::idevices;

static void Print_dictionary_applier(const void *key, const void *value, void *context);

const char *_device_info_keys[] = {
		"DeviceColor",
		"DeviceEnclosureColor",
		"DeviceName",
		"CarrierBundleInfoArray",
		"MobileEquipmentIdentifier",
		"InternationalMobileEquipmentIdentity",
		"MLBSerialNumber",
		"MobileSubscriberCountryCode",
		"MobileSubscriberNetworkCode",
		"ModelNumber",
		"ProductType",
		"ProductVersion",
		"SIMStatus",
		"SIMTrayStatus",
		"SerialNumber",
		"TimeIntervalSince1970",
		"PhoneNumber",
		"PhoneNumber2",
		"AirplaneMode",
		"CarrierBundleInfo",
		"MarketingName",
		"RegulatoryModelNumber",
		//"ActivationState",
		//"BasebandBootloaderVersion",
		//"BasebandStatus",
		//"BasebandVersion",
		//"BluetoothAddress",
		//"BuildVersion",
		//"CPUArchitecture",
		//"DeviceClass",
		//"DieID",
		//"CFBundleIdentifier",
		//"FirmwareVersion",
		//"HardwareModel",
		//"HardwarePlatform",
		//"HostAttached",
		//"IMLockdownEverRegisteredKey",
		//"PartitionType",
		//"PasswordProtected",
		//"ProductionSOC",
		//"ProtocolVersion",
		//"RegionInfo",
		//"SBLockdownEverRegisteredKey",
		//"SoftwareBehavior",
		// "SoftwareBundleVersion",
		//"TelephonyCapability",
		//"TimeZone",
		//"TrustedHostAttached",
		//"UniqueChipID",
		//"UniqueDeviceID",
		//"UseRaptorCerts",
		//"ActivityURL",
		//"Uses24HourClock",
		//"WiFiAddress",
		//"WirelessBoardSerialNumber",
		//"IntegratedCircuitCardIdentity",
		//"InternationalMobileSubscriberIdentity",
		//"kCTPostponementStatus",
		//"TotalDiskCapacity",
		//"ActivationStateAcknowleX",
		//"CloudBackupEnabled",
		//"PasswordProtected",
};

iDeviceClass::iDeviceClass() :_device(NULL), wasAFC2(false), hService(NULL), hAFC(NULL)
{
	
}

iDeviceClass::iDeviceClass(HANDLE h) : wasAFC2(false), hService(NULL), hAFC(NULL)
{
	_device = h;
}

iDeviceClass::~iDeviceClass()
{
	
}

void deviceNotificationCallback(AMDeviceNotificationCallbackInformationRef info, void *user)
{
	PARAMETERS_T *pThis = (PARAMETERS_T  *)user;
	logIt("deviceNotificationCallback++ %d \n", info->msgType);
	switch (info->msgType)
	{
	case ADNCI_MSG_CONNECTED:
	{
		logIt("device arrived \n");
		if (iDeviceClass::curDevice.udid_current.empty())
		{
			iDeviceClass::addDevices(info->deviceHandle);
		}
		else
		{
			iDeviceClass::addDevices(info->deviceHandle);
			iDeviceClass::waitDevice(info->deviceHandle);
		}
	}
	break;
	case ADNCI_MSG_DISCONNECTED:
	{
		if (iDeviceClass::curDevice.udid_current.empty())
		{
			STRING udid = iDeviceClass::getDeviceUdid(info->deviceHandle);
			if (iDeviceClass::idevices.find(udid)!=iDeviceClass::idevices.end())
			{
				logIt("Remove iOS device by udid: %s\n", udid.c_str());
				iDeviceClass::idevices.erase(udid);
			}
		}
	}
	break;
	case ADNCI_MSG_UNSUBSCRIBED:// 驱动被卸载时会走到这里 （该信号值未完全确定是代表Itunes环境被卸载）
		break;
	default:break;
	}
}

BOOL Register_iTunesMobileDevice_Callback()
{
	BOOL ret = TRUE;
	ENTER_FUNC_BEGIN;
	int err = g_iTunesMobileDevice.AMDeviceNotificationSubscribe(&deviceNotificationCallback, 0, 1,
		&g_args, (void **)&g_iTunesMobileDevice._notification);
	if (err != ERROR_SUCCESS)
	{
		ret = FALSE;
	}

	ENTER_FUNC_ENDRET(ret);
	return ret;
}

bool iDeviceClass::start(STRING udid)
{
	bool ret = true;

	if (!init_iTunesMobileDevice(kAMNormalNotification))
	{
		return false;
	}

	quitlisten = CreateEvent(NULL, FALSE, FALSE, NULL);
	curDevice.udid_current = udid;

	ENTER_FUNC_BEGIN;
	//RestorableDevice_Register_CallBack();
	//ret = Register_iTunesMobileDevice_Callback();
	
	int err = g_iTunesMobileDevice.AMDeviceNotificationSubscribe(&deviceNotificationCallback, 0, 0,
		&g_args, (void **)&g_iTunesMobileDevice._notification);
	if (err != ERROR_SUCCESS)
	{
		ret = false;
	}

	ENTER_FUNC_ENDRET(ret);
	return ret;
}

void iDeviceClass::stop()
{
	try
	{
		if (quitlisten != NULL)
		{
			CloseHandle(quitlisten);
		}
		idevices.clear();
	}
	catch (...)
	{
	}
	uninit_iTunesMobileDevice();
}

iDeviceClass* iDeviceClass::getDeviceInstanceByPtr(HANDLE device)
{
	iDeviceClass* ret = NULL;
	if (device==NULL)
	{
		device = iDeviceClass::curDevice.ptr_curdev;
	}
	ret = new iDeviceClass(device);
	return ret;
}

iDeviceClass* iDeviceClass::getDeviceInstanceByUdid(STRING udid)
{
	iDeviceClass* ret = NULL;
	if (idevices.find(udid)!=idevices.end())
	{
		ret = new iDeviceClass(idevices[udid]);
	}
	return ret;
}

std::vector<STRING> iDeviceClass::getUdids()
{
	std::vector<STRING> v;
	for (std::map<STRING, HANDLE>::iterator it = idevices.begin(); it != idevices.end(); ++it) {
		v.push_back(it->first);
	}
	return v;
}

STRING iDeviceClass::getDeviceUdid(HANDLE device)
{
	STRING s;
	//Program.logIt("getDeviceUdid ++", false);
	try
	{
		CFStringRef pp = g_iTunesMobileDevice.AMDeviceCopyDeviceIdentifier(device);
		if (pp!=NULL)
		{
			s = CUtilTools::ConvertData(pp);
		}
	}
	catch (...)
	{

	}
	//Program.logIt("getDeviceUdid --" + (string.IsNullOrEmpty(s)?"Can get uuid!!":s), false);
	return s;
}

#define	INTERVALTIME 200

BOOL waitDeivceTimeout(STRING udid, int nTimeout)
{
	int nStartTime = GetTickCount();
	int nEndTime = GetTickCount();
	BOOL bTimeout = TRUE;
	if (udid.empty()) return FALSE;
	do
	{
		if (g_args.deivces.check_device(udid))
		{
			if (iDeviceClass::curDevice.udid_current.empty())
			{
				iDeviceClass::addDevices(g_args.deivces.get_device(udid));
			}
			else
			{
				iDeviceClass::addDevices(g_args.deivces.get_device(udid));
				iDeviceClass::waitDevice(g_args.deivces.get_device(udid));
			}

			bTimeout = FALSE;
			break;
		}
		else
		{
			Sleep(INTERVALTIME);
			nEndTime = GetTickCount();
		}
	} while (nEndTime - nStartTime < nTimeout);

	return bTimeout;

}

DWORD WINAPI MyThreadProc(LPVOID pParam)
{
	int* pnTimeOut = (int*)pParam;

	try
	{
		if (WaitForSingleObject(iDeviceClass::quitlisten, *pnTimeOut) != WAIT_OBJECT_0) {
			CFRunLoopStop(iDeviceClass::currentLoopRef);
			iDeviceClass::currentLoopRef = NULL;
			*pnTimeOut = 1;
		}
	}
	catch (...)
	{

	}	

	return 0;   // thread completed successfully
}

bool  iDeviceClass::idDeviceReady(int nTimeOut)
{
	 //bool istimeout = waitDeivceTimeout(iDeviceClass::curDevice.udid_current, nTimeOut);
	 //return istimeout==false;
	currentLoopRef = CFRunLoopGetCurrent();
	CreateThread(NULL, 0, MyThreadProc, &nTimeOut, 0, NULL);
	CFRunLoopRun();
	if (nTimeOut == 1) return false;
	else return true;
	/*try
	{
		return WaitForSingleObject(quitlisten, nTimeOut) == WAIT_OBJECT_0;
	}
	catch (...)
	{
		
	}
	return false;*/
}

void iDeviceClass::waitDevice(HANDLE device)
{
	STRING udid = getDeviceUdid(device);
	if (!udid.empty() && udid.compare(curDevice.udid_current) == 0)
	{
		logIt("found iOS device by udid: %s\n", udid.c_str());
		curDevice.ptr_curdev = device;
		SetEvent(quitlisten);
		if (NULL != iDeviceClass::currentLoopRef) {
			CFRunLoopStop(iDeviceClass::currentLoopRef);
			iDeviceClass::currentLoopRef = NULL;
		}
	}
}

void iDeviceClass::addDevices(HANDLE device)
{
	STRING udid = getDeviceUdid(device);
	if (!udid.empty())
	{
		logIt("Add iOS device by udid: %s\n", udid.c_str());
		idevices[udid] = device;
	}
	else
		logIt("fail to get UDID from iOS devices.");
}


STRING iDeviceClass::getUdid()
{
	return getDeviceUdid(_device);
}
bool iDeviceClass::checkTrust()
{
	bool ret = false;
	int err;
	err = g_iTunesMobileDevice.AMDeviceConnect(_device);
	if (err == ERROR_SUCCESS)
	{
		CFStringRef s = g_iTunesMobileDevice.AMDeviceCopyValue(_device,NULL, CFSTR("ProductVersion"));
		
		CVersion v(CUtilTools::ConvertData(s));
		STRING ss = getUdid();

		CVersion vv("7.0.0.0");
		if (v >= vv)
		{
			if (g_iTunesMobileDevice.AMDGetPairingRecordDirectoryPath!=NULL)
			{
				CFStringRef sP = g_iTunesMobileDevice.AMDGetPairingRecordDirectoryPath();
				if (sP != NULL){
					STRING sPath = CUtilTools::ConvertData(sP);
					sPath.append("\\");
					sPath.append(curDevice.udid_current);
					sPath.append(".plist");

					if (PathFileExists(sPath.c_str()))
					{
						ret = true;
					}
				}
			}
		}

		g_iTunesMobileDevice.AMDeviceDisconnect(_device);
	}
	return ret;
}

int iDeviceClass::startSession()
{
	int err = g_iTunesMobileDevice.AMDeviceStartSession(_device);
	return err;
}
int iDeviceClass::connect()
{
	ENTER_FUNC_BEGIN;
	int ret = 0;
	int err;
	err = g_iTunesMobileDevice.AMDeviceConnect(_device);
	err = g_iTunesMobileDevice.AMDeviceIsPaired(_device);
	if (err == 1)
	{
		// paired on this pc
		err = g_iTunesMobileDevice.AMDeviceValidatePairing(_device);
		if (err != 0)
		{
			err = g_iTunesMobileDevice.AMDevicePair(_device);
		}
	}
	else
	{
		// not paired on this pc
		err = g_iTunesMobileDevice.AMDevicePair(_device);
	}

	if (err == 0)
	{
		// start lock down service
		err = g_iTunesMobileDevice.AMDeviceStartSession(_device);
		ret = err;
		// start AFC service
		// add by Richard
		if ((err = g_iTunesMobileDevice.AMDeviceStartService(_device, CFSTR("com.apple.afc2"), &hService)) == 0)
		{
			err = g_iTunesMobileDevice.AFCConnectionOpen(hService, 0, &hAFC);
			//ret = err;
			if (err == 0)
			{
				wasAFC2 = true;
				//ret = 0;
			}
		}
		else
		{
			if ((err = g_iTunesMobileDevice.AMDeviceStartService(_device, CFSTR("com.apple.afc"), &hService)) == 0)
			{
				err = g_iTunesMobileDevice.AFCConnectionOpen(hService, 0, &hAFC);
			}
		}
	}
	else
	{
		ret = err;
	}
	ENTER_FUNC_ENDRET(ret);
	return ret;
}
void iDeviceClass::disconnect()
{
	ENTER_FUNC_BEGIN;
	int err;
	err = g_iTunesMobileDevice.AMDeviceStopSession(_device);
	err = g_iTunesMobileDevice.AMDeviceDisconnect(_device);
}

void iDeviceClass::stopSession()
{
	int err;
	err = g_iTunesMobileDevice.AMDeviceStopSession(_device);
}

STRING iDeviceClass::copyValue(STRING key)
{
	STRING sret;
	CFStringRef sPtr =  g_iTunesMobileDevice.AMDeviceCopyValue(_device, NULL, __CFStringMakeConstantString(key.c_str()));
	if (sPtr!=NULL)
	{
		sret = CUtilTools::ConvertData(sPtr);
	}
	return sret;
}


STRING iDeviceClass::copyValue(STRING domain, STRING key)
{
	STRING sret;
	CFStringRef sPtr = g_iTunesMobileDevice.AMDeviceCopyValue(_device, __CFStringMakeConstantString(domain.c_str()), __CFStringMakeConstantString(key.c_str()));
	if (sPtr != NULL)
	{
		sret = CUtilTools::ConvertData(sPtr);
	}
	return sret;
}

CFTypeRef iDeviceClass::copyOnlyValue(STRING domain, STRING key)
{
	CFStringRef rfdomain = domain.empty() ? NULL : __CFStringMakeConstantString(domain.c_str());
	CFTypeRef sPtr = g_iTunesMobileDevice.AMDeviceCopyValue(_device, rfdomain, __CFStringMakeConstantString(key.c_str()));
	return sPtr;
}

int iDeviceClass::LookupApplications(CFDictionaryRef options, std::map<STRING, STRING> &AppBundles)
{
	CFDictionaryRef UntypedDict = NULL;
	int Result = g_iTunesMobileDevice.AMDeviceLookupApplications(_device, options, &UntypedDict);
	if (Result == 0 && UntypedDict != NULL)
	{
		CFDictionaryRef dict = UntypedDict;
		CFIndex n = CFDictionaryGetCount(dict);
		void** pKeys = (void**)malloc(n*sizeof(CFTypeRef));
		void** pValues = (void**)malloc(n*sizeof(CFTypeRef));
		if (pKeys && pValues)
		{
			CFDictionaryGetKeysAndValues(dict, (const void**)pKeys, (const void**)pValues);
			for (int i = 0; i < n; i++)
			{
				STRING sKey = CUtilTools::ConvertData((CFStringRef)pKeys[i]);
				if (CFDictionaryGetTypeID() == CFGetTypeID(pValues[i]))
				{
					CFStringRef spVal = ((CFDictionaryRef)pValues[i], CFSTR("Path"));
					if (CFStringGetTypeID()== CFGetTypeID(spVal))
					{
						STRING sV = CUtilTools::ConvertData(spVal);
						AppBundles[sKey] = sV;
					}
				}
				

			}
		}
	}

	return Result;
}


STRING iDeviceClass::getFMiPhoneInfo(BOOL bReadANum)
{
	ENTER_FUNC_BEGIN;
	STRING s = "N/A";
	try
	{
		CFTypeRef lvalue = g_iTunesMobileDevice.AMDeviceCopyValue(_device,NULL,CFSTR("ActivationInfo"));
		if (lvalue != NULL)
		{
			if (CFDictionaryGetTypeID() == CFGetTypeID(lvalue))
			{
				CFDictionaryRef dict = (CFDictionaryRef)lvalue;
				CFTypeRef ftype = CFDictionaryGetValue(dict, CFSTR("ActivationInfoXML"));
				if (ftype != NULL && CFDataGetTypeID() == CFGetTypeID(ftype))
				{
					CFPropertyListFormat fmt;
					CFErrorRef err;
					CFPropertyListRef plist = CFPropertyListCreateWithData(CFAllocatorGetDefault(), (CFDataRef)ftype, 0, &fmt, &err);
					if (plist)
					{
						CFStringRef sActivationState = (CFStringRef)CFDictionaryGetValue((CFDictionaryRef)plist, CFSTR("ActivationState"));
						STRING sAS = CUtilTools::ConvertData(sActivationState);
						CFBooleanRef bFMiPAccountExists = (CFBooleanRef)CFDictionaryGetValue((CFDictionaryRef)plist, CFSTR("FMiPAccountExists"));
						if (bFMiPAccountExists==NULL)
						{
							s = "false";
						}
						else{
							if (sAS.compare("Unactivated") == 0 && !CFBooleanGetValue(bFMiPAccountExists))
							{
								s = "N/A";
							}
							else
							{
								s = CFBooleanGetValue(bFMiPAccountExists) ? "true" : "false";
							}
						}
						
						CFStringRef sRegulatoryModelNumber = (CFStringRef)CFDictionaryGetValue((CFDictionaryRef)plist, CFSTR("RegulatoryModelNumber"));
						printf("RegulatoryModelNumber=%s\n", CUtilTools::ConvertData(sRegulatoryModelNumber).c_str());
					}
				}
			}
		}
		else if (bReadANum)
		{
			logIt("ActivationInfo Lost, MUST USE ACTIVE.");
			int ret = 87;
			CFTypeRef plCASI = CreateActivationSessionInfo(&ret);
			if (ret == ERROR_SUCCESS)
			{
				StringBuilder^ sbRet = gcnew StringBuilder();
				StringBuilder^ sbSessionInfo = gcnew StringBuilder();
				CFDataRef pdata = CFPropertyListCreateXMLData(NULL, plCASI);
				String^ aplist = gcnew String((char *)CFDataGetBytePtr(pdata), 0, CFDataGetLength(pdata));
				sbSessionInfo->Append(aplist);
				ret = iactive::do_session_app("https://albert.apple.com/deviceservices/drmHandshake", sbSessionInfo, sbRet, false);
				if (0 == ret && sbRet->Length > 0)
				{

					sbRet->Insert(0, "<?xml version=\"1.0\" encoding=\"UTF - 8\"?>\r\n<!DOCTYPE plist PUBLIC \"-//Apple//DTD PLIST 1.0//EN\" \"http://www.apple.com/DTDs/PropertyList-1.0.dtd\">\r\n");
					//if ((ret = dc->startSession()) == 0)
					{

						const char* plistSessionData =
							(const char*)(Marshal::StringToHGlobalAnsi(sbRet->ToString()).ToPointer());
						CFDataRef buffer_as_data = CFDataCreate(CFAllocatorGetDefault(), (UInt8*)plistSessionData, strlen(plistSessionData));
						if (buffer_as_data == NULL)
						{
							logIt("failed to allocate CreateActivationSessionInfo return\n");
						}


						CFStringRef error = NULL;
						CFTypeRef plCAI = CreateActivationInfo(buffer_as_data, &ret);
						if (ret==ERROR_SUCCESS)
						{
							if (CFDictionaryGetTypeID() == CFGetTypeID(plCAI))
							{
								CFDictionaryRef dict = (CFDictionaryRef)plCAI;
								CFTypeRef ftype = CFDictionaryGetValue(dict, CFSTR("ActivationInfoXML"));
								if (ftype != NULL && CFDataGetTypeID() == CFGetTypeID(ftype))
								{
									CFShow(ftype);
									CFPropertyListFormat fmt;
									CFErrorRef err;
									CFPropertyListRef plist = CFPropertyListCreateWithData(CFAllocatorGetDefault(), (CFDataRef)ftype, 0, &fmt, &err);
									if (plist)
									{
										CFShow(plist);
										
										CFDictionaryRef pActivationRequestInfo = (CFDictionaryRef)CFDictionaryGetValue((CFDictionaryRef)plist, CFSTR("ActivationRequestInfo"));
										if (pActivationRequestInfo != NULL)
										{
											CFStringRef sActivationState = (CFStringRef)CFDictionaryGetValue((CFDictionaryRef)pActivationRequestInfo, CFSTR("ActivationState"));
											STRING sAS = CUtilTools::ConvertData(sActivationState);
											CFBooleanRef bFMiPAccountExists = (CFBooleanRef)CFDictionaryGetValue((CFDictionaryRef)pActivationRequestInfo, CFSTR("FMiPAccountExists"));
											if (bFMiPAccountExists == NULL)
											{
												s = "false";
											}
											else{
												if (sAS.compare("Unactivated") == 0 && !CFBooleanGetValue(bFMiPAccountExists))
												{
													s = "N/A";
												}
												else
												{
													s = CFBooleanGetValue(bFMiPAccountExists) ? "true" : "false";
												}
											}
										}
										CFDictionaryRef pDeviceinfo = (CFDictionaryRef)CFDictionaryGetValue((CFDictionaryRef)plist, CFSTR("DeviceInfo"));
										if (pDeviceinfo != NULL)
										{
											CFStringRef sRegulatoryModelNumber = (CFStringRef)CFDictionaryGetValue((CFDictionaryRef)pDeviceinfo, CFSTR("RegulatoryModelNumber"));
											printf("RegulatoryModelNumber=%s\n", CUtilTools::ConvertData(sRegulatoryModelNumber).c_str());
										}
									}
								}
							}
						}

					}
				}
			}

		}
	}
	catch (...)
	{
	}
	ENTER_FUNC_END;
	return s;
}


void iDeviceClass::AddInfoToList(std::vector<STRING> &ll, STRING sDomain, STRING sKey)
{
	try
	{
		STRING v = copyValue(sDomain, sKey);
		Sleep(10);
		ll.push_back(sKey+"="+CUtilTools::trim(v));
	}
	catch (...)
	{

	}

}

const char hexmap[] = { '0', '1', '2', '3', '4', '5', '6', '7',
						   '8', '9', 'a', 'b', 'c', 'd', 'e', 'f' };

std::string hexStr(unsigned char *data, int len)
{
	std::string s(len * 2, ' ');
	for (int i = 0; i < len; ++i) {
		s[2 * i] = hexmap[(data[i] & 0xF0) >> 4];
		s[2 * i + 1] = hexmap[data[i] & 0x0F];
	}
	return s;
}


void iDeviceClass::AddESIMInfo(std::vector<STRING> &ll)
{
	try
	{
		CFDictionaryRef sPtr = (CFDictionaryRef)g_iTunesMobileDevice.AMDeviceCopyValue(_device, NULL, CFSTR("FirmwarePreflightInfo"));
		if (sPtr != NULL && CFDictionaryGetTypeID() == CFGetTypeID(sPtr)) {
			CFIndex n = CFDictionaryGetCount(sPtr);
			void** pKeys = (void**)malloc(n * sizeof(CFTypeRef));
			void** pValues = (void**)malloc(n * sizeof(CFTypeRef));
			if (pKeys && pValues)
			{
				CFDictionaryGetKeysAndValues(sPtr, (const void**)pKeys, (const void**)pValues);
				for (int i = 0; i < n; i++)
				{
					STRING sKey = CUtilTools::ConvertData((CFStringRef)pKeys[i]);
					if (CFNumberGetTypeID() == CFGetTypeID(pValues[i])) {
						STRING v = CUtilTools::ConvertData((CFNumberRef)pValues[i]);
						ll.push_back(sKey + "=" + CUtilTools::trim(v));
					}
					else if (CFDataGetTypeID() == CFGetTypeID(pValues[i])) {
						CFIndex dataLen = CFDataGetLength((CFDataRef)pValues[i]);
						const byte* pdata = CFDataGetBytePtr((CFDataRef)pValues[i]);
						ll.push_back(sKey + "=" + CUtilTools::trim(hexStr((unsigned char *)pdata, dataLen)));
					}
					else {
						logIt(_T("not supported ESIM data key: %s"), sKey.c_str());
					}
				}
			}
		}
	}
	catch (...)
	{

	}

}


static void Print_CFArray(STRING sKey, CFArrayRef val, void *context)
{
	CFIndex nCount = CFArrayGetCount((CFArrayRef)val);
	STRING sValuearray;
	for (CFIndex i = 0; i < nCount; i++)
	{
		CFTypeRef value = CFArrayGetValueAtIndex((CFArrayRef)val, i);
		if (value != NULL&&CFDictionaryGetTypeID() == CFGetTypeID(value))
		{
			CFDictionaryApplyFunction((CFDictionaryRef)value, Print_dictionary_applier, context);
		}
		else if (value != NULL&& CFArrayGetTypeID() == CFGetTypeID(value))
		{
			Print_CFArray(sKey, (CFArrayRef)value, context);
		}
		else if (value != NULL&& CFDataGetTypeID() == CFGetTypeID(value))
		{
			//do nothing
		}
		else{
			if (sValuearray.size() == 0)
			{
				sValuearray.append(sKey);
				sValuearray.append("=");
			}
			sValuearray.append(CUtilTools::ConvertData(value));
			sValuearray.append(";");
		}
	}
	if (sValuearray.size()>0)
	{
		std::vector<STRING> *ret = (std::vector<STRING> *)context;
		ret->push_back(sValuearray);
	}
}

static void Print_dictionary_applier(const void *key, const void *value, void *context)
{
	if (key == NULL) return;
	std::vector<STRING> *ret = (std::vector<STRING> *)context;
	STRING sKey = CUtilTools::ConvertData((CFTypeRef)key);
	if (value != NULL&&CFDictionaryGetTypeID() == CFGetTypeID(value))
	{
		CFDictionaryApplyFunction((CFDictionaryRef)value, Print_dictionary_applier, context);
	}
	else if (value != NULL&& CFArrayGetTypeID() == CFGetTypeID(value))
	{
		Print_CFArray(sKey, (CFArrayRef)value, context);
	}
	else if (value != NULL&& CFDataGetTypeID() == CFGetTypeID(value))
	{
		//do nothing
	}
	else{
		STRING v = CUtilTools::ConvertData((CFTypeRef)value);
		if (sKey.compare("TimeIntervalSince1970") == 0 && !v.empty())
		{
			double timestamp = std::stod(CUtilTools::trim(v));
			time_t epoch = timestamp;
			ret->push_back(STRING("TimeInterval=") + asctime(gmtime(&epoch)));
		}
		sKey.append("=");
		sKey.append(v);
		ret->push_back(sKey);


	}
}

void iDeviceClass::GetInfoWithoutTrust()
{
	STRING s;
	CFTypeRef sPtr = g_iTunesMobileDevice.AMDeviceCopyDeviceIdentifier(_device);
	if (sPtr != NULL)
	{
		s = "UDID=" + CUtilTools::ConvertData(sPtr);
		_tprintf("%s\n", s.c_str()); 
		fflush(stdout);
	}
	
	s = copyValue("ProductVersion");
	_tprintf("ProductVersion=%s\n", s.c_str());
	fflush(stdout);
	Sleep(10);
	
	s = copyValue("HardwareModel");
	_tprintf("HardwareModel=%s\n", s.c_str());
	fflush(stdout);
	Sleep(10);
	s = copyValue("UniqueChipID");
	_tprintf("UniqueChipID=%s\n", s.c_str());
	fflush(stdout);
	Sleep(10);
	s = copyValue("DeviceName");
	_tprintf("DeviceName=%s\n", s.c_str());
	fflush(stdout);
	Sleep(10);
	s = copyValue("DeviceColor");
	_tprintf("DeviceColor=%s\n", s.c_str());
	fflush(stdout);
	Sleep(10);
	s = copyValue("ProductType");
	_tprintf("ProductType=%s\n", s.c_str());
	fflush(stdout);
	Sleep(10);	
}


std::vector<STRING> iDeviceClass::getDeviceANumber()
{
	ENTER_FUNC_BEGIN;
	std::vector<STRING> ret;
	STRING sFmi = getFMiPhoneInfo(true);
	ret.push_back("FMiPAccountExists=" + sFmi);

	return ret;
}

std::vector<STRING> iDeviceClass::getDeviceInfoDetect()
{
	ENTER_FUNC_BEGIN;
	std::vector<STRING> ret;
	CFTypeRef sPtr = g_iTunesMobileDevice.AMDeviceCopyValue(_device, NULL, NULL);
	if (sPtr != NULL&&CFDictionaryGetTypeID() == CFGetTypeID(sPtr))
	{
		CFShow(sPtr);
		CFDictionaryApplyFunction((CFDictionaryRef)sPtr, Print_dictionary_applier, &ret/*NULL*/);
	}
	return ret;
}

std::vector<STRING> iDeviceClass::getDeviceInfo(BOOL bReadANum, BOOL bSingle)
{
	ENTER_FUNC_BEGIN;
	std::vector<STRING> ret;
	BOOL bFindProblem = false;
	STRING sOldData = "";
	//if (bSingle)
	for(int i = 0; i < 5; i++)
	{
		CFTypeRef sPtr = g_iTunesMobileDevice.AMDeviceCopyValue(_device, NULL, NULL);
		if (sPtr != NULL&&CFDictionaryGetTypeID() == CFGetTypeID(sPtr))
		{
			CFShow(sPtr);
			CFDictionaryApplyFunction((CFDictionaryRef)sPtr, Print_dictionary_applier, &ret/*NULL*/);
			break;
		}
		else {
			Sleep(5000);
		}
	}
	//if (ret.size()==0)
	{
		for each(STRING k in _device_info_keys)
		{
			try
			{
				STRING v = copyValue(k);
				Sleep(10);
				if (bFindProblem)
				{
					if (v.compare(sOldData) == 0)
					{
						ret.clear();
						break;
					}
					bFindProblem = false;
				}
				if (v.empty())
				{
					bFindProblem = true;
					v = "";
					v = copyValue(k);
					if (v.empty())
					{
						bFindProblem = false;
						v = "";
					}
					sOldData = v;
				}

				ret.push_back(k + "=" + CUtilTools::trim(v));
				if (k.compare("TimeIntervalSince1970") == 0 && !v.empty())
				{
					double timestamp = std::stod(CUtilTools::trim(v));
					time_t epoch = timestamp;
					ret.push_back(STRING("TimeInterval=") + asctime(gmtime(&epoch)));
				}
			}
			catch (...)
			{
			}
		}
	}

	AddInfoToList(ret, "com.apple.disk_usage", "CalculateDiskUsage");
	AddInfoToList(ret, "com.apple.disk_usage", "TotalDataCapacity");
	AddInfoToList(ret, "com.apple.disk_usage", "TotalSystemCapacity");
	AddInfoToList(ret, "com.apple.disk_usage", "TotalDiskCapacity");
	AddInfoToList(ret, "com.apple.mobile.battery", "BatteryCurrentCapacity");
	AddInfoToList(ret, "com.apple.mobile.iTunes", "iTunesSetupComplete");
	AddInfoToList(ret, "com.apple.mobile.iTunes", "SupportsAirTraffic");
	AddInfoToList(ret, "com.apple.itunesstored", "AppleID");
	AddInfoToList(ret, "com.apple.fmip", "IsAssociated");
	AddInfoToList(ret, "com.apple.mobile.iTunes", "FairPlayDeviceType");
	AddInfoToList(ret, "com.apple.mobile.iTunes", "FairPlayGUID");
	//AddInfoToList(ret, "com.apple.mobile.iTunes", "FairPlayCertificate");
	AddInfoToList(ret, "com.apple.mobile.iTunes", "KeyTypeSupportVersion");

	AddESIMInfo(ret);
	STRING sFmi = getFMiPhoneInfo(false);
	ret.push_back("FMiPAccountExists="+ sFmi);

	return ret;
}

std::vector<STRING> iDeviceClass::getDeviceInfoBundle()
{
	std::vector<STRING> ret;
	STRING sVBundle = "";
	try
	{
		CFTypeRef lvalue = g_iTunesMobileDevice.AMDeviceCopyValue(_device, NULL, CFSTR("CarrierBundleInfo"));
		if (lvalue ==NULL)
		{
			lvalue = g_iTunesMobileDevice.AMDeviceCopyValue(_device, NULL, CFSTR("CarrierBundleInfoArray")); 
			if (lvalue == NULL)
			{
				ret.push_back("CarrierName=");
				return ret;
			}
		}

		CFShow(lvalue);
		if (CFGetTypeID(lvalue) == CFArrayGetTypeID() && CFArrayGetCount((CFArrayRef)(lvalue))>0)
		{
			lvalue = CFArrayGetValueAtIndex((CFArrayRef)(lvalue), 0);
		}
		else if (CFGetTypeID(lvalue) == CFArrayGetTypeID() && CFArrayGetCount((CFArrayRef)(lvalue))==0)
		{
			ret.push_back("CarrierName=");
			return ret;
		}

		CFErrorRef myError;
		CFDataRef xmlData = CFPropertyListCreateData(
			CFAllocatorGetDefault(), lvalue, kCFPropertyListXMLFormat_v1_0, 0, &myError);

		STRING xml((char *)CFDataGetBytePtr(xmlData), CFDataGetLength(xmlData));
		string::size_type pos = 0; // Must initialize
		while ((pos = xml.find("\n", pos)) != string::npos)
		{
			xml.erase(pos, 1);
		}
		pos = 0; // Must initialize
		while ((pos = xml.find("\r", pos)) != string::npos)
		{
			xml.erase(pos, 1);
		}
		ret.push_back("CarrierBundleInfo="+xml);

		CFPropertyListFormat fmt;
		CFPropertyListRef plist = CFPropertyListCreateWithData(CFAllocatorGetDefault(), xmlData, 0, &fmt, &myError);
		if (plist)
		{
			CFDictionaryRef dict = (CFDictionaryRef)plist;
			CFTypeRef ftype = CFDictionaryGetValue(dict, CFSTR("CFBundleIdentifier"));
			STRING sId;
			if (ftype!=NULL && CFStringGetTypeID() == CFGetTypeID(ftype))
			{
				sId = CUtilTools::ConvertData((CFStringRef)ftype);
			}
			else{
				ftype = CFDictionaryGetValue(dict, CFSTR("CFBundleExecutable"));
				if (ftype != NULL && CFStringGetTypeID() == CFGetTypeID(ftype)){
					sId = "com.apple." + CUtilTools::ConvertData((CFStringRef)ftype);
				}
			}
			if (!sId.empty())
			{
				TCHAR Curpath[MAX_PATH+1] = {0};
				STRING sPath = GetThisPath(Curpath, MAX_PATH);
				if (!sPath.empty())
				{
					sPath = sPath + "\\CarrierBundle.ini";
					TCHAR vvv[MAX_PATH] = { 0 };
					GetPrivateProfileString("carrierName", sId.c_str(), "", vvv, MAX_PATH, sPath.c_str());
					if (_tcslen(vvv)>0)
					{
						ret.push_back("CarrierName=" + CUtilTools::trim(STRING(vvv)));
					}
					else
					{
						ret.push_back("CarrierName=" + sId.substr(10));//remove com.apple
					}
				}
			}
		}
	}
	catch (...)
	{
	}

	return ret;
}

void dump_installed_app(const void *key, const void *value, void *context)
{
	ENTER_FUNC_BEGIN;
	if ((key == NULL) || (value == NULL))
	{
		return;
	}
	map<STRING, STRING> *apps = NULL;
	if (context != NULL)
	{
		apps = (map<STRING, STRING> *)context;
	}
	STRING bundle_id = CUtilTools::ConvertData((CFStringRef)key);

	if (bundle_id.size() == 0)
	{
		return;
	}
	STRING path;
	CFStringRef path_value;
	path_value = (CFStringRef)CFDictionaryGetValue((CFDictionaryRef)value, CFSTR("Path"));

	if (path_value != NULL)
	{
		path = CUtilTools::ConvertData(path_value);
		apps->insert(pair<STRING, STRING>(path, bundle_id));
	}
}

//add by jeffery
 BOOL iDeviceClass::IsJailbreak()
{
	ENTER_FUNC_BEGIN;
	BOOL bRet = FALSE;
	STRING jailbreak_apps[] =
	{
		//_T("/Applications"),
		_T("/Applications/Cydia.app"),
		_T("/Applications/blackra1n.app"),
		_T("/Applications/blacksn0w.app"),
		_T("/Applications/greenpois0n.app"),
		_T("/Applications/limera1n.app"),
		_T("/Applications/redsn0w.app"),
		_T("/Applications/FakeCarrier.app"),
		_T("/Applications/Icy.app"),
		_T("/Applications/IntelliScreen.app"),
		_T("/Applications/MxTube.app"),
		_T("/Applications/RockApp.app"),
		_T("/Applications/SBSetttings.app"),
		_T("/Applications/WinterBoard.app"),
		//_T("/var/mobile/Library/AddressBook/AddressBook.sqlitedb"),
	};
	if (wasAFC2) return true;

	CFDictionaryRef apps=NULL;
#define DefaultApplicationLookupDictionaryCount 6
	const void* values[DefaultApplicationLookupDictionaryCount] = { CFSTR(kAppLookupKeyCFBundleIdentifier),
		CFSTR(kAppLookupKeyApplicationType), CFSTR(kAppLookupKeyCFBundleDisplayName),
		CFSTR(kAppLookupKeyCFBundleName), CFSTR(kAppLookupKeyContainer), CFSTR(kAppLookupKeyPath) };
	CFArrayRef lookupValues = CFArrayCreate(CFAllocatorGetDefault(), values, DefaultApplicationLookupDictionaryCount,
		&g_iTunesMobileDevice.my_kCFTypeArrayCallBacks);

	CFMutableDictionaryRef optionsDict = CFDictionaryCreateMutable(NULL, 0, &g_iTunesMobileDevice.my_kCFTypeDictionaryKeyCallBacks, &g_iTunesMobileDevice.my_kCFTypeDictionaryValueCallBacks);
	CFDictionarySetValue(optionsDict, CFSTR("ReturnAttributes"), lookupValues);

	if (g_iTunesMobileDevice.AMDeviceLookupApplications(_device, optionsDict, &apps) == ERROR_SUCCESS)
	{
		map<STRING, STRING> lapps;
		CFDictionaryApplyFunction(apps, dump_installed_app, &lapps);
		if (apps != NULL)
			CF_RELEASE_CLEAR(apps);

		for (int i = 0; i < (sizeof(jailbreak_apps) / sizeof(jailbreak_apps[0])); i++)
		{
			if (lapps.find(jailbreak_apps[i]) != lapps.end())
			{
				bRet = TRUE;
				break;
			}
		}	
	}

	
	ENTER_FUNC_ENDRET(bRet);
	return bRet;
}
// add by Richard
long iDeviceClass::GetFileSize(STRING path)
{
	HANDLE info = INVALID_HANDLE_VALUE;
	std::map<STRING, STRING> items;
	int ret = g_iTunesMobileDevice.AFCDeviceInfoOpen(hAFC, &info);
	if (ret == 0 && info != INVALID_HANDLE_VALUE)
	{
		char* pname = NULL, *pvalue = NULL;

		while (g_iTunesMobileDevice.AFCKeyValueRead(info, &pname, &pvalue) == ERROR_SUCCESS && pname != NULL && pvalue != NULL)
		{
			STRING name = pname;
			STRING value = pvalue;
			items[name] = value;
		}

		g_iTunesMobileDevice.AFCKeyValueClose(info);
	}

	long retsize = 0;
	if (items.find("st_size")!=items.end())
	{
		retsize = std::stol(items["st_size"]);
	}


	return retsize;
}
/*
* FSFreeBytes - free bytes on system device for afc2, user device for afc
* FSBlockSize - file system block size
* FSTotalBytes - size of device
* Model - iPhone1,1 etc.
*/
std::vector<STRING> iDeviceClass::GetSysInfo()
{
	std::vector<STRING> retVal;
	if (hAFC != INVALID_HANDLE_VALUE)
	{
		HANDLE info = INVALID_HANDLE_VALUE;

		int ret = g_iTunesMobileDevice.AFCDeviceInfoOpen(hAFC, &info);
		if (ret == 0 && info != INVALID_HANDLE_VALUE)
		{
			char* pname = NULL, *pvalue = NULL;

			while (g_iTunesMobileDevice.AFCKeyValueRead(info, &pname, &pvalue) == ERROR_SUCCESS && pname != NULL && pvalue != NULL)
			{
				STRING name = pname;
				STRING value = pvalue;
				if (name.size() > 0)
				{
					name.append("=");
					name.append(value);
					retVal.push_back(name);
					logIt(_T("%s"), name.c_str());
				}
			}

			g_iTunesMobileDevice.AFCKeyValueClose(info);
		}
		else
			logIt(_T("AFCDeviceInfoOpen fail, ret: %d\n"), ret);
	}
	return retVal;
}
// end
int iDeviceClass::deactive()
{
	return g_iTunesMobileDevice.AMDeviceDeactivate(_device);
}

CFTypeRef iDeviceClass::CreateActivationSessionInfo(int *errcode)
{
	return g_iTunesMobileDevice.AMDeviceCreateActivationSessionInfo(_device, errcode);
}

int iDeviceClass::ActivateWithOptions(CFTypeRef pl_p, CFTypeRef pl_header)
{
	return g_iTunesMobileDevice.AMDeviceActivateWithOptions(_device, pl_p, pl_header);
}

CFTypeRef iDeviceClass::CreateActivationInfoWithOptions(CFTypeRef pdata, int zero, int *errcode)
{
	return g_iTunesMobileDevice.AMDeviceCreateActivationInfoWithOptions(_device, pdata, zero, errcode);
}

CFTypeRef iDeviceClass::CreateActivationInfo(CFTypeRef pdata, int *errcode)
{
	return g_iTunesMobileDevice.AMDeviceCreateActivationInfo(_device, pdata, errcode);
}


int iDeviceClass::active(std::list<CFStringRef> keys, std::list<CFTypeRef> values)
{
	if (keys.size() != values.size()) return ERROR_INVALID_PARAMETER;
	CFMutableDictionaryRef actRecord = CFDictionaryCreateMutable(NULL, 0, &g_iTunesMobileDevice.my_kCFTypeDictionaryKeyCallBacks, &g_iTunesMobileDevice.my_kCFTypeDictionaryValueCallBacks);
	while (!keys.empty())
	{
		auto k = keys.front();
		keys.pop_front();
		auto v = values.front();
		values.pop_front();

		CFDictionaryAddValue(actRecord, k, v);
		//CFDictionaryAddValue(actRecord, CFSTR("AccountTokenCertificate"), CFDictionaryGetValue((CFDictionaryRef)record, CFSTR("AccountTokenCertificate")));
		//CFDictionaryAddValue(actRecord, CFSTR("AccountToken"), CFDictionaryGetValue((CFDictionaryRef)record, CFSTR("AccountToken")));
		//CFDictionaryAddValue(actRecord, CFSTR("FairPlayKeyData"), CFDictionaryGetValue((CFDictionaryRef)record, CFSTR("FairPlayKeyData")));
		//CFDictionaryAddValue(actRecord, CFSTR("DeviceCertificate"), CFDictionaryGetValue((CFDictionaryRef)record, CFSTR("DeviceCertificate")));
		//CFDictionaryAddValue(actRecord, CFSTR("AccountTokenSignature"), CFDictionaryGetValue((CFDictionaryRef)record, CFSTR("AccountTokenSignature")));
	}
	return g_iTunesMobileDevice.AMDeviceActivate(_device, (CFMutableDictionaryRef)actRecord);
}
