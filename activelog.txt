===>HTML chunked
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html lang="en" xmlns="http://www.apple.com/itms/">
<head>
	<meta http-equiv="&quote;Content-Type&quote; content=&quote;text/html; charset=utf-8&quote;"></meta>
	<meta name="keywords" content="iTunes Store"></meta>
	<meta name="description" content="iTunes Store"></meta>
	<title>iPhone Activation</title>
	<link rel="stylesheet" charset="utf-8" href="https://static.ips.apple.com/CSCustomer/css/shared/common-min.css"></link>
	<link rel="stylesheet" charset="utf-8" href="https://static.ips.apple.com/CSCustomer/css/pages/WaitPage-min.css"></link>
</head>
<body>
	<div id="jingle-page-wrapper">
		<div id="jingle-page-wrapper-header">
			<div id="secure">
				<img src="https://static.ips.apple.com/CSCustomer/img/lock.png"></img>
			</div>
			<div id="banner">
				<div id="apple-logo">
					<img src="https://static.ips.apple.com/CSCustomer/img/apple_chrome.png"></img>
				</div>
			</div>
		</div>
		<div id="jingle-page-wrapper-content">
			<img src="https://static.ips.apple.com/CSCustomer/img/iphonereg/default/iphonereg_title_processing.gif"></img>
			<form id="jingle-page-form" action="https://activateiphone8.apple.com/CSCustomer/direct-fulfill/refreshAction">
				<div id="jingle-page-content">
					<script>
					window.controllerConfig = {
						"data": {"data-url":{"url":"https://activateiphone8.apple.com/CSCustomer/direct-fulfill/poll","delay":1000},"wait-page-processing-text":{"value":"We are processing your request. The page you have requested will load automatically."}},
						"ajaxTimeoutErrorPage": "/CSCustomer/direct-fulfill/error",
						"ajaxTimeoutDelay": 30000
					};
					</script>
					<script src="https://static.ips.apple.com/CSCustomer/js/Utils-min.js"></script>
					<script src="https://static.ips.apple.com/CSCustomer/js/WaitPage-min.js"></script>
					<div id="WaitPage">
						<p class="wait" id="header">Please wait while we process your request.</p>
						<p class="eta" id="eta">This may take up to 3 minutes.</p>
						<p class="spinner"></p>
					</div>
				</div>
			<div id="jingle-page-buttons"></div>
			</form>
		</div>
	</div>
	<div id="jingle-page-wrapper-footer">
		<div id="footer">
			<div id="legal">Copyright &copy; 2018 Apple Inc. All rights reserved. | <a href="http://www.apple.com/legal/iphone/us/privacy/" target="_blank">Privacy Policies</a> | <a href="http://www.apple.com/legal/iphone/us/terms/" target="_blank">Terms &amp; Conditions</a>
		</div>
	</div>
</div>
</body>
</html>


//next
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html lang="en" xmlns="http://www.apple.com/itms/">
<head>
	<meta http-equiv="&quote;Content-Type&quote; content=&quote;text/html; charset=utf-8&quote;"></meta>
	<meta name="keywords" content="iTunes Store"></meta><meta name="description" content="iTunes Store"></meta>
	<title>iPhone Activation</title>
	<link rel="stylesheet" charset="utf-8" href="https://static.ips.apple.com/CSCustomer/css/shared/common-min.css"></link>
	<link rel="stylesheet" charset="utf-8" href="https://static.ips.apple.com/CSCustomer/css/pages/SetupComplete-min.css"></link>
</head>
<body>
	<div id="jingle-page-wrapper">
		<div id="jingle-page-wrapper-header">
			<div id="secure">
				<img src="https://static.ips.apple.com/CSCustomer/img/lock.png"></img>
			</div>
			<div id="banner">
				<div id="apple-logo">
					<img src="https://static.ips.apple.com/CSCustomer/img/apple_chrome.png"></img>
				</div>
			</div>
		</div>
		<div id="jingle-page-wrapper-content">
			<img src="https://static.ips.apple.com/CSCustomer/img/iphonereg/verizon_US/iphonereg_title_activation_complete.gif"></img>
			<form id="jingle-page-form" action="https://activateiphone8.apple.com/CSCustomer/direct-fulfill/next">
			<div id="jingle-page-content">
				<div id="SetupComplete">
					<div class="congrats">
						<p>Your activation requires additional time to complete.</p>
					</div>
					<div class="instruction">
						<p>Please contact Verizon at 877-807-4646 to get the iPhone added to your account. Click Continue to be able to explore its features and the App Store.</p>
					</div>
				</div>
			</div>
			<div id="jingle-page-buttons"><div id="jingle-page-buttons-left"></div>
			<div id="jingle-page-buttons-right"><input class="IPAJingleButton right" type="submit" name="submit" value="Continue" required/></div>
		</div>
		</form>
	</div>
</div>
<div id="jingle-page-wrapper-footer"><div id="footer"><div id="legal">Copyright &copy; 2018 Apple Inc. All rights reserved. | <a href="http://www.apple.com/legal/iphone/us/privacy/" target="_blank">Privacy Policies</a> | <a href="http://www.apple.com/legal/iphone/us/terms/" target="_blank">Terms &amp; Conditions</a></div></div></div></body></html> 


<?xml version="1.0"?>
<plist version="1.0">
<dict>
	<key>ActivationRecord</key>
	<dict>
		<key>unbrick</key>
		<true/>
		<key>AccountTokenCertificate</key>
		<data>LS0tLS1CRUdJTiBDRVJUSUZJQ0FURS0tLS0tCk1JSURaekNDQWsrZ0F3SUJBZ0lCQWpBTkJna3Foa2lHOXcwQkFRVUZBREI1TVFzd0NRWURWUVFHRXdKVlV6RVQKTUJFR0ExVUVDaE1LUVhCd2JHVWdTVzVqTGpFbU1DUUdBMVVFQ3hNZFFYQndiR1VnUTJWeWRHbG1hV05oZEdsdgpiaUJCZFhSb2IzSnBkSGt4TFRBckJnTlZCQU1USkVGd2NHeGxJR2xRYUc5dVpTQkRaWEowYVdacFkyRjBhVzl1CklFRjFkR2h2Y21sMGVUQWVGdzB3TnpBME1UWXlNalUxTURKYUZ3MHhOREEwTVRZeU1qVTFNREphTUZzeEN6QUoKQmdOVkJBWVRBbFZUTVJNd0VRWURWUVFLRXdwQmNIQnNaU0JKYm1NdU1SVXdFd1lEVlFRTEV3eEJjSEJzWlNCcApVR2h2Ym1VeElEQWVCZ05WQkFNVEYwRndjR3hsSUdsUWFHOXVaU0JCWTNScGRtRjBhVzl1TUlHZk1BMEdDU3FHClNJYjNEUUVCQVFVQUE0R05BRENCaVFLQmdRREZBWHpSSW1Bcm1vaUhmYlMyb1BjcUFmYkV2MGQxams3R2JuWDcKKzRZVWx5SWZwcnpCVmRsbXoySkhZdjErMDRJekp0TDdjTDk3VUk3ZmswaTBPTVkwYWw4YStKUFFhNFVnNjExVApicUV0K25qQW1Ba2dlM0hYV0RCZEFYRDlNaGtDN1QvOW83N3pPUTFvbGk0Y1VkemxuWVdmem1XMFBkdU94dXZlCkFlWVk0d0lEQVFBQm80R2JNSUdZTUE0R0ExVWREd0VCL3dRRUF3SUhnREFNQmdOVkhSTUJBZjhFQWpBQU1CMEcKQTFVZERnUVdCQlNob05MK3Q3UnovcHNVYXEvTlBYTlBIKy9XbERBZkJnTlZIU01FR0RBV2dCVG5OQ291SXQ0NQpZR3UwbE01M2cyRXZNYUI4TlRBNEJnTlZIUjhFTVRBdk1DMmdLNkFwaGlkb2RIUndPaTh2ZDNkM0xtRndjR3hsCkxtTnZiUzloY0hCc1pXTmhMMmx3YUc5dVpTNWpjbXd3RFFZSktvWklodmNOQVFFRkJRQURnZ0VCQUY5cW1yVU4KZEErRlJPWUdQN3BXY1lUQUsrcEx5T2Y5ek9hRTdhZVZJODg1VjhZL0JLSGhsd0FvK3pFa2lPVTNGYkVQQ1M5Vgp0UzE4WkJjd0QvK2Q1WlFUTUZrbmhjVUp3ZFBxcWpubTlMcVRmSC94NHB3OE9OSFJEenhIZHA5NmdPVjNBNCs4CmFia29BU2ZjWXF2SVJ5cFhuYnVyM2JSUmhUekFzNFZJTFM2alR5Rll5bVplU2V3dEJ1Ym1taWdvMWtDUWlaR2MKNzZjNWZlREF5SGIyYnpFcXR2eDNXcHJsanRTNDZRVDVDUjZZZWxpblpuaW8zMmpBelJZVHh0UzZyM0pzdlpEaQpKMDcrRUhjbWZHZHB4d2dPKzdidFcxcEZhcjBaakY5L2pZS0tuT1lOeXZDcndzemhhZmJTWXd6QUc1RUpvWEZCCjRkK3BpV0hVRGNQeHRjYz0KLS0tLS1FTkQgQ0VSVElGSUNBVEUtLS0tLQo=</data>
		<key>DeviceCertificate</key>
		<data>LS0tLS1CRUdJTiBDRVJUSUZJQ0FURS0tLS0tCk1JSUM4akNDQWx1Z0F3SUJBZ0lKZWdjYjI3RjlxUVFXTUEwR0NTcUdTSWIzRFFFQkJRVUFNRm94Q3pBSkJnTlYKQkFZVEFsVlRNUk13RVFZRFZRUUtFd3BCY0hCc1pTQkpibU11TVJVd0V3WURWUVFMRXd4QmNIQnNaU0JwVUdodgpibVV4SHpBZEJnTlZCQU1URmtGd2NHeGxJR2xRYUc5dVpTQkVaWFpwWTJVZ1EwRXdIaGNOTVRnd056QXlNakV4Ck1UUXpXaGNOTWpFd056QXlNakV4TVRReldqQ0JnekV0TUNzR0ExVUVBeFlrT1VWRFJqSTJPRU10TVRWQ09DMDAKTnpkRUxUbEJPVFl0TWpFNE9UUXdNelZHUkRBNE1Rc3dDUVlEVlFRR0V3SlZVekVMTUFrR0ExVUVDQk1DUTBFeApFakFRQmdOVkJBY1RDVU4xY0dWeWRHbHViekVUTUJFR0ExVUVDaE1LUVhCd2JHVWdTVzVqTGpFUE1BMEdBMVVFCkN4TUdhVkJvYjI1bE1JR2ZNQTBHQ1NxR1NJYjNEUUVCQVFVQUE0R05BRENCaVFLQmdRQ25YM3B0cVBzN0xEZXkKTXlsRXgyZlpsMzBOdXdFMjVvdlBla1BvL21WTHhBZEdaQnVMMHFEVVBkcUM1bGJzKzIxakVDYmgzeE5SN1lyVApqai9iU3dNcU55MzM2RWxVUjJWQkF3dlM0alBxL3ZrNGxsUzA0bDQwM2s4ZTdCYWhqZHJaUHlBY2cwUngwRW9LCko0YW9nV0I2bzg3ZFZialRpV1M3WE05c3FtWk1ld0lEQVFBQm80R1ZNSUdTTUI4R0ExVWRJd1FZTUJhQUZMTCsKSVNORWhwVnFlZFdCSm81ekVOaW5USTUwTUIwR0ExVWREZ1FXQkJUWFdsdXN2TWRXdmtCYVFDQWx5T3hSbmpEdgpQVEFNQmdOVkhSTUJBZjhFQWpBQU1BNEdBMVVkRHdFQi93UUVBd0lGb0RBZ0JnTlZIU1VCQWY4RUZqQVVCZ2dyCkJnRUZCUWNEQVFZSUt3WUJCUVVIQXdJd0VBWUtLb1pJaHZkalpBWUtBZ1FDQlFBd0RRWUpLb1pJaHZjTkFRRUYKQlFBRGdZRUFMSkJHM1grcDRjVG1KSGk0bi9SY3hHeUlxcmYxSlZodUlNc1JQbVlHOTBXREhBRmpXYXE4dTZjRwo1VjB4K05qOXdFVk80U0pBeUZQWnVMdHQ1U1JqSXQ5M3JOY3RlM2s3ZGVsTlVPUktna1plWVBkR3d6QU1NK0NaCks3TjJzcTd3RDNYMEZZbDR0YjBZVjlzZE5qVmNqRnRYSjBnTlZ0MmtoT2lOSlVtZFUwRT0KLS0tLS1FTkQgQ0VSVElGSUNBVEUtLS0tLQo=</data>
		<key>FairPlayKeyData</key>
		<data>LS0tLS1CRUdJTiBDT05UQUlORVItLS0tLQpBQUVBQWJqUEd6RHBPY1JwMUhPMkZ0aFdHRzJFVlFtNW1sS3R2UFdkaXBVbU9HajZZSjRhQldKSU50bWRXbEVECklSM055ZlIzRFV0UzQvYU5VcVBIelh0WmZwK3NOcUZkNWpUdzNpUWxsNGkrTW9zWGxlajkwamJEU0VPMitiN0QKcEVMWFhzaUZOQi81Zm90MDh3VCtMckc2ZzA1R21JLy85STZkeXN2S1p2QWE3ZHlobWVpaWEvWCt1TGRTVW5SRgp4citDQnBOeDc0SnpPSm5DOVE4YVFENTNlN1dOMUlYMUdXdGhTdTZPUlp4SzN6NDlOMVhnQ3FjN2RIVkp3dTZTCnBvb0phZ3FPTXNPK3NzTksyTExBbHZuTzQyQi9LaHRoMEg4L0lCcWNCUTZlckZlbU5zV0taLzFpUVd6ckxlVHIKS25lYzJZZzJuTWhOcktHaGJBb2FDdWpPNWE2NHk4QmgyTTB4U2k2MktPdWpoT29sdk0yazR2ZXhXNlJNS3Yzbgo0QkZDRGtkQzB2QTVXanZzbk5USFJvaEc1T3R0eXptaStrZENTMUlCMmV0UDIwN283dGo4OWM3OVoxTUM3RG9SCmRIeUNycWZCY0tvTm1KNTBCKzdNbGVFd1NPQSt4N0RxazI1SFRJZ3FWU0piMjIzbi9qSFZCZjhwMFgzRGE0SXIKc3lKZTU4a1pqdVNQWTQ2czdwV3laTVRXVEE3TFhMdjd4aE4yK1R6dlIxYkRHaE9MREQxNER5cllYVzhjaGdtTgpQYTNGYTlKVFUzM0lJYU5oY3FNbCtUekN5RzdpQ3NtNFZZZklUVzMzRHRRQ3RkS29qOUVsbkFPb1NrQWVXbnBaCmZZVGRTMGRYZkRMV2JReWtGVk53L1c1bkNLckk5QlI3c052bXhURnNvUWh2UzdXMWUyNmk1OWlBTVZyL3JWYkUKZ0phZnhJREFpaDl3R01xOEVnQ00vSDBXOFgrV0tzZWE5S3hVTXkvemliWFFwWHRwV25xUFI0SVpoK200aitQYgpXRklPZHJPejBhL1lxZVNTOFdGUi9DVGs4MGFIRjltaHhmNi9nT1NZRVAxVi9UK2w5eGgyaFJSbFYrN3lHbnFZCi9ydTk1WGJycWtaOXBmMnJPV1h2eGszcWZRSHNmbnZRaVU2UFhYMkgrcjB2OTdydy9PdkhBYnFITU1ENlFyNkcKNHNTZE1YbkhaSTl1Q1VMcjI0Znc2dVFqY1gxWCtxUkdZVnhkLzc2czRvKys1WWtlQXY4NElIZ0srdm40MzZnOAp5N0hFUzRiSzZuWGQzRERTWWxYWjVhS1pTQ2t0azJ3d05wN2R6YlZ6Q3VVS0Y4SGs5bjZiUWo2a1pJcTVMN2RoCkthVDQ3V2ppZHNwMzJQbmhhRCs0K2IxY2tnUkZsK2w1MUVIL1d5TmR2VXBnOFZGZTN4dDdWeHJ2dDlQVUFJWDkKdEFVUXIrbEFiNHVtWHJzczJzOUNteTNoUlorcVZ4QVdVR3BtaFJBSVVldDBlVE1ISUoxYWVPMHIwbW04UU0xTgplRU5HNHl0M2xmZytxank3d283WklkL21OU1o1enlNb3c1QXhPSm85V3F1ZUF2V0tsalpoTkJpMEszUThRajZiCnFXSlN3ZHJpemVsYUhjVDZXaktub0tQbXRIdVB3MEttQTcxeitPbkNadGxwUlFKOTZhbEdFNWNSMEM0NnlCLzAKK3FoL1dMRzZ2T2VWcEVPbFAyR1MwcTRRSDgyQ3Z2MXo4Z3M1THllT0FHN2Jmd1NoajB1aUNrSU9reWhHWjZPdQo5R3RHMUpSU1dZbUpIckxTemo0am9WK3hQeXdjT0ZGc1RHS2xDSGN3SWhJTGZ3dVNzeXh4c1VsNDd3REg4U000CnFoWWk1VmpNbWZyUmhrQm9ZQUhCUTJiVm5uMmFpT1VraUF1SkcvK05TbjFWMW04Nmg1V2MwMjB4YmpIV25iUGkKZTY5a09qRXllbEZqTFJUK1RtMEgxcWR5ZTQzOXdNUTUzWW1iVjl4cmxKc2d0bGpYCi0tLS0tRU5EIENPTlRBSU5FUi0tLS0tCg==</data>
		<key>AccountToken</key>
		<data>ewoJIkludGVybmF0aW9uYWxNb2JpbGVFcXVpcG1lbnRJZGVudGl0eSIgPSAiMzUyMDAyMDY2NzUxNTc5IjsKCSJDZXJ0aWZpY2F0ZVVSTCIgPSAiaHR0cHM6Ly9hbGJlcnQuYXBwbGUuY29tL2RldmljZXNlcnZpY2VzL2NlcnRpZnlNZSI7CgkiQWN0aXZhdGlvblRpY2tldCIgPSAiTUlJQ0JRSUJBVEFMQmdrcWhraUc5dzBCQVFVeGdjK2ZQd1FxSnNVa24wQUU0U0J2QUo5TEZFUzhuTHMxb29wSU10dTBDc1lwQWZqRit0U2NuNGRyQnpVZ0FnWm5VVmVmaDIwSE5TQUNCbWRSVjUrWFBYZ2dBQUFCTVFFZzd1N3U3dThnQUFBQ01RRWc3dTd1N3U4Z0FBQURNUUVnN3U3dTd1OGdBQUFFTVFFZzd1N3U3dThnQUFBUk1RRWc3dTd1N3U4Z0FBQVNNUUVnN3U3dTd1OGdBQUFUTVFFZzd1N3U3dThnQUFDaE1RRWc3dTd1N3U4Z0FBQ2lNUUVnN3U3dTd1OGdBQUNrTVFFZzd1N3U3dStmbHo0RUFRQUFBSitYUHdRQkFBQUFuNWRBQkFFQUFBQUVnWUIrbld0RDhNbjE5TmtvU1FGUE1lY1FmNjl1TkdVL2FUaTRqZTBCS0VhOWk2QVdkOGNueWliSnpCUjRyS2ZKQk44V3Y5QW5EU1JnaC96Z013U2RWaFRha01LVTlMK2QyU2NrME9yVnQweFhoU2Jsb0hVTjduUHQxdHc4ZlNpbE5zaDZrS0x4djVQcjBvc3AvclUvcEVWaE5mOFdUQVdERW5ITlprbjVrZjlCa2FPQm5UQUxCZ2txaGtpRzl3MEJBUUVEZ1kwQU1JR0pBb0dCQU8wNlAyVFI2Q0Njd1ZJR05POStMN0lKZmdBc1EwUG4rS3JyNWtxendMWUF4RFVtY0dsYXgxNlJmbkVTZ1FBYXBBMnROR0hCRGNkbmJVdGJiaHl5Z3paOWN6R1MybVVhWWl2VkhvakR6V1MrcHBZTmRnVmk4Rlk1UlRoaEEvRk9xWDE4RHNPZy94UWcwWUw4aGdaKzhMTVBMQkd1d0haV0pqQnhwcEw4SURqOUFnTUJBQUU9XG4iOwoJIlBob25lTnVtYmVyTm90aWZpY2F0aW9uVVJMIiA9ICJodHRwczovL2FsYmVydC5hcHBsZS5jb20vZGV2aWNlc2VydmljZXMvcGhvbmVIb21lIjsKCSJTZXJpYWxOdW1iZXIiID0gIkY5N01QMFU3RkY5WSI7CgkiSW50ZXJuYXRpb25hbE1vYmlsZVN1YnNjcmliZXJJZGVudGl0eSIgPSAiMzEwMTIwMTAyMTQ1NTE5IjsKCSJNb2JpbGVFcXVpcG1lbnRJZGVudGlmaWVyIiA9ICIzNTIwMDIwNjY3NTE1NyI7CgkiUHJvZHVjdFR5cGUiID0gImlQaG9uZTYsMSI7CgkiVW5pcXVlRGV2aWNlSUQiID0gIjBkMjJmYzhmOTMwMzQ1ZTUyYTI2MzgzZjYzNTczNDZhZGI4M2NiMjIiOwoJIkFjdGl2YXRpb25SYW5kb21uZXNzIiA9ICI0NzJDQTE3OS1GMDAyLTQ3RkMtOENENi0wQzRFQjQ5QkU5Q0IiOwoJIkFjdGl2aXR5VVJMIiA9ICJodHRwczovL2FsYmVydC5hcHBsZS5jb20vZGV2aWNlc2VydmljZXMvYWN0aXZpdHkiOwoJIkludGVncmF0ZWRDaXJjdWl0Q2FyZElkZW50aXR5IiA9ICI4OTAxMTIwMTAwMDAyMTQ1NTE5OSI7Cn0=</data>
		<key>AccountTokenSignature</key>
		<data>L7qZ/BjFVYIt2i9jm5V5lS/yM5y79zg6klMN5iXSz5Ck+DAy2HHRnNgi4GnxgawfPEP563VDLb+K7/D3cTilRl65vmtfiFu8/CC6/mR55irpL5GpVZXg1wi+iajCzlI/s6O5Y9IlAkmETHE7lVFcvHMyVos2Ys1LflWzCs/Ujxg=</data>
		<key>UniqueDeviceCertificate</key>
		<data>LS0tLS1CRUdJTiBDRVJUSUZJQ0FURS0tLS0tCk1JSUMrVENDQXFDZ0F3SUJBZ0lHQVdSYzFrUUJNQW9HQ0NxR1NNNDlCQU1DTUVVeEV6QVJCZ05WQkFnTUNrTmgKYkdsbWIzSnVhV0V4RXpBUkJnTlZCQW9NQ2tGd2NHeGxJRWx1WXk0eEdUQVhCZ05WQkFNTUVFWkVVa1JETFZWRApVbFF0VTFWQ1EwRXdIaGNOTVRnd056QXlNakV3TVRReldoY05NVGd3TnpBNU1qRXhNVFF6V2pCdU1STXdFUVlEClZRUUlEQXBEWVd4cFptOXlibWxoTVJNd0VRWURWUVFLREFwQmNIQnNaU0JKYm1NdU1SNHdIQVlEVlFRTERCVjEKWTNKMElFeGxZV1lnUTJWeWRHbG1hV05oZEdVeElqQWdCZ05WQkFNTUdUQXdNREE0T1RZd0xUQXdNREF3TlRCRApPRGcyTWtRd1JEQXdXVEFUQmdjcWhrak9QUUlCQmdncWhrak9QUU1CQndOQ0FBUmREb3hKRHg3WTJwd2NpSmVjCmRBbVNEUDg3cjd2N3FrNzZpenB0SFJDTTd6SHFhTmVET3B4QWNVcEtOOVFLMnZZTC96TTZic0pNbGNrd2ZWdGoKWDV0ZG80SUJVVENDQVUwd0RBWURWUjBUQVFIL0JBSXdBREFPQmdOVkhROEJBZjhFQkFNQ0JQQXdnZ0VYQmdrcQpoa2lHOTJOa0NnRUVnZ0VJTVlJQkJQK0VtcUdTVUEwd0N4WUVRMGhKVUFJREFJbGcvNFNxalpKRUVEQU9GZ1JGClEwbEVBZ1lGREloaTBORC9ocE8xd21NYk1Ca1dCR0p0WVdNRUVUUTRPbVEzT2pBMU9qaG1PbUZsT2pFMS80YkwKdGNwcEdUQVhGZ1JwYldWcEJBOHpOVEl3TURJd05qWTNOVEUxTnpuL2h1dVYwbVFZTUJZV0JHMWxhV1FFRGpNMQpNakF3TWpBMk5qYzFNVFUzLzRlYnlkeHRGakFVRmdSemNtNXRCQXhHT1RkTlVEQlZOMFpHT1ZuL2g2dVIwbVF5Ck1EQVdCSFZrYVdRRUtEQmtNakptWXpobU9UTXdNelExWlRVeVlUSTJNemd6WmpZek5UY3pORFpoWkdJNE0yTmkKTWpML2g3dTF3bU1iTUJrV0JIZHRZV01FRVRRNE9tUTNPakExT2pobU9tRmxPakUwTUJJR0NTcUdTSWIzWTJRSwpBZ1FGTUFNQ0FRQXdDZ1lJS29aSXpqMEVBd0lEUndBd1JBSWdjNjI1a290WDgxUE1DRnBFWWpnaDFPcGJyVmZ1CjVkQ1RObURmU2dtcnlNWUNJRjg3ZEw0WWdtYWdiMDNwdENMNW5saXJLY1lVaCtlSVR2Tmg1eTFESzhZMAotLS0tLUVORCBDRVJUSUZJQ0FURS0tLS0tdWtNdEg5UmRTUXZIekJ4N0ZpQkdyNy9LY21seFgvWHdvV2VXbldiNklSTT0KLS0tLS1CRUdJTiBDRVJUSUZJQ0FURS0tLS0tCk1JSUNGekNDQVp5Z0F3SUJBZ0lJT2NVcVE4SUMvaHN3Q2dZSUtvWkl6ajBFQXdJd1FERVVNQklHQTFVRUF3d0wKVTBWUUlGSnZiM1FnUTBFeEV6QVJCZ05WQkFvTUNrRndjR3hsSUVsdVl5NHhFekFSQmdOVkJBZ01Da05oYkdsbQpiM0p1YVdFd0hoY05NVFl3TkRJMU1qTTBOVFEzV2hjTk1qa3dOakkwTWpFME16STBXakJGTVJNd0VRWURWUVFJCkRBcERZV3hwWm05eWJtbGhNUk13RVFZRFZRUUtEQXBCY0hCc1pTQkpibU11TVJrd0Z3WURWUVFEREJCR1JGSkUKUXkxVlExSlVMVk5WUWtOQk1Ga3dFd1lIS29aSXpqMENBUVlJS29aSXpqMERBUWNEUWdBRWFEYzJPL01ydVl2UApWUGFVYktSN1JSem42NkIxNC84S29VTXNFRGI3bkhrR0VNWDZlQyswZ1N0R0hlNEhZTXJMeVdjYXAxdERGWW1FCkR5a0dRM3VNMmFON01Ia3dIUVlEVlIwT0JCWUVGTFNxT2tPdEcrVit6Z29NT0JxMTBobkxsVFd6TUE4R0ExVWQKRXdFQi93UUZNQU1CQWY4d0h3WURWUjBqQkJnd0ZvQVVXTy9XdnNXQ3NGVE5HS2FFcmFMMmUzczZmODh3RGdZRApWUjBQQVFIL0JBUURBZ0VHTUJZR0NTcUdTSWIzWTJRR0xBRUIvd1FHRmdSMVkzSjBNQW9HQ0NxR1NNNDlCQU1DCkEya0FNR1lDTVFEZjV6TmlpS04vSnFtczF3KzNDRFlrRVNPUGllSk1wRWtMZTlhMFVqV1hFQkRMMFZFc3EvQ2QKRTNhS1hrYzZSMTBDTVFEUzRNaVdpeW1ZK1J4a3Z5L2hpY0REUXFJL0JMK04zTEhxekpaVXV3MlN4MGFmRFg3Qgo2THlLaytzTHE0dXJrTVk9Ci0tLS0tRU5EIENFUlRJRklDQVRFLS0tLS0=</data>
		<key>DeviceConfigurationFlags</key>
		<string>0</string>
	</dict>
</dict>
</plist>

