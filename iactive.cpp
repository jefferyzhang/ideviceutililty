#include "stdafx.h"
#include <CoreFoundation/CFString.h>
#include <CoreFoundation/CFNumber.h>
#include <CoreFoundation/CFPropertyList.h>
#include <CoreFoundation/CFURLAccess.h>
#include "UtilTools.h"
#include "iDeviceUtil.h"
#include "iDeviceClass.h"
#include "iactive.h"
#include "DeviceInfo.h"
#include "RunExe.h"

using namespace System;
using namespace Runtime::InteropServices;


using namespace System::Xml;
using namespace System::IO;
using namespace System::Net;
using namespace System::Web;
using namespace System::Security::Cryptography::X509Certificates;
using namespace System::Net::Security;
using namespace System::Web::Script::Serialization;
using namespace System::Text::RegularExpressions;



void String2CharP(String^ str, char *Buff, int size) {
	auto h = Marshal::StringToHGlobalAnsi(str);
	char* str2 = (char *)(void*)h;
	if (str->Length >= size) {
		logIt(_T("Buffer size is too small"));
		return;
	}
	_sntprintf_s(Buff, 1024, size, _T("%s\0\0"), str2);
	Marshal::FreeHGlobal(h);
}

iactive::iactive()
{
	useName = "";
	passWord = "";
}

int iactive::Deactive(String^ udid, int delay)
{
	int ret = 1167;
	
	const char* lpudid =
		(const char*)(Marshal::StringToHGlobalAnsi(udid).ToPointer());
	iDeviceClass::start(lpudid);

	if (iDeviceClass::idDeviceReady(delay))
	{
		iDeviceClass* dc = iDeviceClass::getDeviceInstanceByUdid(lpudid);
		if (dc != NULL)
		{
			dc->connect();

			STRING aStatus = dc->copyValue("ActivationState");
			if (aStatus != "Unactivated")
			{
				ret = dc->deactive();
			}
			dc->disconnect();
		}
	}
	iDeviceClass::stop();
	return ret;
}

StringDictionary^ iactive::InfoToDict(iDeviceClass* dc)
{
	StringDictionary^ sDict = gcnew StringDictionary();
	STRING keys[] = {
		"ProductType",
			"ProductVersion",
			"UniqueChipID",
			"ActivationState",
			"IntegratedCircuitCardIdentity",
			"SerialNumber",
			"InternationalMobileEquipmentIdentity",
			"InternationalMobileSubscriberIdentity",
			"BasebandStatus",
			"ActivationPrivateKey",
			"ActivationPublicKey"
	};

	for each(STRING key in keys)
	{
		try
		{
			STRING s = dc->copyValue(key);
			sDict->Add(gcnew String(key.c_str()), s.empty() ? "" : gcnew String(s.c_str()));
		}
		catch (Exception^ ex)
		{
			logItcli(ex->ToString());
		}
	}
	CFTypeRef lvalue = dc->copyOnlyValue("", "ActivationInfo");
	if (lvalue != NULL)
	{
		CFDataRef pdata = CFPropertyListCreateXMLData(NULL, lvalue);
		String^ aplist = gcnew String((char *)CFDataGetBytePtr(pdata),0, CFDataGetLength(pdata));
		
		try
		{
			sDict->Add("ActivationInfo", aplist);
		}
		catch (Exception^)
		{
		}
	}
	return sDict;
}

int iactive::Active(StringDictionary^ _param, int delay)
{
	String^ udid = _param["udid"];
	BOOL verbose = _param->ContainsKey("verbose");
	logItcli(String::Format("active++ udid={0}", udid), verbose);
	int ret = 1167;
	const char* lpudid =
		(const char*)(Marshal::StringToHGlobalAnsi(udid).ToPointer());
	iDeviceClass::start(lpudid);
	{
		if (iDeviceClass::idDeviceReady(delay))
		{
			iDeviceClass* dc = iDeviceClass::getDeviceInstanceByUdid(lpudid);
			if (dc != NULL)
			{
				if ((ret = dc->connect()) == ERROR_SUCCESS)
				{
					StringDictionary^ sDict = InfoToDict(dc);
					if (!sDict->ContainsKey("udid"))
					{
						sDict->Add("udid", udid);
					}
					dc->disconnect();

					if (_param->ContainsKey("login") && _param->ContainsKey("password"))
					{
						if (!String::IsNullOrEmpty(_param["login"]) && !String::IsNullOrEmpty(_param["password"]))
						{
							sDict->Add("login", _param["login"]);
							sDict->Add("password", _param["password"]);
							useName = _param["login"];
							passWord = _param["password"];
						}
					}

					String^ aProductType = sDict["ProductType"];
					String^ aProductVersion = sDict["ProductVersion"];
					Version^ v = gcnew Version(aProductVersion);
					String^ aStatus = sDict["ActivationState"];
					if (String::IsNullOrEmpty(aStatus) || String::Compare(aStatus, "Unactivated", true) == 0)
					{
						ret = do_active_V2(verbose, "", sDict,TRUE);
						if (ret != ERROR_SUCCESS && ret != iCloudLock && ret != iErrorSIM)
						{
							int extocde = -1;
							String^ sPath = System::Environment::GetEnvironmentVariable("APSTHOME");
							sPath = Path::Combine(sPath, "tools\\winiMobileDevice\\ideviceactivation.exe");
							if (File::Exists(sPath))
							{
								String^ sParam = String::Format("activate -u {0} -d", udid);//Path::Combine(System::AppDomain::CurrentDomain->BaseDirectory, "Activation.exe")
								logItcli(sParam);
								CRunExe::runExe(sPath, sParam, extocde, 180 * 1000, true);
								logItcli(String::Format("Run ideviceactivation.exe to activate ret ={0}"), extocde);
								if (extocde == 0) {
									printf("DeviceConfigurationFlags=unknown\n");
									ret = 0;
								}
							}
						}
					}
					else
					{
						ret = iErrorAlreadyActive;
					}
					//done = true;
				}
				else
				{
					logItcli(String::Format("Fail to connect to device {0}, error={1}", udid, ret.ToString("X8")), verbose);
				}
			}
		}

	}
	iDeviceClass::stop();
	logItcli(String::Format("active-- ret={0}", ret), verbose);
	return ret;
}

int iactive::do_active_V2(BOOL verbose, String^ sUrl, StringDictionary^ sd, BOOL bFirst)//true
{
	int ret = 87;
	logItcli("do_active_V2: ++", verbose);

	// make a request file,
	StringBuilder^ sb = gcnew StringBuilder();
	//makeRequestDataFile(sb, sd);
	makeCarrierLockDataFile(sb, sd);
	//logItcli(String::Format("do_active_V2: data filename={0}", dataFilename));

	if (verbose)
	{
		logItcli(String::Format("request: {0}", sb->ToString()), verbose);
	}

	String^ sUrlParam = "/deviceservices/deviceActivation";

	StringCollection^ urls = getUrlsFromConfig(sUrlParam);
	if (!bFirst)
	{
		return do_access_apple(sUrl, sd, sb, verbose, bFirst);
	}

	for each(String^ sHost in urls)
	{
		ret = do_access_apple(sHost, sd, sb, verbose, bFirst);
		if (ret == iWaitTry)
		{
			Sleep(5000);
			ret = do_access_apple(sHost, sd, sb, verbose, bFirst);
		}

		if (ret == 0 || ret == iErrorSIM || ret == iCloudLock)
		{
			break;
		}
		if (!bFirst) break;
	}

	logItcli(String::Format("do_active_V2: -- ret={0}", ret), verbose);
	return ret;
}

String^ iactive::makeCarrierLockDataFile(StringBuilder^ sb, StringDictionary^ sdict)
{
	String^ ret = String::Empty;
	try
	{
		String^ value;
		String^ boundary = sdict["udid"]->ToUpper();

		// add InStoreActivation
		sb->AppendLine(String::Format("--{0}", boundary));
		sb->AppendLine("Content-Disposition: form-data; name=\"InStoreActivation\"");
		sb->AppendLine();
		sb->AppendLine("false");

		// add activation-info
		value = sdict["ActivationInfo"];
		if (!String::IsNullOrEmpty(value))
		{
			XmlDocument^ doc = gcnew XmlDocument();
			doc->XmlResolver = nullptr;
			doc->LoadXml(value);
			XmlNode^ n = doc->SelectSingleNode("/plist/dict");
			if (n != nullptr)
			{
				sb->AppendLine(String::Format("--{0}", boundary));
				sb->AppendLine("Content-Disposition: form-data; name=\"activation-info\"");
				sb->AppendLine();
				sb->AppendLine(n->OuterXml);
				sdict["activation-info"] = n->OuterXml;
			}
		}
		sb->AppendLine(String::Format("--{0}--", boundary));
		sb->AppendLine();
	}
	catch (Exception^)
	{
	}
	return ret;

}

StringCollection^ iactive::getUrlsFromConfig(String^ sUrlparam)
{
	StringCollection^ urls = gcnew StringCollection();
	String^ sPath = System::Environment::GetEnvironmentVariable("APSTHOME");
	if (String::IsNullOrEmpty(sPath))
	{
		urls->Add("https://albert.apple.com" + sUrlparam);
		urls->Add("https://srv4.futuredial.com/");
		return urls;
	}
	String^ sFileName = Path::Combine(sPath, "config.ini");
	if (!File::Exists(sFileName))
	{
		urls->Add("https://albert.apple.com" + sUrlparam);
		urls->Add("https://srv4.futuredial.com/");
	}
	else
	{
		const char* lpFileName =
			(const char*)(Marshal::StringToHGlobalAnsi(sFileName).ToPointer());
		TCHAR vvv[65536] = { 0 };
		GetPrivateProfileString("appleactiveproxy", "urls", "", vvv, 65536, lpFileName);
		if (_tcslen(vvv) > 0)
		{
			String^ sUrl = gcnew String(vvv);
			array<Char>^ idsp = { ',', ';' };
			array<String^>^ s = sUrl->Split(idsp, StringSplitOptions::RemoveEmptyEntries);
			for each(String^ value in s)
			{
				urls->Add(value);
			}
		}
		/*else
		{
		Dictionary<string, string> setsproxy = ini.GetSectionValues("appleactiveproxy");
		foreach(var value in setsproxy.Values)
		{
		urls.Add(value);
		}
		}*/
		if (urls->Count == 0)
		{
			urls->Add("https://albert.apple.com" + sUrlparam);
			urls->Add("http://srv4.futuredial.com/");
		}
	}
	return urls;
}

static bool ValidateServerCertificate(
	Object^ sender,
	X509Certificate^ certificate,
	X509Chain^ chain,
	SslPolicyErrors sslPolicyErrors)
{
	logIt(certificate->ToString());
	return true;
}

int  iactive::do_access_apple(String^ sHost, StringDictionary^ sd, StringBuilder^ sb, BOOL verbose, BOOL bFirst)// = true
{
	int ret = 87;
	Uri^ uri = gcnew Uri(sHost);//new Uri(new Uri(sHost), sUrlParam);

	logItcli(uri->ToString(), verbose);
	bool bhttps = String::Compare(uri->Scheme, "https", true) == 0;
	if (bhttps)
	{
		ServicePointManager::SecurityProtocol = SecurityProtocolType::Tls12;
		ServicePointManager::ServerCertificateValidationCallback = gcnew RemoteCertificateValidationCallback(ValidateServerCertificate);
	}
	HttpWebRequest^ request = (HttpWebRequest^)WebRequest::Create(uri);
	request->Timeout = RequestTimeOut;
REDIRECT:
	request->CookieContainer = CContainer;
	request->ServicePoint->Expect100Continue = false;
	if (bhttps)
	{
		request->UseDefaultCredentials = true;
	}
	extern BOOL bNoProxy ;
	if (bNoProxy)
	{
		request->Proxy = nullptr;
	}
	/*如果是icloudlock, 可以发
	* Header:Content-Type: application/x-www-form-urlencoded
	* Referer: https://albert.apple.com/deviceservices/deviceActivation
	* Body中增加:
	* login=fd.qa.tester%40gmail.com&password=408Fdail&activation-info-base64=sb的base64
	* &isAuthRequired=true
	* 作为内容就可以了
	*/
	request->ContentType = String::Format("multipart/form-data; boundary={0}", sd["udid"]->ToUpper());
	request->Method = "POST";
	if (String::Compare(uri->Host, "albert.apple.com", true) != 0)
	{
		request->Headers->Add("X-FD-TARGET-URL", "https://albert.apple.com/deviceservices/deviceActivation");
	}
	request->UserAgent = "iTunes/12.5.1 (Windows; Microsoft Windows 7 Business Edition Service Pack 1 (Build 7601)) AppleWebKit/7602.1050.4.4";
	request->Date = DateTime::Now;
	request->Headers->Add("X-Apple-Tz", "-18000");
	request->Headers->Add("X-Apple-Store-Front", "143441-1,32 ab:aF574PC0");
	request->Headers->Add("X-Apple-I-MD-RINFO", "17106176");
	request->Headers->Add("X-Apple-I-MD-M", "xtWf6zcZrxvFcEFcwjkFZKMpQTY8Qx4mcdK/Ri0znVLQokZ5GYNm+FXRRNxLin4B+UFAo7XaGgVat1oQ");
	request->Headers->Add("X-Apple-I-MD", "AAAABQAAABCD1OhzTh7L61chQs0/9sdCAAAAAg==");
	request->Headers->Add("Accept-Language", "en-us");

	try
	{
		//logItcli(request->ToString(), verbose);
		request->AllowAutoRedirect = false;

		request->AutomaticDecompression = DecompressionMethods::GZip | DecompressionMethods::Deflate;
		array<byte>^ data = Encoding::ASCII->GetBytes(sb->ToString());
		
		request->ContentLength = data->Length;
		Stream^ s = request->GetRequestStream();
		s->Write(data, 0, data->Length);

		HttpWebResponse^ res = (HttpWebResponse^)request->GetResponse();
		if (res->StatusCode == HttpStatusCode::Redirect ||
			res->StatusCode == HttpStatusCode::MovedPermanently)
		{
			String^ newUrl = res->GetResponseHeader("Location");
			logIt("Redirect1:{0}", newUrl);
			//do_access_apple(newUrl, sd, sb, verbose, TRUE);
			res->Close();
			Sleep(100);
			logIt("Redirect1:{0}", newUrl);
			request = (HttpWebRequest^)WebRequest::Create(newUrl);
			request->Timeout = RequestTimeOut;
			goto REDIRECT;
			ret = iWaitTry;
		}
		String^ responseStr = "";
		try
		{
			StreamReader^ reader = gcnew StreamReader(res->GetResponseStream());
			responseStr = reader->ReadToEnd();
			reader->Close();
		}
		catch (Exception^ e)
		{
			logItcli(e->ToString(), verbose);
		}

		finally
		{
			res->Close();
			s->Close();
		}

		if (verbose)
		{
			logItcli(String::Format("response: {0}", responseStr->ToString()), verbose);
		}

		if (!String::IsNullOrEmpty(responseStr) && ret != iWaitTry)
		{
			ret = CheckActiveReturn(responseStr, sHost, sd, verbose, bFirst);
			if (ret == iCloudLock)
			{
				if (do_send_active_info_FMI(sd, bFirst) == ERROR_SUCCESS)
				{
					ret = ERROR_SUCCESS;
				}

				if (ret!=ERROR_SUCCESS)
				{
					ret = iCloudLock;
				}
			}
		}
	}
	catch (Exception^ ex)
	{
		logItcli(ex->ToString(), verbose);
		ret = iErrorNetWork;
	}


	return ret;
}

int iactive::CheckActiveReturn(String^ s, String^ sUrl, StringDictionary^ infos, BOOL verbose, BOOL bFirst)
{
	int ret = 87;
	logItcli("CheckActiveReturn++", verbose);
	XmlDocument^ doc = gcnew XmlDocument();
	doc->XmlResolver = nullptr;
	bool bXml = true;
	try
	{
		XmlTextReader^ tr = gcnew XmlTextReader(gcnew StringReader(s));
		try
		{
			tr->DtdProcessing = DtdProcessing::Ignore;
			tr->Namespaces = false;
			doc->Load(tr);
			tr->Close();
		}
		catch (Exception^)
		{
			bXml = false;
		}

		logItcli("CheckActiveReturn: xml parser", verbose);
		if (doc->DocumentElement != nullptr && bXml)
		{
			XmlNode^ n = doc->SelectSingleNode("/Document/Protocol/plist");
			if (n != nullptr && n->HasChildNodes)
			{
				ret = doReturnPlist(sUrl, verbose, infos, n->OuterXml, bFirst);
			}
			else
			{
				bXml = false;
			}
		}
		else
			bXml = false;
		if (!bXml)
		{
			logItcli("CheckActiveReturn: html parser", verbose);
			HtmlAgilityPack::HtmlNode::ElementsFlags->Remove("form");
			HtmlAgilityPack::HtmlDocument^ htmldoc = gcnew HtmlAgilityPack::HtmlDocument();
			htmldoc->LoadHtml(s);
			//<script id="protocol" type="text/x-apple-plist"><plist version="1.0">
			//  <dict>
			//    <key>iphone-activation</key>
			//    <dict>
			//      <key>activation-record</key>
			//      <dict>
			//        <key>FairPlayKeyData</key>
			//        <data>LS0tLS1CRUdJTiBDT05UQUlORVItLS0tLQpBQUVBQVVPQTZtcmV1emZiT2FVSnFsVGNaOG56YWttSXIySENWVFUybWpsRHdTTC84aHNZZkVONFRtS3J2cmYzClNpajhrWlJVZGhUb1BCN2U0MmZId2pJNC9MZUxnWm01ZG43SXdPUlE0RDhvd21rQ0pqc1NPU1pZUlpKRGFtb04KS25Rd1hOYXpZMHFneTZsVG5kTE9tUVNvMGV2bGRZMFZIY1J4TW5aNDdFek40VVlmajNWQ3lxTHYwSVNmSEpoRApCd2poYWR0QW15amprY1hJU0llR3JwWGh4WFM5ZXVEOTNwU2FaNEZJRkhaUWZYS3YvZjNxSjJNWlpPWXhWK0ZmClJnVjVxVHlSR2tIZzk3UmUreFpXQUE3amhtU2JzMkQ0ZFpJOENEZGVVSE4xVXorRzdJdW5qaTBqbWFtOThLYnYKU1pLbnQ3TFlZUzVFMFdubUlIZVJNVEM2S1ZDeDB1bExSYTJ2YmJYSm9jMzJnQVVWR1ZyV01EWGVzS3dPc2FHZApYODE5Q2YrYnYzQmdub0lZREpiZWtSdXZyWlRDSmlpNVdyZ3laSGpkQ1hQZVFCUmJpdFRQSHhNSjZybzlQN0pJCmtLaXpGSk5aVHdEdnp3ZXNXYVlFOCtEWElZZm9lWlRhcFZrQWxIZFltWGJURmVLclJxeWw1d2trWWdwYnowUHYKU2NLSUpjLzBaNmZFa21pQ2g4S1pSaDdOdHg5eTJkaW91aGhvUThJNXk0VGVZa1VmcFZuOU5qSUlGRHpBcTZCYgozVGhGODRQNlNlaERMa3NQRDFSUGtLamNKZHRuV1NSZVFibzZ4WURzanUyaTVzalh5YnF1dnRRTGgwU3pHOEJYCkJlclJEWi9YRFVnR2RMem9OamN6YlNaMDlUTTlYOVNvK2kwbUk1WWZ1dFNKVEVVNlVmUzZ6b1VsbGxCcnJnMVEKMlNIa25RWFRENXhzUS8rZDBjckhtNDFoc1N4NmQ2VUI2R3NOSE42d1pxY2dEY205cytWUm9GcEh1VDZ0aEVUTQpwT1IrN0JINXFwUW9TTVhjSEpyMGdDT2RVbkFEVWRZOUZhV0ZPVXdFak54bEJzakZyRkR3SkVSd0VCblA5Rm9GCk83VTdwM3pLRW11MzY2YXI0cHIxRWNxckFuZUswbk9BMk9WcTFyWWh1eE84eWNiRlFuS2JHOUwvRkx2TTRTcTAKckRaTU9YaGUyUG5QVzYrKzJ0L2ZMY0ZMd3JZbmhCQ21JYzRaL3pUWHc5ZHlzNzJjY0hRbkJ6em16dzJObm4xNAplOFYxMlI1b29BT1JhNzNJdm5RYWoyYmxzR1J5WXZreWowMDZYZzBROExoRUFvMXBTNGYxc1VGRHpJekZ5UzM3CjdhMkcxNU5udUxVRVpURDVEMEdLQTQxNy9LYmxTYlNHM1I3TFkyMEpwZnBMUzVySXNOcUFsL3NBekdhMDArUVMKbkNCVGJweWZMa04yNUZUVGs4TTFJTDlFRGRNT1prdnp1V0tqckxweDlTOUZJUkdCRXFhT1QrVWIvMWVVcTh0cAplMGVIeHZrTmJyWkliZlk2WTVUdHl3bC9uQ1hmaEk0cUQvdE5nWm9yS0tGc2wxT0pZMGo2YXIvcHdDeVd2ZUFxCi9jSU1FdDRsbVlacFdpaVpsbjRJM2pacXU1L3hISjFpQzRrVlJ3QU85L3Y4U09xSUJvR0N2ZXpQcTNtcy9QQUMKTjF5aVdJUVFwUjkrTndKWGswRTM5NkpWRithQm9ST3FlSWt3eVlhZ3JXK1FQcFI2TnJ6b3U5enUxSXNvT3pLVgpxU2R0Z1lrbVlMZy9qa1dZbEJnb1p0dU1TbzMydnFGUkhrbms5OFp2bWJSZ0JldHR2ZmdPTWNkVDhhcTVHcThnClhvYTRnM2kwM0ttUGZ1T0tiNmY1TzhMd1hNTnQrYnpKRWh6bU5EK1hNbDRsdUU5Qm93YjE4QkJXc2R0Nk1GWE8KUmttM21jREZZelAxNkhpWnp5NEpKQ0taTnpTSktlcFMvZ0JsNE9oc3ZXa0hpYnliCi0tLS0tRU5EIENPTlRBSU5FUi0tLS0tCg==</data>
			//        <key>AccountTokenCertificate</key>
			//        <data>LS0tLS1CRUdJTiBDRVJUSUZJQ0FURS0tLS0tCk1JSURaekNDQWsrZ0F3SUJBZ0lCQWpBTkJna3Foa2lHOXcwQkFRVUZBREI1TVFzd0NRWURWUVFHRXdKVlV6RVQKTUJFR0ExVUVDaE1LUVhCd2JHVWdTVzVqTGpFbU1DUUdBMVVFQ3hNZFFYQndiR1VnUTJWeWRHbG1hV05oZEdsdgpiaUJCZFhSb2IzSnBkSGt4TFRBckJnTlZCQU1USkVGd2NHeGxJR2xRYUc5dVpTQkRaWEowYVdacFkyRjBhVzl1CklFRjFkR2h2Y21sMGVUQWVGdzB3TnpBME1UWXlNalUxTURKYUZ3MHhOREEwTVRZeU1qVTFNREphTUZzeEN6QUoKQmdOVkJBWVRBbFZUTVJNd0VRWURWUVFLRXdwQmNIQnNaU0JKYm1NdU1SVXdFd1lEVlFRTEV3eEJjSEJzWlNCcApVR2h2Ym1VeElEQWVCZ05WQkFNVEYwRndjR3hsSUdsUWFHOXVaU0JCWTNScGRtRjBhVzl1TUlHZk1BMEdDU3FHClNJYjNEUUVCQVFVQUE0R05BRENCaVFLQmdRREZBWHpSSW1Bcm1vaUhmYlMyb1BjcUFmYkV2MGQxams3R2JuWDcKKzRZVWx5SWZwcnpCVmRsbXoySkhZdjErMDRJekp0TDdjTDk3VUk3ZmswaTBPTVkwYWw4YStKUFFhNFVnNjExVApicUV0K25qQW1Ba2dlM0hYV0RCZEFYRDlNaGtDN1QvOW83N3pPUTFvbGk0Y1VkemxuWVdmem1XMFBkdU94dXZlCkFlWVk0d0lEQVFBQm80R2JNSUdZTUE0R0ExVWREd0VCL3dRRUF3SUhnREFNQmdOVkhSTUJBZjhFQWpBQU1CMEcKQTFVZERnUVdCQlNob05MK3Q3UnovcHNVYXEvTlBYTlBIKy9XbERBZkJnTlZIU01FR0RBV2dCVG5OQ291SXQ0NQpZR3UwbE01M2cyRXZNYUI4TlRBNEJnTlZIUjhFTVRBdk1DMmdLNkFwaGlkb2RIUndPaTh2ZDNkM0xtRndjR3hsCkxtTnZiUzloY0hCc1pXTmhMMmx3YUc5dVpTNWpjbXd3RFFZSktvWklodmNOQVFFRkJRQURnZ0VCQUY5cW1yVU4KZEErRlJPWUdQN3BXY1lUQUsrcEx5T2Y5ek9hRTdhZVZJODg1VjhZL0JLSGhsd0FvK3pFa2lPVTNGYkVQQ1M5Vgp0UzE4WkJjd0QvK2Q1WlFUTUZrbmhjVUp3ZFBxcWpubTlMcVRmSC94NHB3OE9OSFJEenhIZHA5NmdPVjNBNCs4CmFia29BU2ZjWXF2SVJ5cFhuYnVyM2JSUmhUekFzNFZJTFM2alR5Rll5bVplU2V3dEJ1Ym1taWdvMWtDUWlaR2MKNzZjNWZlREF5SGIyYnpFcXR2eDNXcHJsanRTNDZRVDVDUjZZZWxpblpuaW8zMmpBelJZVHh0UzZyM0pzdlpEaQpKMDcrRUhjbWZHZHB4d2dPKzdidFcxcEZhcjBaakY5L2pZS0tuT1lOeXZDcndzemhhZmJTWXd6QUc1RUpvWEZCCjRkK3BpV0hVRGNQeHRjYz0KLS0tLS1FTkQgQ0VSVElGSUNBVEUtLS0tLQo=</data>
			//        <key>DeviceCertificate</key>
			//        <data>LS0tLS1CRUdJTiBDRVJUSUZJQ0FURS0tLS0tCk1JSUM4ekNDQWx5Z0F3SUJBZ0lLQW1haTRZKzRSSEZPY2pBTkJna3Foa2lHOXcwQkFRVUZBREJhTVFzd0NRWUQKVlFRR0V3SlZVekVUTUJFR0ExVUVDaE1LUVhCd2JHVWdTVzVqTGpFVk1CTUdBMVVFQ3hNTVFYQndiR1VnYVZCbwpiMjVsTVI4d0hRWURWUVFERXhaQmNIQnNaU0JwVUdodmJtVWdSR1YyYVdObElFTkJNQjRYRFRFek1UQXpNREUzCk1qVTBObG9YRFRFMk1UQXpNREUzTWpVME5sb3dnWU14TFRBckJnTlZCQU1XSkRReE1FSTBOMEZETFVVeE9EUXQKTkRnek9DMDVOemsyTFVORU5EVXlNVU5DUkRZelJERUxNQWtHQTFVRUJoTUNWVk14Q3pBSkJnTlZCQWdUQWtOQgpNUkl3RUFZRFZRUUhFd2xEZFhCbGNuUnBibTh4RXpBUkJnTlZCQW9UQ2tGd2NHeGxJRWx1WXk0eER6QU5CZ05WCkJBc1RCbWxRYUc5dVpUQ0JuekFOQmdrcWhraUc5dzBCQVFFRkFBT0JqUUF3Z1lrQ2dZRUF1SThzQXVxZTNtNCsKV3kvcG9BT2o1dysvZDJkZDJJdUpnZEVRSmEzazR2WlRncUpObTlkVyt1ZzRNQURFZkdOSUhVMTdjVWhiMThrbwp5UjE2WEtjL3dHZDNaWFBKaTRlZHl5VnlocXh3c3c5cEtlUjgxY21pWFM0cHVmTWtyK2JmRE1vbzhpMFR5T0xkCnVlcHJ5aXBrRHlBaitGdUI5bXhCUG5GaHFkamtFT2tDQXdFQUFhT0JsVENCa2pBZkJnTlZIU01FR0RBV2dCU3kKL2lFalJJYVZhbm5WZ1NhT2N4RFlwMHlPZERBZEJnTlZIUTRFRmdRVUVGNFY0VHBOSVM4amp0NUVoK05LUG5weApBQTh3REFZRFZSMFRBUUgvQkFJd0FEQU9CZ05WSFE4QkFmOEVCQU1DQmFBd0lBWURWUjBsQVFIL0JCWXdGQVlJCkt3WUJCUVVIQXdFR0NDc0dBUVVGQndNQ01CQUdDaXFHU0liM1kyUUdDZ0lFQWdVQU1BMEdDU3FHU0liM0RRRUIKQlFVQUE0R0JBQUgrZEhyd040ZU5lVitVUnJzRklrMkZjWVlLb0hBdVY4YmFkVW9xTXphZEhVQU5ZcnMwTTNLZgpxRUxmcVUzQmpFTHY4M0FJaGdieDZ5Q09JUExMcURXSzNlRmNORllNRHBZajNqdHZpNEZaRy8zMzJrbTFhZEVpClhSdUJobTc0ekpacnB0OU1sZTZvSnhSZG5xeGJ4dHBVcVByUC9BVGEyM3FXaTVpQjRPSnUKLS0tLS1FTkQgQ0VSVElGSUNBVEUtLS0tLQo=</data>
			//        <key>AccountTokenSignature</key>
			//        <data>T89GgEAZuGxrKIpZbKGQ9pHDIORMERAbceEGbILSZOphGL1I3sT9DV2tbVP9y8HnOg+moTBzNt4cnUcGLaGv8w4RvUHGLp5ajdVCAAW5p2Uw3sJfSpV5V6Zjcaw91Fdg9/rz53OTFNbjOf04tPLEAInvFNYiUpEW4zpi5xNLIw0=</data>
			//        <key>AccountToken</key>
			//        <data>ewoJIkFjdGl2aXR5VVJMIiA9ICJodHRwczovL2FsYmVydC5hcHBsZS5jb20vZGV2aWNlc2VydmljZXMvYWN0aXZpdHkiOwoJIkFjdGl2YXRpb25SYW5kb21uZXNzIiA9ICI2QjhDRTJGNS04REJFLTRENDQtQUNEQS0yMzMyQTgxMUQwNzUiOwoJIlVuaXF1ZURldmljZUlEIiA9ICJhMWMyOTZiNzA3NTc4NWI2MmRhNmUyN2Y4YjA4Y2EwNTkxMjY5MmUyIjsKCSJBY3RpdmF0aW9uVGlja2V0IiA9ICJIekZPVUJRM0RpMmFMUmRWSVpid3RpdnFrYW50T2N0RmRoQkZ5Z1lZd2EreDIxc1JIcTlzZ0JZZEVSUEJzNzV1VStNaDF3QUFwNjk1c3RueXY0Zjc4S05OcXR2MUV1SjlvV0xaMjR1UmNqeEszVElsY05EYktKZUo3bjBRSkNocFBScy9kaXM3TCs1NHNjYWVRTXkrR21ua3RJb1ZONkR5bFhvME9EdFYvYmdqUGtoRFlVS091N0NaK01hL1J4ZlFIWXRSZG1RVlBaNGo2MFQ1R3YvQWV4SjQvajd6TTdLSkdYeEVxblEzK2liZThWWUNRN0pBSzFacDVwVXdhdko4UzdNd3U0aXhrUEh2bFhvTnV0L2U5WW5pQ1V3MUZEWHFMd3AyWnhNd3A0a3F4Vk12bFlBb2Q3cXlUOTFtOGpOV1dLU0tCZnlDY1ZRcnJwVEZLd2dNVXg4Z2RFRlZYeUhmcWNaMk1HaWxHamEzRktVTHkxQXhjVEZVaVpWRnQzSzFjY3hGVGNCWnMybXRDSEZqVE5jalZBK2JGeUJLNW5SMC9RMXhnaWlZcXhNdkZlVm5lMnlUYUZzclJwS3IwM2hHWHBHYVNBdngxTlVLN0hyLy8zSFFiazdnN2tyS1B2M014b0wrUm5nSTFpMWN1cWVtalFqS0hTbGtyTWZpQVUzTUdML1ZsUzJPZjRNS0lRcVpoUjZSOHFwNzVzRkhkb2RvWVJCeG0xaUsyQWlkR3B3NjlCMTlXV1ZKUHhXelB1RFhcbiI7CgkiUGhvbmVOdW1iZXJOb3RpZmljYXRpb25VUkwiID0gImh0dHBzOi8vYWxiZXJ0LmFwcGxlLmNvbS9XZWJPYmplY3RzL0FMVW5icmljay53b2Evd2EvcGhvbmVIb21lIjsKfQ==</data>
			//      </dict>
			//      <key>unbrick</key>
			//      <true/>
			//    </dict>
			//  </dict>
			//</plist></script>
			logItcli("CheckActiveReturn: selectsingleNode parser", verbose);
			HtmlAgilityPack::HtmlNode^ htmlNode = htmldoc->DocumentNode->SelectSingleNode("//*[@id=\"IPAJingleEndPointErrorPage\"]");
			HtmlAgilityPack::HtmlNode^ htmlNode1 = htmldoc->DocumentNode->SelectSingleNode("//*[@id=\"EndpointError\"]/h1");

			if (htmlNode != nullptr)
			{
				if (verbose)
				{
					logItcli("[*#sourcemsg#*]=" + htmlNode->OuterHtml, verbose);
				}
				logItcli(String::Format("[*#message#*]={0}", Convert::ToBase64String(Encoding::UTF8->GetBytes(htmlNode->OuterHtml))), TRUE);
				ret = iErrorSIM;
				if (htmlNode->OuterHtml->IndexOf("Choose Your Carrier to Continue")>0){
					ret = iErrorSelectCarrier;
				}
				else if (htmlNode->OuterHtml->IndexOf("Demo Registration")>0){
					ret = iErrorDemoPhone;
				}	
				else if (htmlNode->OuterHtml->IndexOf("Success, your iPhone has been unlocked", StringComparison::CurrentCultureIgnoreCase) >= 0) {
					ret = iErrorAlreadyActive;
				}
				else if (htmlNode->OuterHtml->IndexOf("There is a problem with your iPhone") > 0) {
					ret = iErrorProblem;
				}

			}else if (htmlNode1 != nullptr) {
				String^ inntxt = htmlNode1->InnerText;
				logIt(inntxt);
				if (!String::IsNullOrEmpty(inntxt)) {
					if (String::Compare(inntxt, "Choose Your Carrier to Continue", true) == 0) {
						ret = iErrorSelectCarrier;
					}
					else if (String::Compare(inntxt, "Demo Registration", true) == 0) {
						ret = iErrorDemoPhone;
					}
					else if (inntxt->IndexOf("Success, your iPhone has been unlocked", StringComparison::CurrentCultureIgnoreCase) >= 0) {
						ret = iErrorAlreadyActive;
					}
					else if (String::Compare(inntxt, "There is a problem with your iPhone", true) == 0) {
						ret = iErrorProblem;
					}
					else {
						ret = iErrorSIM;
					}
				}
			}
			else if ((htmlNode = htmldoc->DocumentNode->SelectSingleNode("//*[@id=\"protocol\" and @type=\"text/x-apple-plist\"]")) != nullptr)
			{
				String^ plistString = htmlNode->InnerHtml;
				String^ pat = "<key>DeviceConfigurationFlags</key>\\W*<string>(.*?)</string>";
				Regex^ r = gcnew Regex(pat, RegexOptions::IgnoreCase | RegexOptions::Multiline | RegexOptions::IgnorePatternWhitespace);
				Match^ m = r->Match(plistString);
				if (m->Success){
					printf("hasDeviceConfigurationFlags=1\n");
					char buff[1024] = { 0 };
					String2CharP(m->Groups[1]->Value, buff, 1024);
					printf("DeviceConfigurationFlags_Value=%s\n", buff);
					if (String::Compare(m->Groups[1]->Value, "0") != 0)
						printf("DeviceConfigurationFlags=1\n");
				}
				/*if (plistString->IndexOf("<key>DeviceConfigurationFlags</key>") > 0)
				{
					printf("DeviceConfigurationFlags=1\n");
				}*/
				ret = doReturnPlist(sUrl, verbose, infos, htmlNode->InnerHtml, bFirst);
			}
			else if ((htmlNode = htmldoc->DocumentNode->SelectSingleNode("//input[@type=\"password\" and @name=\"password\"]")) != nullptr)
			{
				if (!String::IsNullOrEmpty(useName) && !String::IsNullOrEmpty(passWord))
				{
					fmiResponeItem = gcnew Dictionary<String^, String^>();
					HtmlAgilityPack::HtmlNode^ auth_form = htmldoc->GetElementbyId("auth_form");
					for each(HtmlAgilityPack::HtmlNode^ node in auth_form->SelectNodes(".//input"))
					{
						HtmlAgilityPack::HtmlAttribute^ attrName = node->Attributes["name"];
						if (attrName != nullptr)
						{
							if (String::Compare("login", attrName->Value, TRUE) == 0)
							{
								fmiResponeItem[attrName->Value] = useName;
							}
							else if (String::Compare("password", attrName->Value, TRUE) == 0)
							{
								fmiResponeItem[attrName->Value] = passWord;
							}
							else
							{
								HtmlAgilityPack::HtmlAttribute^ attrValue = node->Attributes["value"];
								if (!String::IsNullOrEmpty(attrValue->Value))
								{
									fmiResponeItem[attrName->Value] = attrValue->Value;
								}
							}

						}
					}
				}
				htmlNode = htmldoc->DocumentNode->SelectSingleNode("//section[@class=\"headline\"]");
				if (verbose)
				{
					logItcli("[*#sourcemsg#*]=" + htmlNode->OuterHtml, verbose);
				}
				logItcli(String::Format("[*#message#*]={0}", Convert::ToBase64String(Encoding::UTF8->GetBytes(htmlNode->OuterHtml))), TRUE);
				ret = iCloudLock;
			}

		}

	}
	catch (Exception^ ex)
	{
		logItcli(ex->ToString());
	}

	logItcli("CheckActiveReturn--");

	return ret;
}

int iactive::doReturnPlist(String^ sUrl, BOOL verbose, StringDictionary^ infos, String^ sPlist, BOOL bFirst)
{
	int ret = 87;
	logItcli(String::Format("doReturnPlist: ++"));
	String^ tempfile = Path::GetTempFileName();
	StringBuilder^ sb = gcnew StringBuilder();
	sb->AppendLine("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
	sb->AppendLine("<!DOCTYPE plist PUBLIC \"-//Apple//DTD PLIST 1.0//EN\" \"http://www.apple.com/DTDs/PropertyList-1.0.dtd\">");
	sb->Append(sPlist);
	File::WriteAllText(tempfile, sb->ToString());
	if (bFirst)
	{
		ret = do_active_stage_2(tempfile, verbose, infos);
		File::Delete(tempfile);
		if (ret == 0)
		{
			do_active_V2(verbose, sUrl, infos, FALSE);
		}
		else
		{
			ret = iActiveFail;
			logItcli(String::Format("doReturnPlist: active failed. ret={0}", ret));
		}
	}
	else
	{
		ret = do_active_stage_3(tempfile, verbose);
		File::Delete(tempfile);
	}
	logItcli(String::Format("doReturnPlist: -- ret={0}", ret));
	return ret;
}

int iactive::do_active_stage_2(String^ ticketFilename, BOOL verbose, StringDictionary^ sDict)
{
	int ret = 87;
	logItcli("do_active_stage_2++");
	String^ sudid = sDict["udid"];

	logItcli(String::Format("do_active_stage_2: ++ ticket: {0}, {1}", ticketFilename, sudid));
	if (verbose)
	{
		if (File::Exists(ticketFilename))
			logItcli(String::Format("ticket info: {0}",File::ReadAllText(ticketFilename)));
	}
	try
	{
		iDeviceClass::stop();
		iDeviceClass::start(iDeviceClass::curDevice.udid_current);

		const char* file_path =
			(const char*)(Marshal::StringToHGlobalAnsi(ticketFilename).ToPointer());

		CFPropertyListRef plistref;
		CFStringRef       errorString;
		CFDataRef         resourceData;
		BOOL           status;
		SInt32            errorCode;

		if (!PathFileExists(file_path)) return ERROR_INVALID_PARAMETER;

		CFURLRef fileURL = CFURLCreateWithFileSystemPath(CFAllocatorGetDefault(), __CFStringMakeConstantString(file_path), kCFURLWindowsPathStyle, false);
		// Read the XML file.
		status = CFURLCreateDataAndPropertiesFromResource(
			CFAllocatorGetDefault(),
			fileURL,
			&resourceData,            // place to put file data
			NULL,
			NULL,
			&errorCode);

		// Reconstitute the dictionary using the XML data.
		plistref = CFPropertyListCreateFromXMLData(CFAllocatorGetDefault(),
			resourceData,
			kCFPropertyListImmutable,
			&errorString);

		if (resourceData) {
			CFRelease(resourceData);
		}
		else {
			CFRelease(errorString);
		}

		if (plistref != NULL)
		{
			CFDictionaryRef iphone_activation = (CFDictionaryRef)CFDictionaryGetValue((CFDictionaryRef)plistref, CFSTR("iphone-activation"));
			if (iphone_activation == NULL)
				iphone_activation = (CFDictionaryRef)CFDictionaryGetValue((CFDictionaryRef)plistref, CFSTR("device-activation"));
			if (iphone_activation == NULL)
				iphone_activation = (CFDictionaryRef)CFDictionaryGetValue((CFDictionaryRef)plistref, CFSTR("ActivationRecord"));
			if (iphone_activation != NULL)
			{
				CFDictionaryRef dict = (CFDictionaryRef)CFDictionaryGetValue(iphone_activation, CFSTR("activation-record"));
				if (dict==NULL)
					dict = (CFDictionaryRef)CFDictionaryGetValue(iphone_activation, CFSTR("ActivationRecord"));
				std::list<CFTypeRef> wct;
				wct.push_back( CFDictionaryGetValue(dict, CFSTR("AccountTokenCertificate")));
				wct.push_back(CFDictionaryGetValue(dict, CFSTR("AccountToken")));
				wct.push_back(CFDictionaryGetValue(dict, CFSTR("FairPlayKeyData")));
				wct.push_back(CFDictionaryGetValue(dict, CFSTR("DeviceCertificate")));
				wct.push_back(CFDictionaryGetValue(dict, CFSTR("AccountTokenSignature")));
				//CFStringRef keys[] = { CFSTR("AccountTokenCertificate"), CFSTR("AccountToken"), CFSTR("FairPlayKeyData"), CFSTR("DeviceCertificate"), CFSTR("AccountTokenSignature") };
				std::list<CFStringRef> keys;
				keys.push_back(CFSTR("AccountTokenCertificate")); 
				keys.push_back(CFSTR("AccountToken"));
				keys.push_back(CFSTR("FairPlayKeyData")); 
				keys.push_back(CFSTR("DeviceCertificate")); 
				keys.push_back(CFSTR("AccountTokenSignature"));
				if (iDeviceClass::idDeviceReady(10 * 1000))
				{
					logItcli("do_active_stage_2 call idDeviceReady api.");
					iDeviceClass* dc = iDeviceClass::getDeviceInstanceByUdid(iDeviceClass::curDevice.udid_current);//iDeviceClass.getDeviceInstanceByUdid(sudid);
					if (dc != NULL)
					{
						logItcli("do_active_stage_2 call getDeviceInstanceByUdid api.");
						if ((ret = dc->connect()) == ERROR_SUCCESS)
						{
							//bdone = true;
							logItcli("do_active_stage_2 call connect api.");
							ret = dc->active(keys, wct);
							if (ret == ERROR_SUCCESS)
							{
								logItcli("do_active_stage_2 call active api.");
								CFTypeRef lvalue = dc->copyOnlyValue("", "ActivationInfo");
								if (lvalue != NULL)
								{
									logItcli("do_active_stage_2 call copyValue_IntPtr api.");
									
									CFDictionaryRef ddict = (CFDictionaryRef)lvalue;
									CFDataRef pdata = CFPropertyListCreateXMLData(NULL, lvalue);
									String^ aplist = gcnew String((char *)CFDataGetBytePtr(pdata), 0, CFDataGetLength(pdata));
									sDict["ActivationInfo"] = aplist;
								}
								else
								{
									logItcli(String::Format("ActivationInfo Lost"));
								}
							}
							else
							{
								logItcli(String::Format("active fail {0}", ret));
							}
							dc->disconnect();
						}
						else
						{
							logItcli(String::Format("device connect fail {0}", ret));
						}
					}
					else
						logItcli(String::Format("getDeviceInstanceByUdid null {0}", sudid));
				}
				else
				{
					logItcli(String::Format("idDeviceReady false {0}", sudid));
				}

			}
		}
		else
		{
			logItcli("do_active_stage_2: get PropertyList fail");
		}
	}
	catch (AccessViolationException^ ex)
	{
		logItcli(String::Format("do_active_stage_2: AccessViolationException {0}", ex->Message));
		if (File::Exists(ticketFilename))
			logItcli(String::Format("ticket info: {0}", File::ReadAllText(ticketFilename)));
	}
	catch (Exception^) {}
	logItcli(String::Format("do_active_stage_2: -- {0}", ret));
	return ret;
}

int  iactive::do_active_stage_3(String^ ticketFilename, BOOL verbose)
{
	int ret = 87;
	logItcli("do_active_stage_3++");
	logItcli(String::Format("do_active_stage_3: ++ ticket: {0}", ticketFilename));
	if (verbose)
	{
		if (File::Exists(ticketFilename))
			logItcli(String::Format("ticket info: {0}", File::ReadAllText(ticketFilename)));
	}

	const char* file_path =
		(const char*)(Marshal::StringToHGlobalAnsi(ticketFilename).ToPointer());

	CFPropertyListRef plistref;
	CFStringRef       errorString;
	CFDataRef         resourceData;
	BOOL           status;
	SInt32            errorCode;

	if (!PathFileExists(file_path)) return ERROR_INVALID_PARAMETER;

	CFURLRef fileURL = CFURLCreateWithFileSystemPath(CFAllocatorGetDefault(), __CFStringMakeConstantString(file_path), kCFURLWindowsPathStyle, false);
	// Read the XML file.
	status = CFURLCreateDataAndPropertiesFromResource(
		CFAllocatorGetDefault(),
		fileURL,
		&resourceData,            // place to put file data  
		NULL,
		NULL,
		&errorCode);

	// Reconstitute the dictionary using the XML data.
	plistref = CFPropertyListCreateFromXMLData(CFAllocatorGetDefault(),
		resourceData,
		kCFPropertyListImmutable,
		&errorString);

	if (resourceData) {
		CFRelease(resourceData);
	}
	else {
		CFRelease(errorString);
	}

	try
	{
		if (plistref != NULL)
		{
			CFDictionaryRef iphone_activation = (CFDictionaryRef)CFDictionaryGetValue((CFDictionaryRef)plistref, CFSTR("iphone-activation"));
			if (iphone_activation == NULL)
				iphone_activation =(CFDictionaryRef)CFDictionaryGetValue((CFDictionaryRef)plistref, CFSTR("device-activation"));
			if (iphone_activation != NULL)
			{
				CFBooleanRef ackrec = (CFBooleanRef)CFDictionaryGetValue(iphone_activation,CFSTR("ack-received"));
				if (ackrec != NULL)
				{
					BOOL ack = CFBooleanGetValue(ackrec);
					if (ack)
					{
						ret = 0;
					}
					else
					{
						ret = iActiveFail;
					}
				}
				else
				{
					ret = iActiveFail;
					logItcli("do_active_stage_3: get ack-received fail");
				}

			}
		}
		else
		{
			logItcli("do_active_stage_3: get PropertyList fail");
		}
	}
	catch (AccessViolationException^ ex)
	{
		logItcli(String::Format("do_active_stage_3: AccessViolationException {0}", ex->Message));
		if (File::Exists(ticketFilename))
			logItcli(String::Format("ticket info: {0}", File::ReadAllText(ticketFilename)));
	}
	catch (Exception^) {}
	logItcli(String::Format("do_active_stage_3: -- {0}", ret));
	return ret;

}

int iactive::ActiveV1(StringDictionary^ _param, int delay)
{
	String^ udid = _param["udid"]; 
	BOOL verbose = _param->ContainsKey("verbose");
	logItcli(String::Format("activeV1++ udid={0}", udid), verbose);
	int ret = 1167;
	const char* lpudid =
		(const char*)(Marshal::StringToHGlobalAnsi(udid).ToPointer());
	iDeviceClass::start(lpudid);
	{
		if (iDeviceClass::idDeviceReady(delay))
		{
			iDeviceClass *dc = iDeviceClass::getDeviceInstanceByUdid(lpudid);
			if (dc != NULL)
			{
				if ((ret = dc->connect()) == ERROR_SUCCESS)
				{
					STRING ssStatus = dc->copyValue("ActivationState");
					StringDictionary^ sDict = gcnew StringDictionary();//InfoToDict(dc);
					if (!sDict->ContainsKey("udid"))
					{
						sDict->Add("udid", udid);
					}
					sDict->Add("ActivationState", gcnew String(ssStatus.c_str()));
					dc->disconnect();

					if (_param->ContainsKey("login") && _param->ContainsKey("password"))
					{
						if (!String::IsNullOrEmpty(_param["login"]) && !String::IsNullOrEmpty(_param["password"]))
						{
							sDict->Add("login", _param["login"]);
							sDict->Add("password", _param["password"]);
							useName = _param["login"];
							passWord = _param["password"];

						}
					}

					//String^ aProductType = sDict["ProductType"];
					//String^ aProductVersion = sDict["ProductVersion"];
					//Version v = new Version(aProductVersion);
					String^ aStatus = sDict["ActivationState"];
					if (String::IsNullOrEmpty(aStatus) || String::Compare(aStatus, "Unactivated", true) == 0)
					{
						ret = do_active_V3(verbose, "", sDict, TRUE);
						if (ret == iErrorAlreadyActive) ret = ERROR_SUCCESS;
						if (ret != ERROR_SUCCESS && ret != iCloudLock && ret != iErrorSIM && ret != iErrorDemoPhone && 
							ret != iErrorSelectCarrier && ret != iErrorProblem)
						{
							printf("DeviceConfigurationFlags=unknown\n");
							int extocde = -1;
							String^ sPath = System::Environment::GetEnvironmentVariable("APSTHOME");

							sPath = Path::Combine(sPath, "tools\\winiMobileDevice\\ideviceactivation.exe");
							if (File::Exists(sPath))
							{
								String^ sParam = String::Format("activate -u {0}", udid);//Path::Combine(System::AppDomain::CurrentDomain->BaseDirectory, "Activation.exe")
								logItcli(sParam);
								CRunExe::runExe(sPath, sParam, extocde, 180 * 1000, true);
								logItcli(String::Format("Run ideviceactivation.exe to activate ret ={0}", extocde));
								
								if (extocde == 0 || extocde == iCloudLock) ret = extocde;
							}
							
						}
					}
					else
					{
						ret = iErrorAlreadyActive;
					}
					//done = true;
				}
				else
				{
					logItcli(String::Format("Fail to connect to device {0}, error={1}", udid, ret.ToString("X8")));
				}
			}
		}
	}
	iDeviceClass::stop();
	logItcli(String::Format("activeV1-- ret={0}", ret));
	return ret;

}

int iactive::CarrierLock(StringDictionary^ _param, int delay)
{
	String^ udid = _param["udid"];
	verbose = _param->ContainsKey("verbose");
	logIt("CarrierLock++ udid={0}", udid);
	int ret = 1167;
	const char* lpudid =
		(const char*)(Marshal::StringToHGlobalAnsi(udid).ToPointer());
	iDeviceClass::start(lpudid);
	{
		if (iDeviceClass::idDeviceReady(delay))
		{
			iDeviceClass *dc = iDeviceClass::getDeviceInstanceByUdid(lpudid);
			if (dc != NULL)
			{
				if ((ret = dc->connect()) == ERROR_SUCCESS)
				{
					STRING ssStatus = dc->copyValue("ActivationState");
					StringDictionary^ sDict = gcnew StringDictionary();//InfoToDict(dc);
					if (!sDict->ContainsKey("udid"))
					{
						sDict->Add("udid", udid);
					}
					sDict->Add("ActivationState", gcnew String(ssStatus.c_str()));
					dc->disconnect();

					String^ aStatus = sDict["ActivationState"];
					if (String::IsNullOrEmpty(aStatus) || String::Compare(aStatus, "Activated", true) == 0)
					{
						ret = do_active_Carrier(verbose, "", sDict, TRUE);
						
					}
					else
					{
						ret = iErrorUnactived;
					}
				}
				else
				{
					logItcli(String::Format("Fail to connect to device {0}, error={1}", udid, ret.ToString("X8")));
				}
			}
		}
	}
	iDeviceClass::stop();
	logItcli(String::Format("CarrierLock-- ret={0}", ret));
	return ret;

}

int iactive::ActiveV2(StringDictionary^ _param, int delay)
{
	String^ udid = _param["udid"];
	verbose = _param->ContainsKey("verbose");
	logIt("activeV2++ udid={0}", udid);
	int ret = 1167;
	const char* lpudid =
		(const char*)(Marshal::StringToHGlobalAnsi(udid).ToPointer());
	iDeviceClass::start(lpudid);
	{
		if (iDeviceClass::idDeviceReady(delay))
		{
			iDeviceClass *dc = iDeviceClass::getDeviceInstanceByUdid(lpudid);
			if (dc != NULL)
			{
				if ((ret = dc->connect()) == ERROR_SUCCESS)
				{
					STRING ssStatus = dc->copyValue("ActivationState");
					StringDictionary^ sDict = gcnew StringDictionary();//InfoToDict(dc);
					if (!sDict->ContainsKey("udid"))
					{
						sDict->Add("udid", udid);
					}
					sDict->Add("ActivationState", gcnew String(ssStatus.c_str()));
					dc->disconnect();

					if (_param->ContainsKey("login") && _param->ContainsKey("password"))
					{
						if (!String::IsNullOrEmpty(_param["login"]) && !String::IsNullOrEmpty(_param["password"]))
						{
							sDict->Add("login", _param["login"]);
							sDict->Add("password", _param["password"]);
							useName = _param["login"];
							passWord = _param["password"];

						}
					}

					String^ aStatus = sDict["ActivationState"];
					if (String::IsNullOrEmpty(aStatus) || String::Compare(aStatus, "Unactivated", true) == 0)
					{
						ret = do_active_V4(verbose, "", sDict, TRUE);
						if (ret == iErrorAlreadyActive) ret = ERROR_SUCCESS;
						if (ret != ERROR_SUCCESS && ret != iCloudLock && ret != iErrorSIM && ret != iErrorDemoPhone && ret != iErrorSelectCarrier)
						{
							printf("DeviceConfigurationFlags=unknown\n");
							int extocde = -1;
							String^ sPath = System::Environment::GetEnvironmentVariable("APSTHOME");

							sPath = Path::Combine(sPath, "tools\\winiMobileDevice\\ideviceactivation.exe");
							if (File::Exists(sPath))
							{
								String^ sParam = String::Format("activate -u {0}", udid);//Path::Combine(System::AppDomain::CurrentDomain->BaseDirectory, "Activation.exe")
								logItcli(sParam);
								CRunExe::runExe(sPath, sParam, extocde, 180 * 1000, true);
								logItcli(String::Format("Run ideviceactivation.exe to activate ret ={0}", extocde));
								
								if (extocde == 0 || extocde == iCloudLock) {
									ret = extocde;
								}
							}

						}
					}
					else
					{
						ret = iErrorAlreadyActive;
					}
					//done = true;
				}
				else
				{
					logItcli(String::Format("Fail to connect to device {0}, error={1}", udid, ret.ToString("X8")));
				}
			}
		}
	}
	iDeviceClass::stop();
	logItcli(String::Format("activeV1-- ret={0}", ret));
	return ret;

}

int iactive::do_active_Carrier(BOOL verbose, String^ sUrl, StringDictionary^ sd, BOOL bFirst)
{
	int ret = 87;
	//if (iDeviceClass::idDeviceReady(10 * 1000))
	{
		logItcli("do_active_Carrier call idDeviceReady api.", verbose);
		iDeviceClass *dc = iDeviceClass::getDeviceInstanceByUdid(iDeviceClass::curDevice.udid_current);//iDeviceClass.getDeviceInstanceByUdid(sudid);
		if (dc != NULL)
		{
			DateTime starttime = DateTime::Now;
			logItcli("do_active_Carrier call getDeviceInstanceByUdid api.", verbose);
			if ((ret = dc->connect()) == 0)
			{
				logItcli("do_active_Carrier call CreateActivationSessionInfo api.", verbose);
				CFTypeRef plCASI = dc->CreateActivationSessionInfo(&ret);
				dc->stopSession();
				if (ret == 0)
				{
					if (verbose)
					{
						CFShow(plCASI);
					}
					StringBuilder^ sbRet = gcnew StringBuilder();
					StringBuilder^ sbSessionInfo = gcnew StringBuilder();
					CFDataRef pdata = CFPropertyListCreateXMLData(NULL, plCASI);
					String^ aplist = gcnew String((char *)CFDataGetBytePtr(pdata), 0, CFDataGetLength(pdata));
					sbSessionInfo->Append(aplist);
					logItcli("do_active_Carrier call drmHandshake  api.  " + sbSessionInfo->ToString(), verbose);
					ret = do_session_app("https://albert.apple.com/deviceservices/drmHandshake", sbSessionInfo, sbRet, verbose);
					if (0 == ret && sbRet->Length > 0)
					{
						logItcli("do_active_Carrier call startSesstion  api.", verbose);
						sbRet->Insert(0, "<?xml version=\"1.0\" encoding=\"UTF - 8\"?>\r\n<!DOCTYPE plist PUBLIC \"-//Apple//DTD PLIST 1.0//EN\" \"http://www.apple.com/DTDs/PropertyList-1.0.dtd\">\r\n");
						if ((ret = dc->startSession()) == ERROR_SUCCESS)
						{
							if (verbose)
								logItcli(sbRet->ToString());

							const char* plistSessionData =
								(const char*)(Marshal::StringToHGlobalAnsi(sbRet->ToString()).ToPointer());
							CFDataRef buffer_as_data = CFDataCreate(CFAllocatorGetDefault(), (UInt8*)plistSessionData, strlen(plistSessionData));
							if (buffer_as_data == NULL)
							{
								logIt("failed to allocate CreateActivationSessionInfo return\n");
							}
							else
							{
								if (verbose) {
									CFShow(buffer_as_data);
								}
							}

							CFStringRef error = NULL;
							//CFPropertyListRef plistSession = CFPropertyListCreateFromXMLData(CFAllocatorGetDefault(), buffer_as_data, kCFPropertyListImmutable, &error);
							logItcli("do_active_Carrier call CreateActivationInfo  api.", verbose);
							CFTypeRef plCAI = dc->CreateActivationInfo(buffer_as_data, &ret);
							dc->stopSession();
							if (ret == ERROR_SUCCESS) {
								StringBuilder^ sbData = gcnew StringBuilder();
								StringBuilder^ sbHeader = gcnew StringBuilder();
								pdata = CFPropertyListCreateXMLData(NULL, plCAI);
								String^ ss = gcnew String((char *)CFDataGetBytePtr(pdata), 0, CFDataGetLength(pdata));
								sbSessionInfo->Append(aplist);

								XmlDocument^ doc = gcnew XmlDocument();
								doc->XmlResolver = nullptr;
								doc->LoadXml(ss);
								XmlNode^ n = doc->SelectSingleNode("/plist/dict");
								if (n != nullptr)
								{
									sd["activation-info"] = n->OuterXml;
								}

								ret = do_send_carrierstatus(Encoding::UTF8->GetBytes(ss), sbHeader, sbData, verbose);

							}
							else
							{
								logItcli(String::Format("do_active_Carrier CreateActivationInfo error = {0}.", ret));
							}
						}
						else
						{
							logItcli(String::Format("do_active_Carrier startsession error = {0}.", ret));
						}
					}
				}
				else
				{
					return ret;
				}
			}
			else
			{
				logItcli(String::Format("do_active_Carrier connect error = {0}.", ret));
			}
			if ((DateTime::Now - starttime).TotalSeconds > 5 && ret != 0)
			{
				ret = iErrorTimeout;
			}
		}
	}

	return ret;
}

int iactive::do_active_V4(BOOL verbose, String^ sUrl, StringDictionary^ sd, BOOL bFirst)
{
	int ret = 87;
	//if (iDeviceClass::idDeviceReady(10 * 1000))
	{
		logItcli("do_active_V4 call idDeviceReady api.", verbose);
		iDeviceClass *dc = iDeviceClass::getDeviceInstanceByUdid(iDeviceClass::curDevice.udid_current);//iDeviceClass.getDeviceInstanceByUdid(sudid);
		if (dc != NULL)
		{
			DateTime starttime = DateTime::Now;
			logItcli("do_active_V4 call getDeviceInstanceByUdid api.", verbose);
			if ((ret = dc->connect()) == 0)
			{
				logItcli("do_active_V4 call CreateActivationSessionInfo api.", verbose);
				CFTypeRef plCASI = dc->CreateActivationSessionInfo(&ret);
				dc->stopSession();
				if (ret == 0)
				{
					if (verbose)
					{
						CFShow(plCASI);
					}
					StringBuilder^ sbRet = gcnew StringBuilder();
					StringBuilder^ sbSessionInfo = gcnew StringBuilder();
					CFDataRef pdata = CFPropertyListCreateXMLData(NULL, plCASI);
					String^ aplist = gcnew String((char *)CFDataGetBytePtr(pdata), 0, CFDataGetLength(pdata));
					sbSessionInfo->Append(aplist);
					logItcli("do_active_V4 call drmHandshake  api.  "+sbSessionInfo->ToString(), verbose);
					ret = do_session_app("https://albert.apple.com/deviceservices/drmHandshake", sbSessionInfo, sbRet, verbose);
					if (0 == ret && sbRet->Length>0)
					{
						logItcli("do_active_V4 call startSesstion  api.", verbose);
						sbRet->Insert(0, "<?xml version=\"1.0\" encoding=\"UTF - 8\"?>\r\n<!DOCTYPE plist PUBLIC \"-//Apple//DTD PLIST 1.0//EN\" \"http://www.apple.com/DTDs/PropertyList-1.0.dtd\">\r\n");
						if ((ret = dc->startSession()) == ERROR_SUCCESS)
						{
							if (verbose)
								logItcli(sbRet->ToString());

							const char* plistSessionData =
								(const char*)(Marshal::StringToHGlobalAnsi(sbRet->ToString()).ToPointer());
							CFDataRef buffer_as_data = CFDataCreate(CFAllocatorGetDefault(), (UInt8*)plistSessionData, strlen(plistSessionData));
							if (buffer_as_data == NULL)
							{
								logIt("failed to allocate CreateActivationSessionInfo return\n");
							}
							else
							{
								if (verbose){
									CFShow(buffer_as_data);
								}
							}

							CFStringRef error = NULL;
							//CFPropertyListRef plistSession = CFPropertyListCreateFromXMLData(CFAllocatorGetDefault(), buffer_as_data, kCFPropertyListImmutable, &error);
							logItcli("do_active_V4 call CreateActivationInfo  api.", verbose);
							CFTypeRef plCAI = dc->CreateActivationInfo(buffer_as_data, &ret);
							dc->stopSession();
							if (ret == ERROR_SUCCESS){
								StringBuilder^ sbData = gcnew StringBuilder();
								StringBuilder^ sbHeader = gcnew StringBuilder();
								pdata = CFPropertyListCreateXMLData(NULL, plCAI);
								String^ ss = gcnew String((char *)CFDataGetBytePtr(pdata), 0, CFDataGetLength(pdata));
								sbSessionInfo->Append(aplist);
								
								XmlDocument^ doc = gcnew XmlDocument();
								doc->XmlResolver = nullptr;
								doc->LoadXml(ss);
								XmlNode^ n = doc->SelectSingleNode("/plist/dict");
								if (n != nullptr)
								{
									sd["activation-info"] = n->OuterXml;
								}

								ret = do_send_active_Redirect(Encoding::UTF8->GetBytes(ss), sbHeader, sbData, verbose);
								if (ret != 0) return ret;
								if (ret == 0 && sbData->Length == 0) return ret;
								if ((ret = dc->startSession()) == 0)
								{
									const char* plDataData =
										(const char*)(Marshal::StringToHGlobalAnsi(sbData->ToString()).ToPointer());
									buffer_as_data = CFDataCreate(CFAllocatorGetDefault(), (UInt8*)plDataData, sbData->ToString()->Length);
									if (buffer_as_data == NULL)
									{
										logIt("failed to allocate CreateActivationSessionInfo return\n");
									}

									CFPropertyListRef plData = CFPropertyListCreateFromXMLData(CFAllocatorGetDefault(), buffer_as_data, kCFPropertyListImmutable, &error);
									if (verbose)
									{
										CFShow(plData);
									}
									
									const char* pldataheader =
										(const char*)(Marshal::StringToHGlobalAnsi(sbHeader->ToString()).ToPointer());
									buffer_as_data = CFDataCreate(CFAllocatorGetDefault(), (UInt8*)pldataheader, sbHeader->ToString()->Length);
									if (buffer_as_data == NULL)
									{
										logIt("failed to allocate CreateActivationSessionInfo return\n");
									}

									CFPropertyListRef aPlHeader = CFPropertyListCreateFromXMLData(CFAllocatorGetDefault(), buffer_as_data, kCFPropertyListImmutable, &error);
									if (verbose)
									{
										CFShow(aPlHeader);
									}
									
									ret = dc->ActivateWithOptions(plData, aPlHeader);
									logItcli(String::Format("do_active_V3 ActivateWithOptions error = {0}.", ret));
									dc->stopSession();
								}
							}
							else
							{
								logItcli(String::Format("do_active_V4 CreateActivationInfo error = {0}.", ret));
							}
						}
						else
						{
							logItcli(String::Format("do_active_V3 startsession error = {0}.", ret));
						}
					}
				}
				else
				{
					return ret;
				}
			}
			else
			{
				logItcli(String::Format("do_active_V4 connect error = {0}.", ret));
			}
			if ((DateTime::Now - starttime).TotalSeconds > 5 && ret != 0)
			{
				ret = iErrorTimeout;
			}
		}
	}

	return ret;
}

int iactive::do_active_V3(BOOL verbose, String^ sUrl, StringDictionary^ sd, BOOL bFirst)
{
	int ret = 87;
	//if (iDeviceClass::idDeviceReady(10 * 1000))
	{
		logItcli("do_active_V3 call idDeviceReady api.", verbose);
		iDeviceClass *dc = iDeviceClass::getDeviceInstanceByUdid(iDeviceClass::curDevice.udid_current);//iDeviceClass.getDeviceInstanceByUdid(sudid);
		if (dc != NULL)
		{
			DateTime starttime = DateTime::Now;
			logItcli("do_active_V3 call getDeviceInstanceByUdid api.", verbose);
			if ((ret = dc->connect()) == 0)
			{
				logItcli("do_active_V3 call CreateActivationSessionInfo api.", verbose);
				CFTypeRef plCASI = dc->CreateActivationSessionInfo(&ret);
				dc->stopSession();
				if (ret == 0)
				{
					if (verbose)
					{
						CFShow(plCASI);
					}
					StringBuilder^ sbRet = gcnew StringBuilder();
					StringBuilder^ sbSessionInfo = gcnew StringBuilder();
					CFDataRef pdata = CFPropertyListCreateXMLData(NULL, plCASI);
					String^ aplist = gcnew String((char *)CFDataGetBytePtr(pdata), 0, CFDataGetLength(pdata));
					sbSessionInfo->Append(aplist);
					logItcli("do_active_V3 call drmHandshake  api.  " + sbSessionInfo->ToString(), verbose);
					ret = do_session_app("https://albert.apple.com/deviceservices/drmHandshake", sbSessionInfo, sbRet, verbose);
					if (0 == ret && sbRet->Length>0)
					{
						logItcli("do_active_V3 call startSesstion  api.", verbose);
						sbRet->Insert(0, "<?xml version=\"1.0\" encoding=\"UTF - 8\"?>\r\n<!DOCTYPE plist PUBLIC \"-//Apple//DTD PLIST 1.0//EN\" \"http://www.apple.com/DTDs/PropertyList-1.0.dtd\">\r\n");
						if ((ret = dc->startSession()) == ERROR_SUCCESS)
						{
							if (verbose)
								logItcli(sbRet->ToString());

							const char* plistSessionData =
								(const char*)(Marshal::StringToHGlobalAnsi(sbRet->ToString()).ToPointer());
							CFDataRef buffer_as_data = CFDataCreate(CFAllocatorGetDefault(), (UInt8*)plistSessionData, strlen(plistSessionData));
							if (buffer_as_data == NULL)
							{
								logIt("failed to allocate CreateActivationSessionInfo return\n");
							}
							else
							{
								if (verbose){
									CFShow(buffer_as_data);
								}
							}

							CFStringRef error = NULL;
							//CFPropertyListRef plistSession = CFPropertyListCreateFromXMLData(CFAllocatorGetDefault(), buffer_as_data, kCFPropertyListImmutable, &error);
							logItcli("do_active_V3 call CreateActivationInfo  api.", verbose);
							CFTypeRef plCAI = dc->CreateActivationInfo(buffer_as_data, &ret);
							dc->stopSession();
							if (ret == ERROR_SUCCESS){
								StringBuilder^ sbData = gcnew StringBuilder();
								StringBuilder^ sbHeader = gcnew StringBuilder();
								pdata = CFPropertyListCreateXMLData(NULL, plCAI);
								String^ ss = gcnew String((char *)CFDataGetBytePtr(pdata), 0, CFDataGetLength(pdata));
								sbSessionInfo->Append(aplist);

								XmlDocument^ doc = gcnew XmlDocument();
								doc->XmlResolver = nullptr;
								doc->LoadXml(ss);
								XmlNode^ n = doc->SelectSingleNode("/plist/dict");
								if (n != nullptr)
								{
									sd["activation-info"] = n->OuterXml;
								}

								ret = do_send_active_info(Encoding::UTF8->GetBytes(ss), sbHeader, sbData, verbose);
								if (ret != 0) return ret;
								if (ret == 0 && sbData->Length == 0) return ret;
								if ((ret = dc->startSession()) == 0)
								{
									const char* plDataData =
										(const char*)(Marshal::StringToHGlobalAnsi(sbData->ToString()).ToPointer());
									buffer_as_data = CFDataCreate(CFAllocatorGetDefault(), (UInt8*)plDataData, sbData->ToString()->Length);
									if (buffer_as_data == NULL)
									{
										logIt("failed to allocate CreateActivationSessionInfo return\n");
									}

									CFPropertyListRef plData = CFPropertyListCreateFromXMLData(CFAllocatorGetDefault(), buffer_as_data, kCFPropertyListImmutable, &error);
									if (verbose)
									{
										CFShow(plData);
									}

									const char* pldataheader =
										(const char*)(Marshal::StringToHGlobalAnsi(sbHeader->ToString()).ToPointer());
									buffer_as_data = CFDataCreate(CFAllocatorGetDefault(), (UInt8*)pldataheader, sbHeader->ToString()->Length);
									if (buffer_as_data == NULL)
									{
										logIt("failed to allocate CreateActivationSessionInfo return\n");
									}

									CFPropertyListRef aPlHeader = CFPropertyListCreateFromXMLData(CFAllocatorGetDefault(), buffer_as_data, kCFPropertyListImmutable, &error);
									if (verbose)
									{
										CFShow(aPlHeader);
									}

									ret = dc->ActivateWithOptions(plData, aPlHeader);
									logItcli(String::Format("do_active_V3 ActivateWithOptions error = {0}.", ret));
									dc->stopSession();
								}
							}
							else
							{
								logItcli(String::Format("do_active_V3 CreateActivationInfo error = {0}.", ret));
							}
						}
						else
						{
							logItcli(String::Format("do_active_V3 startsession error = {0}.", ret));
						}
					}
				}
				else
				{
					return ret;
				}
			}
			else
			{
				logItcli(String::Format("do_active_V3 connect error = {0}.", ret));
			}
			if ((DateTime::Now - starttime).TotalSeconds > 5 && ret != 0)
			{
				ret = iErrorTimeout;
			}
		}
	}

	return ret;
}


int iactive::do_session_app(String^ sHost, StringBuilder^ sb, StringBuilder^ sbret, BOOL verbose)
{
	int ret = 87;

	Uri^ uri = gcnew Uri(sHost);//new Uri(new Uri(sHost), sUrlParam);

	logIt("do_session_app++ {0}",uri->ToString());
	bool bhttps = String::Compare(uri->Scheme, "https", true) == 0;
	if (bhttps)
	{
		ServicePointManager::SecurityProtocol = SecurityProtocolType::Tls12;
		ServicePointManager::ServerCertificateValidationCallback = gcnew RemoteCertificateValidationCallback(ValidateServerCertificate);
	}
	HttpWebRequest^ request = (HttpWebRequest^)WebRequest::Create(uri);
	request->Timeout = RequestTimeOut;
	request->CookieContainer = CContainer;
	request->ServicePoint->Expect100Continue = false;
	if (bhttps)
	{
		request->UseDefaultCredentials = true;
	}
	extern BOOL bNoProxy;
	if (bNoProxy)
	{
		request->Proxy = nullptr;
	}

	// "https://albert.apple.com/deviceservices/drmHandshake";
	request->Method = "POST";
	if (String::Compare(uri->Host, "albert.apple.com", true) != 0)
	{
		request->Headers->Add("X-FD-TARGET-URL", "https://albert.apple.com/deviceservices/drmHandshake");
	}
	//
	request->UserAgent = "iTunes/12.7 (Windows; Microsoft Windows 10 Professional Edition (Build 14393)) AppleWebKit/7604.1038.1006.6";
	request->Accept = "application/xml";
	request->ContentType = "application/x-apple-plist";
	request->Headers->Add("Accept-Language", "en-us");
	request->ContentLength = sb->Length;

	try
	{
		request->AllowAutoRedirect = false;

		request->AutomaticDecompression = DecompressionMethods::GZip | DecompressionMethods::Deflate;
		array<byte>^ data = Encoding::ASCII->GetBytes(sb->ToString());
		request->ContentLength = data->Length;
		Stream^ s = request->GetRequestStream();
		s->Write(data, 0, data->Length);

		HttpWebResponse^ res = (HttpWebResponse^)request->GetResponse();
		if (res->StatusCode == HttpStatusCode::Redirect ||
			res->StatusCode == HttpStatusCode::MovedPermanently)
		{
			String^ newUrl = res->GetResponseHeader("Location");
			logIt("Redirect2:{0}", newUrl);
			do_session_app(newUrl, sb, sbret, verbose);
			ret = iWaitTry;
		}
		String^ responseStr = "";
		try
		{
			StreamReader^ reader = gcnew StreamReader(res->GetResponseStream());
			{
				responseStr = reader->ReadToEnd();
			}
			reader->Close();
		}
		catch (Exception^ e)
		{
			logItcli(e->ToString());
		}

		finally
		{
			res->Close();
			s->Close();
		}

		if (verbose)
		{
			logItcli(String::Format("response: {0}", responseStr->ToString()), verbose);
		}
		if (!String::IsNullOrEmpty(responseStr))
		{
			sbret->Append(responseStr);
			ret = 0;
		}
	}
	catch (Exception^ ex)
	{
		logItcli(ex->ToString());
		ret = iErrorNetWork;
	}

	return ret;
}

int iactive::do_send_active_info_FMI(StringDictionary^ sd, BOOL verbose)
{
	int ret = 87; //Post addr can return 
	String^ url = "https://albert.apple.com/deviceservices/deviceActivation";

	if (String::IsNullOrEmpty(useName) || String::IsNullOrEmpty(passWord))
	{
		return iCloudLock;
	}

	Uri^ uri = gcnew Uri(url);//new Uri(new Uri(sHost), sUrlParam);

	logItcli(url);
	bool bhttps = String::Compare(uri->Scheme, "https", true) == 0;
	if (bhttps)
	{
		ServicePointManager::SecurityProtocol = SecurityProtocolType::Tls12;
		ServicePointManager::ServerCertificateValidationCallback = gcnew RemoteCertificateValidationCallback(ValidateServerCertificate);
	}
	HttpWebRequest^ request = (HttpWebRequest^)WebRequest::Create(uri);
	request->Timeout = RequestTimeOut;
REDIRECT:
	request->CookieContainer = CContainer;
	request->ServicePoint->Expect100Continue = false;
	if (bhttps)
	{
		request->UseDefaultCredentials = true;
	}
	extern BOOL bNoProxy;
	if (bNoProxy)
	{
		request->Proxy = nullptr;
	}
	/*如果是icloudlock, 可以发
	* Header:Content-Type: application/x-www-form-urlencoded
	* Referer: https://albert.apple.com/deviceservices/deviceActivation
	* Body中增加:
	* login=fd.qa.tester%40gmail.com&password=408Fdail&activation-info-base64=sb的base64
	* &isAuthRequired=true
	* 作为内容就可以了
	*/
	request->ContentType = String::Format("application/x-www-form-urlencoded");
	request->Method = "POST";
	if (String::Compare(uri->Host, "albert.apple.com", true) != 0)
	{
		request->Headers->Add("X-FD-TARGET-URL", "https://albert.apple.com/deviceservices/deviceActivation");
	}
	request->UserAgent = "iTunes/12.6.1 (Windows; Microsoft Windows 7 Ultimate Edition Service Pack 1 (Build 7601)) AppleWebKit/7603.1030.4006.6";
	request->Date = DateTime::Now;
	request->Headers->Add("Accept-Language", "en-us");
	//request->Headers->Add("X-Apple-Tz", "-28800");
	//request->Headers->Add("X-Apple-Store-Front", "143441-1,32");
	//request->Headers->Add("X-Apple-I-MD-RINFO", "17106176");//X-Apple-I-MD-RINFO: 17106176
	//request->Headers->Add("X-Apple-I-MD-M", "DCwB6JfhSuOKzDezgnCq6XiMF2AolMWO8qGA6xsxdr64+XWb2/YoXqt7+cFWFujszYDMrGSxZyXYJKfA");//??  X-Apple-I-MD-M: oNm4Ph0w4VPPSglk7eCPPIXJKHTqwxbv/wh+0ZrqTlwcfG19JdXm1i8zFonJ0LaXhZLXUIFlhWsU/iWn
	//request->Headers->Add("X-Apple-I-MD", "AAAABQAAABBeLqMidEAETaMd0YmS2CIEAAAAAg==");//?? X-Apple-I-MD: AAAABQAAABCGcPPHiCoyZsS01Ka8tRTQAAAAAg==
	//request->KeepAlive = TRUE;

	array<byte>^ httpbody = nullptr;

	/*
	POST /deviceservices/deviceActivation HTTP/1.1
	Host: albert.apple.com
	User-Agent: iTunes/12.7.1 (Windows; Microsoft Windows 10 Professional Edition (Build 15063)) AppleWebKit/7604.3005.2001.1
	Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*\/*; q=0.8
	Referer: https://albert.apple.com/deviceservices/deviceActivation
	Content-Type: application/x-www-form-urlencoded
	Origin: https://albert.apple.com
	Accept-Language: en-us
	Cookie: xp_ab=1#isj11bm+2365+17Eg4xa0; xp_abc=17Eg4xa0; xp_ci=3z46oGrWzAMz51WzBfQz8KM8NNP
	X-Apple-Store-Front: 143441-1,32
	X-Apple-I-MD-RINFO: 17106176
	X-Apple-Tz: -28800
	X-Apple-I-MD: AAAABQAAABBeLqMidEAETaMd0YmS2CIEAAAAAg==
	X-Apple-I-MD-M: DCwB6JfhSuOKzDezgnCq6XiMF2AolMWO8qGA6xsxdr64+XWb2/YoXqt7+cFWFujszYDMrGSxZyXYJKfA
	Connection: keep-alive
	Accept-Encoding: gzip, deflate
	Content-Length: 16208
	*/
	httpbody = getHttpBody3(fmiResponeItem);
	request->ContentLength = httpbody->Length;

	try
	{
		//C# PoST redirect POST become get why??
		request->AllowAutoRedirect = false;

		request->AutomaticDecompression = DecompressionMethods::GZip | DecompressionMethods::Deflate;
		Stream^ s = request->GetRequestStream();
		s->Write(httpbody, 0, httpbody->Length);

		HttpWebResponse^ res = (HttpWebResponse^)request->GetResponse();
		if (res->StatusCode == HttpStatusCode::Redirect ||
			res->StatusCode == HttpStatusCode::MovedPermanently)
		{
			String^ newUrl = res->GetResponseHeader("Location");
			res->Close();
			Sleep(100);
			logIt("Redirect3:{0}", newUrl);
			request = (HttpWebRequest^)WebRequest::Create(newUrl);
			request->Timeout = RequestTimeOut;
			goto REDIRECT;
			ret = iWaitTry;
		}
		String^ responseStr = "";
		try
		{
			StreamReader^ reader = gcnew StreamReader(res->GetResponseStream());
			{
				responseStr = reader->ReadToEnd();
			}
			reader->Close();
		}
		catch (Exception^ e)
		{
			logItcli(e->ToString());
			ret = 10061;
		}

		finally
		{
			res->Close();
			s->Close();
		}

		if (verbose)
		{
			logItcli(String::Format("response: {0}", responseStr->ToString()), verbose);
		}

		if (!String::IsNullOrEmpty(responseStr) && ret != iWaitTry)
		{
			StringBuilder^ sbRet = gcnew StringBuilder();
			ret = CheckActiveReturn(responseStr, url, sd, verbose, true);			
		}
	}
	catch (Exception^ ex)
	{
		logItcli(ex->ToString());
		ret = iErrorNetWork;
	}

	return ret;
}

int iactive::do_send_active_info_FMI(StringBuilder^ headers, StringBuilder^ dataList, BOOL verbose)
{
	int ret = 87; //Post addr can return 
	String^ url = "https://albert.apple.com/deviceservices/deviceActivation";

	if (String::IsNullOrEmpty(useName)||String::IsNullOrEmpty(passWord))
	{
		return iCloudLock;
	}

	Uri^ uri = gcnew Uri(url);//new Uri(new Uri(sHost), sUrlParam);

	logItcli(url);
	bool bhttps = String::Compare(uri->Scheme, "https", true) == 0;
	if (bhttps)
	{
		ServicePointManager::SecurityProtocol = SecurityProtocolType::Tls12;
		ServicePointManager::ServerCertificateValidationCallback = gcnew RemoteCertificateValidationCallback(ValidateServerCertificate);
	}
	HttpWebRequest^ request = (HttpWebRequest^)WebRequest::Create(uri);
	request->Timeout = RequestTimeOut;
REDIRECT:
	request->CookieContainer = CContainer;
	request->ServicePoint->Expect100Continue = false;
	if (bhttps)
	{
		request->UseDefaultCredentials = true;
	}
	extern BOOL bNoProxy;
	if (bNoProxy)
	{
		request->Proxy = nullptr;
	}
	/*如果是icloudlock, 可以发
	* Header:Content-Type: application/x-www-form-urlencoded
	* Referer: https://albert.apple.com/deviceservices/deviceActivation
	* Body中增加:
	* login=fd.qa.tester%40gmail.com&password=408Fdail&activation-info-base64=sb的base64
	* &isAuthRequired=true
	* 作为内容就可以了
	*/
	request->ContentType = String::Format("application/x-www-form-urlencoded");
	request->Method = "POST";
	if (String::Compare(uri->Host, "albert.apple.com", true) != 0)
	{
		request->Headers->Add("X-FD-TARGET-URL", "https://albert.apple.com/deviceservices/deviceActivation");
	}
	request->UserAgent = "iTunes/12.6.1 (Windows; Microsoft Windows 7 Ultimate Edition Service Pack 1 (Build 7601)) AppleWebKit/7603.1030.4006.6";
	request->Date = DateTime::Now;
	request->Headers->Add("Accept-Language", "en-us");
	//request->Headers->Add("X-Apple-Tz", "-28800");
	//request->Headers->Add("X-Apple-Store-Front", "143441-1,32");
	//request->Headers->Add("X-Apple-I-MD-RINFO", "17106176");//X-Apple-I-MD-RINFO: 17106176
	//request->Headers->Add("X-Apple-I-MD-M", "DCwB6JfhSuOKzDezgnCq6XiMF2AolMWO8qGA6xsxdr64+XWb2/YoXqt7+cFWFujszYDMrGSxZyXYJKfA");//??  X-Apple-I-MD-M: oNm4Ph0w4VPPSglk7eCPPIXJKHTqwxbv/wh+0ZrqTlwcfG19JdXm1i8zFonJ0LaXhZLXUIFlhWsU/iWn
	//request->Headers->Add("X-Apple-I-MD", "AAAABQAAABBeLqMidEAETaMd0YmS2CIEAAAAAg==");//?? X-Apple-I-MD: AAAABQAAABCGcPPHiCoyZsS01Ka8tRTQAAAAAg==
	//request->KeepAlive = TRUE;

	array<byte>^ httpbody = nullptr;

	/*
	POST /deviceservices/deviceActivation HTTP/1.1
	Host: albert.apple.com
	User-Agent: iTunes/12.7.1 (Windows; Microsoft Windows 10 Professional Edition (Build 15063)) AppleWebKit/7604.3005.2001.1
	Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*\/*; q=0.8
	Referer: https://albert.apple.com/deviceservices/deviceActivation
	Content-Type: application/x-www-form-urlencoded
	Origin: https://albert.apple.com
	Accept-Language: en-us
	Cookie: xp_ab=1#isj11bm+2365+17Eg4xa0; xp_abc=17Eg4xa0; xp_ci=3z46oGrWzAMz51WzBfQz8KM8NNP
	X-Apple-Store-Front: 143441-1,32
	X-Apple-I-MD-RINFO: 17106176
	X-Apple-Tz: -28800
	X-Apple-I-MD: AAAABQAAABBeLqMidEAETaMd0YmS2CIEAAAAAg==
	X-Apple-I-MD-M: DCwB6JfhSuOKzDezgnCq6XiMF2AolMWO8qGA6xsxdr64+XWb2/YoXqt7+cFWFujszYDMrGSxZyXYJKfA
	Connection: keep-alive
	Accept-Encoding: gzip, deflate
	Content-Length: 16208
	*/
	httpbody = getHttpBody3(fmiResponeItem);
	request->ContentLength = httpbody->Length;

	try
	{
		request->AllowAutoRedirect = false;

		request->AutomaticDecompression = DecompressionMethods::GZip | DecompressionMethods::Deflate;
		Stream^ s = request->GetRequestStream();
		s->Write(httpbody, 0, httpbody->Length);

		HttpWebResponse^ res = (HttpWebResponse^)request->GetResponse();
		if (res->StatusCode == HttpStatusCode::Redirect ||
			res->StatusCode == HttpStatusCode::MovedPermanently)
		{
			String^ newUrl = res->GetResponseHeader("Location");
			res->Close();
			Sleep(100);
			logIt("Redirect4:{0}", newUrl);
			request = (HttpWebRequest^)WebRequest::Create(newUrl);
			request->Timeout = RequestTimeOut;
			goto REDIRECT;
			ret = iWaitTry;
		}
		String^ responseStr = "";
		try
		{
			headers->Clear();
			headers->AppendLine("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
			headers->AppendLine("<!DOCTYPE plist PUBLIC \"-//Apple//DTD PLIST 1.0//EN\" \"http://www.apple.com/DTDs/PropertyList-1.0.dtd\">");
			headers->AppendLine("<plist version=\"1.0\">");
			headers->AppendLine("<dict>");
			headers->AppendLine("<key>ActivationResponseHeaders</key>");
			headers->AppendLine("<dict>");
			for (int i = 0; i < res->Headers->Count; i++)
			{
				headers->AppendLine(String::Format("<key>{0}</key>", res->Headers->GetKey(i)));
				headers->AppendLine(String::Format("<string>{0}</string>", res->Headers->Get(i)));
			}
			headers->AppendLine("</dict>");
			headers->AppendLine("</dict>");
			headers->AppendLine("</plist>");

			StreamReader^ reader = gcnew StreamReader(res->GetResponseStream());
			{
				responseStr = reader->ReadToEnd();
			}
			reader->Close();
			ret = 0;
		}
		catch (Exception^ e)
		{
			logItcli(e->ToString());
			ret = 10061;
		}

		finally
		{
			res->Close();
			s->Close();
		}

		if (verbose)
		{
			logItcli(String::Format("response: {0}", responseStr->ToString()), verbose);
		}

		if (!String::IsNullOrEmpty(responseStr) && ret != iWaitTry)
		{
			StringBuilder^ sbRet = gcnew StringBuilder();
			ret = CheckActiveReturnV1(responseStr, sbRet, verbose);
			if (ret != 0) return ret;
			if (sbRet->ToString()->IndexOf("iphone-activation") >= 0 && sbRet->ToString()->IndexOf("ack-received") >= 0)
			{
				ret = 0;
			}
			else
			{
				dataList->Clear();
				String^ retb64 = Convert::ToBase64String(Encoding::UTF8->GetBytes(sbRet->ToString()));
				dataList->AppendLine("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
				dataList->AppendLine("<!DOCTYPE plist PUBLIC \"-//Apple//DTD PLIST 1.0//EN\" \"http://www.apple.com/DTDs/PropertyList-1.0.dtd\">");
				dataList->AppendLine("<plist version=\"1.0\">");
				dataList->AppendLine("<data>");
				dataList->AppendLine(retb64);
				dataList->AppendLine("</data>");
				dataList->AppendLine("</plist>");
				ret = 0;
			}
		}
	}
	catch (Exception^ ex)
	{
		logItcli(ex->ToString());
		ret = iErrorNetWork;
	}

	return ret;
}

int iactive::do_send_active_info(array<byte>^ data, StringBuilder^ headers, StringBuilder^ dataList, BOOL verbose)
{
	int ret = 87;
	String^ url = "https://albert.apple.com/deviceservices/deviceActivation";

	String^ boundary = Guid::NewGuid().ToString("N");
	Uri^ uri = gcnew Uri(url);//new Uri(new Uri(sHost), sUrlParam);

	logItcli(url);
	bool bhttps = String::Compare(uri->Scheme, "https", true) == 0;
	if (bhttps)
	{
		ServicePointManager::SecurityProtocol = SecurityProtocolType::Tls12;
		ServicePointManager::ServerCertificateValidationCallback = gcnew RemoteCertificateValidationCallback(ValidateServerCertificate);
	}
	HttpWebRequest^ request = (HttpWebRequest^)WebRequest::Create(uri);
	request->Timeout = RequestTimeOut;
REDIRECT:
	request->CookieContainer = CContainer;
	request->ServicePoint->Expect100Continue = false;
	if (bhttps)
	{
		request->UseDefaultCredentials = true;
	}
	extern BOOL bNoProxy;
	if (bNoProxy)
	{
		request->Proxy = nullptr;
	}
	/*如果是icloudlock, 可以发
	* Header:Content-Type: application/x-www-form-urlencoded
	* Referer: https://albert.apple.com/deviceservices/deviceActivation
	* Body中增加:
	* login=fd.qa.tester%40gmail.com&password=408Fdail&activation-info-base64=sb的base64
	* &isAuthRequired=true
	* 作为内容就可以了
	*/
	request->ContentType = String::Format("multipart/form-data; boundary={0}", boundary);
	request->Method = "POST";
	if (String::Compare(uri->Host, "albert.apple.com", true) != 0)
	{
		request->Headers->Add("X-FD-TARGET-URL", "https://albert.apple.com/deviceservices/deviceActivation");
	}
	request->UserAgent = "iTunes/12.6.1 (Windows; Microsoft Windows 7 Ultimate Edition Service Pack 1 (Build 7601)) AppleWebKit/7603.1030.4006.6";
	request->Date = DateTime::Now;
	request->Headers->Add("Accept-Language", "en-us");
	//request->Headers->Add("X-Apple-Tz", "-28800");
	//request->Headers->Add("X-Apple-Store-Front", "143441-1,32");
	//request->Headers->Add("X-Apple-I-MD-RINFO", "17106176");//X-Apple-I-MD-RINFO: 17106176
	//request->Headers->Add("X-Apple-I-MD-M", "DCwB6JfhSuOKzDezgnCq6XiMF2AolMWO8qGA6xsxdr64+XWb2/YoXqt7+cFWFujszYDMrGSxZyXYJKfA");//??  X-Apple-I-MD-M: oNm4Ph0w4VPPSglk7eCPPIXJKHTqwxbv/wh+0ZrqTlwcfG19JdXm1i8zFonJ0LaXhZLXUIFlhWsU/iWn
	//request->Headers->Add("X-Apple-I-MD", "AAAABQAAABBeLqMidEAETaMd0YmS2CIEAAAAAg==");//?? X-Apple-I-MD: AAAABQAAABCGcPPHiCoyZsS01Ka8tRTQAAAAAg==
	//request->KeepAlive = TRUE;

	array<byte>^ httpbody = getHttpBody2(data, boundary);
	request->ContentLength = httpbody->Length;

	try
	{
		request->AllowAutoRedirect = false;

		request->AutomaticDecompression = DecompressionMethods::GZip | DecompressionMethods::Deflate;
		Stream^ s = request->GetRequestStream();
		s->Write(httpbody, 0, httpbody->Length);

		HttpWebResponse^ res = (HttpWebResponse^)request->GetResponse();
		if (res->StatusCode == HttpStatusCode::Redirect ||
			res->StatusCode == HttpStatusCode::MovedPermanently)
		{
			String^ newUrl = res->GetResponseHeader("Location");
			logIt("Redirect5:{0}", newUrl);
			//do_send_active_info(data, headers, dataList, verbose);
			res->Close();
			Sleep(100);
			logIt("Redirect5:{0}", newUrl);
			request = (HttpWebRequest^)WebRequest::Create(newUrl);
			request->Timeout = RequestTimeOut;
			goto REDIRECT;
			ret = iWaitTry;
		}
		String^ responseStr = "";
		try
		{

			headers->AppendLine("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
			headers->AppendLine("<!DOCTYPE plist PUBLIC \"-//Apple//DTD PLIST 1.0//EN\" \"http://www.apple.com/DTDs/PropertyList-1.0.dtd\">");
			headers->AppendLine("<plist version=\"1.0\">");
			headers->AppendLine("<dict>");
			headers->AppendLine("<key>ActivationResponseHeaders</key>");
			headers->AppendLine("<dict>");
			for (int i = 0; i < res->Headers->Count; i++)
			{
				headers->AppendLine(String::Format("<key>{0}</key>", res->Headers->GetKey(i)));
				headers->AppendLine(String::Format("<string>{0}</string>", res->Headers->Get(i)));
			}
			headers->AppendLine("</dict>");
			headers->AppendLine("</dict>");
			headers->AppendLine("</plist>");

			StreamReader^ reader = gcnew StreamReader(res->GetResponseStream());
			{
				responseStr = reader->ReadToEnd();
			}
			reader->Close();

			ret = 0;
		}
		catch (Exception^ e)
		{
			logItcli(e->ToString());
			ret = 10061;
		}

		finally
		{
			res->Close();
			s->Close();
		}

		if (verbose)
		{
			logItcli(String::Format("response: {0}", responseStr->ToString()), verbose);
		}

		if (!String::IsNullOrEmpty(responseStr) && ret != iWaitTry)
		{
			StringBuilder^ sbRet = gcnew StringBuilder();
			ret = CheckActiveReturnV1(responseStr, sbRet, verbose);
			if (ret == iCloudLock)
			{
				try
				{
					if (do_send_active_info_FMI(headers, dataList, verbose) == ERROR_SUCCESS)
					{
						ret = ERROR_SUCCESS;
					}
					if (ret != ERROR_SUCCESS)
					{
						ret = iCloudLock;
					}
				}
				catch (...){}
			}
			if (ret != ERROR_SUCCESS) return ret;
			if (sbRet->ToString()->IndexOf("iphone-activation") >= 0 && sbRet->ToString()->IndexOf("ack-received") >= 0)
			{
				ret = ERROR_SUCCESS;
			}
			else
			{
				if (dataList->Length == 0)
				{
					String^ retb64 = Convert::ToBase64String(Encoding::UTF8->GetBytes(sbRet->ToString()));
					dataList->AppendLine("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
					dataList->AppendLine("<!DOCTYPE plist PUBLIC \"-//Apple//DTD PLIST 1.0//EN\" \"http://www.apple.com/DTDs/PropertyList-1.0.dtd\">");
					dataList->AppendLine("<plist version=\"1.0\">");
					dataList->AppendLine("<data>");
					dataList->AppendLine(retb64);
					dataList->AppendLine("</data>");
					dataList->AppendLine("</plist>");
					ret = ERROR_SUCCESS;
				}
			}
		}
	}
	catch (Exception^ ex)
	{
		logItcli(ex->ToString());
		ret = iErrorNetWork;
	}

	return ret;
}


array<byte>^  iactive::getHttpBody3(Dictionary<String^, String^>^ sdPostItems)
{
	StringBuilder^ sb = gcnew StringBuilder();
	if (sdPostItems != nullptr && sdPostItems->Count > 0)
	{
		for each(KeyValuePair<String^, String^>^  dd in sdPostItems){
			sb->AppendFormat("{0}={1}&", System::Web::HttpUtility::UrlEncode(dd->Key), System::Web::HttpUtility::UrlEncode(dd->Value));
		}
	}
	else
		return nullptr;

	sb->Remove(sb->Length - 1, 1);

	logItcli(sb->ToString());

	return Encoding::UTF8->GetBytes(sb->ToString());
}

array<byte>^  iactive::getHttpBody2(array<byte>^ data, String^ boundary)
{
	String^ c1 = "--" + boundary + "\r\n" +
		"Content-Disposition: form-data; name=\"InStoreActivation\"\r\n";

	String^ c2 = "false\r\n" +
		"--" + boundary + "\r\n" +
		"Content-Disposition: form-data; name=\"activation-info\"\r\n";

	String^ c3 = Encoding::ASCII->GetString(data)->Trim() + "\r\n" +
		"--" + boundary + "--\r\n";

	MemoryStream^ bodyStream = gcnew MemoryStream();
	BinaryWriter^ bodyWriter = gcnew BinaryWriter(bodyStream);
	bodyWriter->Write(Encoding::ASCII->GetBytes(c1));
	bodyWriter->Write(Encoding::ASCII->GetBytes("\r\n"));
	bodyWriter->Write(Encoding::ASCII->GetBytes(c2));
	bodyWriter->Write(Encoding::ASCII->GetBytes("\r\n"));
	bodyWriter->Write(Encoding::ASCII->GetBytes(c3));
	bodyWriter->Flush();

	array<byte>^ bbody = bodyStream->GetBuffer();
	String^ sbody = Encoding::ASCII->GetString(bbody);
	logIt(sbody);
	bodyWriter->Close();
	bodyStream->Close();

	return bbody;
}

int iactive::CheckActiveReturnV1(String^ s, StringBuilder^ retplist, BOOL verbose)
{
	int ret = 87;
	logItcli("CheckActiveReturnV1++");
	XmlDocument^ doc = gcnew XmlDocument();
	doc->XmlResolver = nullptr;
	bool bXml = true;
	try
	{
		XmlTextReader^ tr = gcnew XmlTextReader(gcnew StringReader(s));
		{
			try
			{
				tr->DtdProcessing = DtdProcessing::Ignore;
				tr->Namespaces = false;
				doc->Load(tr);
			}
			catch (Exception^)
			{
				bXml = false;
			}
			tr->Close();
		}
		logItcli("CheckActiveReturnV1: xml parser");
		if (doc->DocumentElement != nullptr && bXml)
		{
			XmlNode^ n = doc->SelectSingleNode("/Document/Protocol/plist");
			if (n != nullptr && n->HasChildNodes)
			{
				//ret = doReturnPlist(sUrl, verbose, infos, n.OuterXml, bFirst);
				retplist->Append(n->OuterXml);
				ret = 0;
			}
			else
			{
				bXml = false;
			}
		}
		else
			bXml = false;
		if (!bXml)
		{
			logItcli("CheckActiveReturnV1: html parser");
			HtmlAgilityPack::HtmlNode::ElementsFlags->Remove("form");
			HtmlAgilityPack::HtmlDocument^ htmldoc = gcnew HtmlAgilityPack::HtmlDocument();
			htmldoc->LoadHtml(s);
			//<script id="protocol" type="text/x-apple-plist"><plist version="1.0">
			//  <dict>
			//    <key>iphone-activation</key>
			//    <dict>
			//      <key>activation-record</key>
			//      <dict>
			//        <key>FairPlayKeyData</key>
			//        <data>LS0tLS1CRUdJTiBDT05UQUlORVItLS0tLQpBQUVBQVVPQTZtcmV1emZiT2FVSnFsVGNaOG56YWttSXIySENWVFUybWpsRHdTTC84aHNZZkVONFRtS3J2cmYzClNpajhrWlJVZGhUb1BCN2U0MmZId2pJNC9MZUxnWm01ZG43SXdPUlE0RDhvd21rQ0pqc1NPU1pZUlpKRGFtb04KS25Rd1hOYXpZMHFneTZsVG5kTE9tUVNvMGV2bGRZMFZIY1J4TW5aNDdFek40VVlmajNWQ3lxTHYwSVNmSEpoRApCd2poYWR0QW15amprY1hJU0llR3JwWGh4WFM5ZXVEOTNwU2FaNEZJRkhaUWZYS3YvZjNxSjJNWlpPWXhWK0ZmClJnVjVxVHlSR2tIZzk3UmUreFpXQUE3amhtU2JzMkQ0ZFpJOENEZGVVSE4xVXorRzdJdW5qaTBqbWFtOThLYnYKU1pLbnQ3TFlZUzVFMFdubUlIZVJNVEM2S1ZDeDB1bExSYTJ2YmJYSm9jMzJnQVVWR1ZyV01EWGVzS3dPc2FHZApYODE5Q2YrYnYzQmdub0lZREpiZWtSdXZyWlRDSmlpNVdyZ3laSGpkQ1hQZVFCUmJpdFRQSHhNSjZybzlQN0pJCmtLaXpGSk5aVHdEdnp3ZXNXYVlFOCtEWElZZm9lWlRhcFZrQWxIZFltWGJURmVLclJxeWw1d2trWWdwYnowUHYKU2NLSUpjLzBaNmZFa21pQ2g4S1pSaDdOdHg5eTJkaW91aGhvUThJNXk0VGVZa1VmcFZuOU5qSUlGRHpBcTZCYgozVGhGODRQNlNlaERMa3NQRDFSUGtLamNKZHRuV1NSZVFibzZ4WURzanUyaTVzalh5YnF1dnRRTGgwU3pHOEJYCkJlclJEWi9YRFVnR2RMem9OamN6YlNaMDlUTTlYOVNvK2kwbUk1WWZ1dFNKVEVVNlVmUzZ6b1VsbGxCcnJnMVEKMlNIa25RWFRENXhzUS8rZDBjckhtNDFoc1N4NmQ2VUI2R3NOSE42d1pxY2dEY205cytWUm9GcEh1VDZ0aEVUTQpwT1IrN0JINXFwUW9TTVhjSEpyMGdDT2RVbkFEVWRZOUZhV0ZPVXdFak54bEJzakZyRkR3SkVSd0VCblA5Rm9GCk83VTdwM3pLRW11MzY2YXI0cHIxRWNxckFuZUswbk9BMk9WcTFyWWh1eE84eWNiRlFuS2JHOUwvRkx2TTRTcTAKckRaTU9YaGUyUG5QVzYrKzJ0L2ZMY0ZMd3JZbmhCQ21JYzRaL3pUWHc5ZHlzNzJjY0hRbkJ6em16dzJObm4xNAplOFYxMlI1b29BT1JhNzNJdm5RYWoyYmxzR1J5WXZreWowMDZYZzBROExoRUFvMXBTNGYxc1VGRHpJekZ5UzM3CjdhMkcxNU5udUxVRVpURDVEMEdLQTQxNy9LYmxTYlNHM1I3TFkyMEpwZnBMUzVySXNOcUFsL3NBekdhMDArUVMKbkNCVGJweWZMa04yNUZUVGs4TTFJTDlFRGRNT1prdnp1V0tqckxweDlTOUZJUkdCRXFhT1QrVWIvMWVVcTh0cAplMGVIeHZrTmJyWkliZlk2WTVUdHl3bC9uQ1hmaEk0cUQvdE5nWm9yS0tGc2wxT0pZMGo2YXIvcHdDeVd2ZUFxCi9jSU1FdDRsbVlacFdpaVpsbjRJM2pacXU1L3hISjFpQzRrVlJ3QU85L3Y4U09xSUJvR0N2ZXpQcTNtcy9QQUMKTjF5aVdJUVFwUjkrTndKWGswRTM5NkpWRithQm9ST3FlSWt3eVlhZ3JXK1FQcFI2TnJ6b3U5enUxSXNvT3pLVgpxU2R0Z1lrbVlMZy9qa1dZbEJnb1p0dU1TbzMydnFGUkhrbms5OFp2bWJSZ0JldHR2ZmdPTWNkVDhhcTVHcThnClhvYTRnM2kwM0ttUGZ1T0tiNmY1TzhMd1hNTnQrYnpKRWh6bU5EK1hNbDRsdUU5Qm93YjE4QkJXc2R0Nk1GWE8KUmttM21jREZZelAxNkhpWnp5NEpKQ0taTnpTSktlcFMvZ0JsNE9oc3ZXa0hpYnliCi0tLS0tRU5EIENPTlRBSU5FUi0tLS0tCg==</data>
			//        <key>AccountTokenCertificate</key>
			//        <data>LS0tLS1CRUdJTiBDRVJUSUZJQ0FURS0tLS0tCk1JSURaekNDQWsrZ0F3SUJBZ0lCQWpBTkJna3Foa2lHOXcwQkFRVUZBREI1TVFzd0NRWURWUVFHRXdKVlV6RVQKTUJFR0ExVUVDaE1LUVhCd2JHVWdTVzVqTGpFbU1DUUdBMVVFQ3hNZFFYQndiR1VnUTJWeWRHbG1hV05oZEdsdgpiaUJCZFhSb2IzSnBkSGt4TFRBckJnTlZCQU1USkVGd2NHeGxJR2xRYUc5dVpTQkRaWEowYVdacFkyRjBhVzl1CklFRjFkR2h2Y21sMGVUQWVGdzB3TnpBME1UWXlNalUxTURKYUZ3MHhOREEwTVRZeU1qVTFNREphTUZzeEN6QUoKQmdOVkJBWVRBbFZUTVJNd0VRWURWUVFLRXdwQmNIQnNaU0JKYm1NdU1SVXdFd1lEVlFRTEV3eEJjSEJzWlNCcApVR2h2Ym1VeElEQWVCZ05WQkFNVEYwRndjR3hsSUdsUWFHOXVaU0JCWTNScGRtRjBhVzl1TUlHZk1BMEdDU3FHClNJYjNEUUVCQVFVQUE0R05BRENCaVFLQmdRREZBWHpSSW1Bcm1vaUhmYlMyb1BjcUFmYkV2MGQxams3R2JuWDcKKzRZVWx5SWZwcnpCVmRsbXoySkhZdjErMDRJekp0TDdjTDk3VUk3ZmswaTBPTVkwYWw4YStKUFFhNFVnNjExVApicUV0K25qQW1Ba2dlM0hYV0RCZEFYRDlNaGtDN1QvOW83N3pPUTFvbGk0Y1VkemxuWVdmem1XMFBkdU94dXZlCkFlWVk0d0lEQVFBQm80R2JNSUdZTUE0R0ExVWREd0VCL3dRRUF3SUhnREFNQmdOVkhSTUJBZjhFQWpBQU1CMEcKQTFVZERnUVdCQlNob05MK3Q3UnovcHNVYXEvTlBYTlBIKy9XbERBZkJnTlZIU01FR0RBV2dCVG5OQ291SXQ0NQpZR3UwbE01M2cyRXZNYUI4TlRBNEJnTlZIUjhFTVRBdk1DMmdLNkFwaGlkb2RIUndPaTh2ZDNkM0xtRndjR3hsCkxtTnZiUzloY0hCc1pXTmhMMmx3YUc5dVpTNWpjbXd3RFFZSktvWklodmNOQVFFRkJRQURnZ0VCQUY5cW1yVU4KZEErRlJPWUdQN3BXY1lUQUsrcEx5T2Y5ek9hRTdhZVZJODg1VjhZL0JLSGhsd0FvK3pFa2lPVTNGYkVQQ1M5Vgp0UzE4WkJjd0QvK2Q1WlFUTUZrbmhjVUp3ZFBxcWpubTlMcVRmSC94NHB3OE9OSFJEenhIZHA5NmdPVjNBNCs4CmFia29BU2ZjWXF2SVJ5cFhuYnVyM2JSUmhUekFzNFZJTFM2alR5Rll5bVplU2V3dEJ1Ym1taWdvMWtDUWlaR2MKNzZjNWZlREF5SGIyYnpFcXR2eDNXcHJsanRTNDZRVDVDUjZZZWxpblpuaW8zMmpBelJZVHh0UzZyM0pzdlpEaQpKMDcrRUhjbWZHZHB4d2dPKzdidFcxcEZhcjBaakY5L2pZS0tuT1lOeXZDcndzemhhZmJTWXd6QUc1RUpvWEZCCjRkK3BpV0hVRGNQeHRjYz0KLS0tLS1FTkQgQ0VSVElGSUNBVEUtLS0tLQo=</data>
			//        <key>DeviceCertificate</key>
			//        <data>LS0tLS1CRUdJTiBDRVJUSUZJQ0FURS0tLS0tCk1JSUM4ekNDQWx5Z0F3SUJBZ0lLQW1haTRZKzRSSEZPY2pBTkJna3Foa2lHOXcwQkFRVUZBREJhTVFzd0NRWUQKVlFRR0V3SlZVekVUTUJFR0ExVUVDaE1LUVhCd2JHVWdTVzVqTGpFVk1CTUdBMVVFQ3hNTVFYQndiR1VnYVZCbwpiMjVsTVI4d0hRWURWUVFERXhaQmNIQnNaU0JwVUdodmJtVWdSR1YyYVdObElFTkJNQjRYRFRFek1UQXpNREUzCk1qVTBObG9YRFRFMk1UQXpNREUzTWpVME5sb3dnWU14TFRBckJnTlZCQU1XSkRReE1FSTBOMEZETFVVeE9EUXQKTkRnek9DMDVOemsyTFVORU5EVXlNVU5DUkRZelJERUxNQWtHQTFVRUJoTUNWVk14Q3pBSkJnTlZCQWdUQWtOQgpNUkl3RUFZRFZRUUhFd2xEZFhCbGNuUnBibTh4RXpBUkJnTlZCQW9UQ2tGd2NHeGxJRWx1WXk0eER6QU5CZ05WCkJBc1RCbWxRYUc5dVpUQ0JuekFOQmdrcWhraUc5dzBCQVFFRkFBT0JqUUF3Z1lrQ2dZRUF1SThzQXVxZTNtNCsKV3kvcG9BT2o1dysvZDJkZDJJdUpnZEVRSmEzazR2WlRncUpObTlkVyt1ZzRNQURFZkdOSUhVMTdjVWhiMThrbwp5UjE2WEtjL3dHZDNaWFBKaTRlZHl5VnlocXh3c3c5cEtlUjgxY21pWFM0cHVmTWtyK2JmRE1vbzhpMFR5T0xkCnVlcHJ5aXBrRHlBaitGdUI5bXhCUG5GaHFkamtFT2tDQXdFQUFhT0JsVENCa2pBZkJnTlZIU01FR0RBV2dCU3kKL2lFalJJYVZhbm5WZ1NhT2N4RFlwMHlPZERBZEJnTlZIUTRFRmdRVUVGNFY0VHBOSVM4amp0NUVoK05LUG5weApBQTh3REFZRFZSMFRBUUgvQkFJd0FEQU9CZ05WSFE4QkFmOEVCQU1DQmFBd0lBWURWUjBsQVFIL0JCWXdGQVlJCkt3WUJCUVVIQXdFR0NDc0dBUVVGQndNQ01CQUdDaXFHU0liM1kyUUdDZ0lFQWdVQU1BMEdDU3FHU0liM0RRRUIKQlFVQUE0R0JBQUgrZEhyd040ZU5lVitVUnJzRklrMkZjWVlLb0hBdVY4YmFkVW9xTXphZEhVQU5ZcnMwTTNLZgpxRUxmcVUzQmpFTHY4M0FJaGdieDZ5Q09JUExMcURXSzNlRmNORllNRHBZajNqdHZpNEZaRy8zMzJrbTFhZEVpClhSdUJobTc0ekpacnB0OU1sZTZvSnhSZG5xeGJ4dHBVcVByUC9BVGEyM3FXaTVpQjRPSnUKLS0tLS1FTkQgQ0VSVElGSUNBVEUtLS0tLQo=</data>
			//        <key>AccountTokenSignature</key>
			//        <data>T89GgEAZuGxrKIpZbKGQ9pHDIORMERAbceEGbILSZOphGL1I3sT9DV2tbVP9y8HnOg+moTBzNt4cnUcGLaGv8w4RvUHGLp5ajdVCAAW5p2Uw3sJfSpV5V6Zjcaw91Fdg9/rz53OTFNbjOf04tPLEAInvFNYiUpEW4zpi5xNLIw0=</data>
			//        <key>AccountToken</key>
			//        <data>ewoJIkFjdGl2aXR5VVJMIiA9ICJodHRwczovL2FsYmVydC5hcHBsZS5jb20vZGV2aWNlc2VydmljZXMvYWN0aXZpdHkiOwoJIkFjdGl2YXRpb25SYW5kb21uZXNzIiA9ICI2QjhDRTJGNS04REJFLTRENDQtQUNEQS0yMzMyQTgxMUQwNzUiOwoJIlVuaXF1ZURldmljZUlEIiA9ICJhMWMyOTZiNzA3NTc4NWI2MmRhNmUyN2Y4YjA4Y2EwNTkxMjY5MmUyIjsKCSJBY3RpdmF0aW9uVGlja2V0IiA9ICJIekZPVUJRM0RpMmFMUmRWSVpid3RpdnFrYW50T2N0RmRoQkZ5Z1lZd2EreDIxc1JIcTlzZ0JZZEVSUEJzNzV1VStNaDF3QUFwNjk1c3RueXY0Zjc4S05OcXR2MUV1SjlvV0xaMjR1UmNqeEszVElsY05EYktKZUo3bjBRSkNocFBScy9kaXM3TCs1NHNjYWVRTXkrR21ua3RJb1ZONkR5bFhvME9EdFYvYmdqUGtoRFlVS091N0NaK01hL1J4ZlFIWXRSZG1RVlBaNGo2MFQ1R3YvQWV4SjQvajd6TTdLSkdYeEVxblEzK2liZThWWUNRN0pBSzFacDVwVXdhdko4UzdNd3U0aXhrUEh2bFhvTnV0L2U5WW5pQ1V3MUZEWHFMd3AyWnhNd3A0a3F4Vk12bFlBb2Q3cXlUOTFtOGpOV1dLU0tCZnlDY1ZRcnJwVEZLd2dNVXg4Z2RFRlZYeUhmcWNaMk1HaWxHamEzRktVTHkxQXhjVEZVaVpWRnQzSzFjY3hGVGNCWnMybXRDSEZqVE5jalZBK2JGeUJLNW5SMC9RMXhnaWlZcXhNdkZlVm5lMnlUYUZzclJwS3IwM2hHWHBHYVNBdngxTlVLN0hyLy8zSFFiazdnN2tyS1B2M014b0wrUm5nSTFpMWN1cWVtalFqS0hTbGtyTWZpQVUzTUdML1ZsUzJPZjRNS0lRcVpoUjZSOHFwNzVzRkhkb2RvWVJCeG0xaUsyQWlkR3B3NjlCMTlXV1ZKUHhXelB1RFhcbiI7CgkiUGhvbmVOdW1iZXJOb3RpZmljYXRpb25VUkwiID0gImh0dHBzOi8vYWxiZXJ0LmFwcGxlLmNvbS9XZWJPYmplY3RzL0FMVW5icmljay53b2Evd2EvcGhvbmVIb21lIjsKfQ==</data>
			//      </dict>
			//      <key>unbrick</key>
			//      <true/>
			//    </dict>
			//  </dict>
			//</plist></script>
			logItcli("CheckActiveReturnV1: selectsingleNode parser");
			HtmlAgilityPack::HtmlNode^ htmlNode = htmldoc->DocumentNode->SelectSingleNode("//*[@id=\"IPAJingleEndPointErrorPage\"]");
			HtmlAgilityPack::HtmlNode^ htmlNode1 = htmldoc->DocumentNode->SelectSingleNode("//*[@id=\"EndpointError\"]/h1");
			if (htmlNode != nullptr)
			{
				if (verbose)//
				{
					logItcli("[*#sourcemsg#*]=" + htmlNode->OuterHtml, verbose);
				}
				logItcli(String::Format("[*#message#*]={0}", Convert::ToBase64String(Encoding::UTF8->GetBytes(htmlNode->OuterHtml))), TRUE);
				ret = iErrorSIM;
				if (htmlNode->OuterHtml->IndexOf("Choose Your Carrier to Continue")>0){
					ret = iErrorSelectCarrier;
				}
				else if  (htmlNode->OuterHtml->IndexOf("Demo Registration")>0){
					ret = iErrorDemoPhone;
				}
				else if (htmlNode->OuterHtml->IndexOf("Success, your iPhone has been unlocked", StringComparison::CurrentCultureIgnoreCase) >= 0) {
					ret = iErrorAlreadyActive;
				}
				else if (htmlNode->OuterHtml->IndexOf("There is a problem with your iPhone") > 0) {
					ret = iErrorProblem;
				}
					
			}
			else if (htmlNode1 != nullptr) {
				String^ inntxt = htmlNode1->InnerText;
				if (verbose)//
				{
					logItcli("[*#sourcemsg#*]=" + inntxt, verbose);
				}
				if (!String::IsNullOrEmpty(inntxt)) {
					if (String::Compare(inntxt, "Choose Your Carrier to Continue", true) == 0) {
						ret = iErrorSelectCarrier;
					}
					else if (String::Compare(inntxt, "Demo Registration", true) == 0) {
						ret = iErrorDemoPhone;
					}
					else if (inntxt->IndexOf("Success, your iPhone has been unlocked", StringComparison::CurrentCultureIgnoreCase) >= 0) {
						ret = iErrorAlreadyActive;
					}
					else if (String::Compare(inntxt, "There is a problem with your iPhone", true) == 0) {
						ret = iErrorProblem;
					}
					else {
						ret = iErrorSIM;
					}
				}
			}
			else if ((htmlNode = htmldoc->DocumentNode->SelectSingleNode("//*[@id=\"protocol\" and @type=\"text/x-apple-plist\"]")) != nullptr)
			{
				//ret = doReturnPlist(sUrl, verbose, infos, htmlNode.InnerHtml, bFirst);
				String^ plistString = htmlNode->InnerHtml;
				String^ pat = "<key>DeviceConfigurationFlags</key>\\W*<string>(.*?)</string>";
				Regex^ r = gcnew Regex(pat, RegexOptions::IgnoreCase|RegexOptions::Multiline|RegexOptions::IgnorePatternWhitespace);
				Match^ m = r->Match(plistString);
				if (m->Success){
					printf("hasDeviceConfigurationFlags=1\n");
					char buff[1024] = { 0 };
					String2CharP(m->Groups[1]->Value, buff, 1024);
					printf("DeviceConfigurationFlags_Value=%s\n", buff);
					if (String::Compare(m->Groups[1]->Value, "0")!=0)
						printf("DeviceConfigurationFlags=1\n");
				}
				/*if (plistString->IndexOf("<key>DeviceConfigurationFlags</key>") > 0 && plistString->IndexOf("<string>0</string>")<0)
				{
					printf("DeviceConfigurationFlags=1\n");
				}*/
				retplist->Append(htmlNode->InnerHtml);
				ret = 0;
			}
			else if ((htmlNode = htmldoc->DocumentNode->SelectSingleNode("//input[@type=\"password\" and @name=\"password\"]")) != nullptr)
			{
				if (!String::IsNullOrEmpty(useName)&&!String::IsNullOrEmpty(passWord))
				{
					fmiResponeItem = gcnew Dictionary<String^, String^>();
					HtmlAgilityPack::HtmlNode^ auth_form = htmldoc->GetElementbyId("auth_form");
					for each(HtmlAgilityPack::HtmlNode^ node in auth_form->SelectNodes(".//input"))
					{
						HtmlAgilityPack::HtmlAttribute^ attrName = node->Attributes["name"];
						if (attrName!=nullptr)
						{
							if (String::Compare("login", attrName->Value, TRUE)==0)
							{
								fmiResponeItem[attrName->Value] = useName;
							}
							else if (String::Compare("password", attrName->Value, TRUE) == 0)
							{
								fmiResponeItem[attrName->Value] = passWord;
							}
							else
							{
								HtmlAgilityPack::HtmlAttribute^ attrValue = node->Attributes["value"];
								if (!String::IsNullOrEmpty(attrValue->Value))
								{
									fmiResponeItem[attrName->Value] = attrValue->Value;
								}
							}

						}
					}				
				}

				htmlNode = htmldoc->DocumentNode->SelectSingleNode("//section[@class=\"headline\"]");
				if (verbose)
				{
					logItcli("[*#sourcemsg#*]=" + htmlNode->OuterHtml, verbose);
				}
				logItcli(String::Format("[*#message#*]={0}", Convert::ToBase64String(Encoding::UTF8->GetBytes(htmlNode->OuterHtml))), TRUE);
				ret = iCloudLock;
			}

		}

	}
	catch (Exception^ ex)
	{
		logItcli(ex->ToString());
	}

	logItcli("CheckActiveReturnV1--");

	return ret;
}

//id="jingle-page-form
String^ iactive::ParseHTML_Refresh_Next(String^ ss)
{
	String^ url = "";
	HtmlAgilityPack::HtmlNode::ElementsFlags->Remove("form");
	HtmlAgilityPack::HtmlDocument^ htmldoc = gcnew HtmlAgilityPack::HtmlDocument();
	htmldoc->LoadHtml(ss);

	HtmlAgilityPack::HtmlNode^ htmlNode = htmldoc->DocumentNode->SelectSingleNode("//*[@id=\"jingle-page-form\"]");
	if (htmlNode != nullptr)
	{
		HtmlAgilityPack::HtmlAttribute^ attrName = htmlNode->Attributes["action"];
		if (attrName != nullptr)
		{
			url = attrName->Value;
		}
	}
	return url;
}

String^ iactive::ParseHTML_controllerConfig(String^ ss)
{
	String^ url = "";
	HtmlAgilityPack::HtmlNode::ElementsFlags->Remove("form");
	HtmlAgilityPack::HtmlDocument^ htmldoc = gcnew HtmlAgilityPack::HtmlDocument();
	htmldoc->LoadHtml(ss);

	HtmlAgilityPack::HtmlNode^ htmlNode = htmldoc->DocumentNode->SelectSingleNode("//*[@id=\"jingle-page-content\"]");
	if (htmlNode != nullptr)
	{
		for each(auto node in htmlNode->ChildNodes)
		{
			if (String::Compare(node->Name, "script", true) == 0)
			{
				String^ inntxt = node->InnerText;
				if (inntxt->IndexOf("window.controllerConfig")>=0)
				{
					int ns = inntxt->IndexOf("{");
					int ne = inntxt->IndexOf("};");
					String^ json = inntxt->Substring(ns, ne - ns+1);
					if (!String::IsNullOrEmpty(json))
					{
						Dictionary<String^, Object^>^ JSONObj = (gcnew JavaScriptSerializer())->Deserialize<Dictionary<String^, Object^>^>(json);
						if (JSONObj->ContainsKey("data"))
						{
							Dictionary<String^, Object^>^ data = (Dictionary<String^, Object^>^)JSONObj["data"];
							if (data->ContainsKey("data-url")){
								Dictionary<String^, Object^>^ dataurl = (Dictionary<String^, Object^>^)data["data-url"];
								if (dataurl->ContainsKey("delay")){
									int delay = Convert::ToInt32(dataurl["delay"]);
									Sleep(delay);
								}
								if (dataurl->ContainsKey("url")){
									url = dataurl["url"]->ToString();

								}
							}
						}

					}
					break;
				}
			}
		}
	}
	else
	{
		if (ss->IndexOf("window.controllerConfig") >= 0)
		{
			String^ sub = ss->Substring(ss->IndexOf("window.controllerConfig") + 23);
			if (!String::IsNullOrEmpty(sub))
			{
				int ns = sub->IndexOf("{");
				int ne = sub->IndexOf("};");
				String^ json = sub->Substring(ns, ne - ns + 1);
				if (!String::IsNullOrEmpty(json))
				{
					Dictionary<String^, Object^>^ JSONObj = (gcnew JavaScriptSerializer())->Deserialize<Dictionary<String^, Object^>^>(json);
					if (JSONObj->ContainsKey("data"))
					{
						Dictionary<String^, Object^>^ data = (Dictionary<String^, Object^>^)JSONObj["data"];
						if (data->ContainsKey("data-url")){
							Dictionary<String^, Object^>^ dataurl = (Dictionary<String^, Object^>^)data["data-url"];
							if (dataurl->ContainsKey("delay")){
								int delay = Convert::ToInt32(dataurl["delay"]);
								Sleep(delay);
							}
							if (dataurl->ContainsKey("url")){
								url = dataurl["url"]->ToString();

							}
						}
					}

				}
			}
		}
	}
	return url;
}

String^ iactive::transfer_encoding_chunked_recv(StreamReader^ reader)
{

	StringBuilder^ sb = gcnew StringBuilder();
	String^ str;
	int size = -1;
	int count = 0;
	try{
		if ((str = reader->ReadLine()) != nullptr){
			logIt("recv:" + str);
			Match^ match = Regex::Match(str, "^[a-fA-F0-9]+$");
			if (match->Success){
				count = 0;
				size = Convert::ToInt32(str, 16);
				if (size == 0) return sb->ToString();
			}
			else{
				size = 0x7fffffff;
				sb->AppendLine(str);
			}
		}

		while ((str = reader->ReadLine()) != nullptr)
		{
			logIt("recv:" + str);
			if (size == -1)
			{
				count = 0;
				size = Convert::ToInt32(str, 16);
				if (size != 0) continue;
				else break;
			}
			if (size > 0)
			{
				count += str->Length;
				if (count == size)
				{
					sb->Append(str);
					size = -1;
				}
				else if (count < size){
					sb->AppendLine(str);
					count += 1; ////TODO: why is +1, maybe +2 
				}
				else{
					logItcli("transfer_encoding_chunked_recv has some problem.");
				}
			}
		}
	}
	catch (...){}
	
	return sb->ToString();
}

void fixCookies(HttpWebRequest^ request, HttpWebResponse^ response)
{
	for (int i = 0; i < response->Headers->Count; i++)
	{
		String^ name = response->Headers->GetKey(i);
		if (String::Compare(name,"Set-Cookie", true)!=0)
			continue;
		String^ value = response->Headers->Get(i);
		for each(auto singleCookie in value->Split(','))
		{
			Match^ match = Regex::Match(singleCookie, "(.+?)=(.+?);");
			if (!match->Success)
				continue;
			response->Cookies->Add(
				gcnew Cookie(
				match->Groups[1]->ToString(),
				match->Groups[2]->ToString(),
				"/",
				request->Host->Split(':')[0]));
		}
	}
}

String^ iactive::ParseHTML_controllerConfig(HtmlAgilityPack::HtmlDocument^ htmldoc)
{
	String^ url = "";
	HtmlAgilityPack::HtmlNode^ htmlNode = htmldoc->DocumentNode->SelectSingleNode("//*[@id=\"jingle-page-content\"]");
	if (htmlNode != nullptr)
	{
		HtmlAgilityPack::HtmlNode^ htmlScipt = htmlNode->SelectSingleNode("//*[@id=\"IPAJingleWaitPage\"]/script[1]/text()");
		if (htmlScipt != nullptr){
			String^ inntxt = htmlScipt->InnerText;
			logIt(inntxt);
			if (inntxt->IndexOf("window.controllerConfig") >= 0)
			{
				int ns = inntxt->IndexOf("{");
				int ne = inntxt->IndexOf("};");
				String^ json = inntxt->Substring(ns, ne - ns + 1);
				if (!String::IsNullOrEmpty(json))
				{
					Dictionary<String^, Object^>^ JSONObj = (gcnew JavaScriptSerializer())->Deserialize<Dictionary<String^, Object^>^>(json);
					if (JSONObj->ContainsKey("data"))
					{
						Dictionary<String^, Object^>^ data = (Dictionary<String^, Object^>^)JSONObj["data"];
						if (data->ContainsKey("data-url")){
							Dictionary<String^, Object^>^ dataurl = (Dictionary<String^, Object^>^)data["data-url"];
							if (dataurl->ContainsKey("delay")){
								int delay = Convert::ToInt32(dataurl["delay"]);
								Sleep(delay);
							}
							if (dataurl->ContainsKey("url")){
								url = dataurl["url"]->ToString();

							}
						}
					}

				}
			}

		}
	}
	return url;
}

int iactive::Parse_Error(HtmlAgilityPack::HtmlDocument^ htmldoc)
{
	int nret = ERROR_SUCCESS;
	HtmlAgilityPack::HtmlNode^ htmlTitle = htmldoc->DocumentNode->SelectSingleNode("/html/body/div/div/section/h1/span");
	if (htmlTitle != nullptr)
	{
		String^ inntxt = htmlTitle->InnerText;
		logIt(inntxt);
		if (String::Compare("Activation Lock", inntxt, true) == 0)
		{
			nret = iCloudLock;
		}
	}
	HtmlAgilityPack::HtmlNode^ htmlNode = htmldoc->DocumentNode->SelectSingleNode("//*[@id=\"IPAJingleEndPointErrorPage\"]/h1");
	if (htmlNode != nullptr)
	{
		HtmlAgilityPack::HtmlNode^ htmlErrorInfo = htmldoc->DocumentNode->SelectSingleNode("//*[@id=\"IPAJingleEndPointErrorPage\"]");
		if (htmlErrorInfo != nullptr)
		{
			if (verbose)
			{
				logItcli("[*#sourcemsg#*]=" + htmlErrorInfo->OuterHtml, verbose);
			}
			logItcli(String::Format("[*#message#*]={0}", Convert::ToBase64String(Encoding::UTF8->GetBytes(htmlErrorInfo->OuterHtml))), TRUE);
		}
		String^ inntxt = htmlNode->InnerText;
		logIt(inntxt);
		if (!String::IsNullOrEmpty(inntxt)){
			if (String::Compare(inntxt, "Choose Your Carrier to Continue", true) == 0){
				nret = iErrorSelectCarrier;
			}
			else if (String::Compare(inntxt, "Demo Registration", true) == 0){
				nret = iErrorDemoPhone;
			}
			else if (inntxt->IndexOf("Success, your iPhone has been unlocked", StringComparison::CurrentCultureIgnoreCase) >= 0) {
				nret = iErrorAlreadyActive;
			}
			else if (String::Compare(inntxt, "There is a problem with your iPhone", true) == 0) {
				nret = iErrorProblem;
			}
			else{
				nret = iErrorSIM;
			}
		}
	}
	else {
		HtmlAgilityPack::HtmlNode^ htmlNode1 = htmldoc->DocumentNode->SelectSingleNode("//*[@id=\"EndpointError\"]/h1");
		if (htmlNode1 != nullptr) {
			String^ inntxt = htmlNode1->InnerText;
			logIt(inntxt);
			if (!String::IsNullOrEmpty(inntxt)) {
				if (String::Compare(inntxt, "Choose Your Carrier to Continue", true) == 0) {
					nret = iErrorSelectCarrier;
				}
				else if (String::Compare(inntxt, "Demo Registration", true) == 0) {
					nret = iErrorDemoPhone;
				}
				else if (inntxt->IndexOf("Success, your iPhone has been unlocked", StringComparison::CurrentCultureIgnoreCase) >= 0) {
					nret = iErrorAlreadyActive;
				}
				else if (String::Compare(inntxt, "There is a problem with your iPhone", true) == 0) {
					nret = iErrorProblem;
				}
				else {
					nret = iErrorSIM;
				}
			}
		}
	}
	return nret;
}

String^ iactive::ParseHTML_PlistData(HtmlAgilityPack::HtmlDocument^ htmldoc)
{
	HtmlAgilityPack::HtmlNode^ htmlNode = htmldoc->DocumentNode->SelectSingleNode("//*[@id=\"protocol\" and @type=\"text/x-apple-plist\"]/text()");
	if (htmlNode != nullptr)
	{
		String^ inntxt = htmlNode->InnerText;
		logIt(inntxt);
		if (!String::IsNullOrEmpty(inntxt))
			return inntxt;
	}
	return "";
}


Tuple<int, String^, String^, String^>^ iactive::ParserHtml(String^ sHtml){
	HtmlAgilityPack::HtmlDocument^ htmldoc = gcnew HtmlAgilityPack::HtmlDocument();
	htmldoc->LoadHtml(sHtml);

	int ret = Parse_Error(htmldoc);
	logIt("parser error {0}", ret);
	if (ret != ERROR_SUCCESS){
		return gcnew Tuple<int, String^, String^, String^>(ret, "","","");
	}

	String^ plist = ParseHTML_PlistData(htmldoc);
	if (!String::IsNullOrEmpty(plist)){
		return gcnew Tuple<int, String^, String^, String^>(0, plist, "", "");
	}

	String^ url = ParseHTML_controllerConfig(htmldoc);
	if (!String::IsNullOrEmpty(url)){
		return gcnew Tuple<int, String^, String^, String^>(0, "", url, "");
	}

	HtmlAgilityPack::HtmlNode^ htmlNode = htmldoc->DocumentNode->SelectSingleNode("//*[@id=\"jingle-page-form\"]");
	StringBuilder ^sb = gcnew StringBuilder();
	if (htmlNode != nullptr)
	{
		HtmlAgilityPack::HtmlAttribute^ attrName = htmlNode->Attributes["action"];
		if (attrName != nullptr)
		{
			logItcli(attrName->Value);
			url = attrName->Value;
		}
	}
	HtmlAgilityPack::HtmlNode^ inputNode1 = htmldoc->DocumentNode->SelectSingleNode("//*[@id=\"IPAJingleServiceSwapSelectCarrierPage\"]/input[1]");
	if (inputNode1 != nullptr)
	{
		HtmlAgilityPack::HtmlAttribute^ attrName = inputNode1->Attributes["name"];
		if (attrName != nullptr)
		{
			logItcli(attrName->Value);
			sb->Append(HttpUtility::UrlEncode(attrName->Value))->Append("=");
		}
		HtmlAgilityPack::HtmlAttribute^ attrValue = inputNode1->Attributes["value"];
		if (attrValue != nullptr)
		{
			logItcli(attrValue->Value);
			sb->Append(HttpUtility::UrlEncode(attrValue->Value))->Append("&");
		}
	}
	HtmlAgilityPack::HtmlNode^ inputNode = htmldoc->DocumentNode->SelectSingleNode("//*[@id=\"jingle-page-buttons-right\"]/input");
	if (inputNode != nullptr)
	{
		HtmlAgilityPack::HtmlAttribute^ attrName = inputNode->Attributes["name"];
		if (attrName != nullptr)
		{
			logItcli(attrName->Value);
			sb->Append(HttpUtility::UrlEncode(attrName->Value))->Append("=");
		}
		HtmlAgilityPack::HtmlAttribute^ attrValue = inputNode->Attributes["value"];
		if (attrValue != nullptr)
		{
			logItcli(attrValue->Value);
			sb->Append(HttpUtility::UrlEncode(attrValue->Value));
		}
	}
	logIt(url);
	logIt(sb->ToString());
	return gcnew Tuple<int, String^, String^, String^>(0, "", url, sb->ToString());
}

String^ GetNewURL(String^ sReferer, String^ sPath){
	UriBuilder^ uri = gcnew UriBuilder(sReferer);
	String^ url = uri->Scheme + "://" + uri->Host + sPath;
	logIt(url);
	return url;
}

Tuple<int, String^, String^>^ iactive::postdatatoServer(String^ url, String^ postdata, String^ referurl, StringBuilder^ headers){
	Uri^ uri = gcnew Uri(GetNewURL(referurl, url));
	HttpWebRequest^ request = (HttpWebRequest^)WebRequest::Create(uri);
	request->Timeout = RequestTimeOut;
	bool bhttps = String::Compare(uri->Scheme, "https", true) == 0;
	if (bhttps)
	{
		ServicePointManager::SecurityProtocol = SecurityProtocolType::Tls12;
		ServicePointManager::ServerCertificateValidationCallback = gcnew RemoteCertificateValidationCallback(ValidateServerCertificate);
	}
	request->CookieContainer = CContainer;
	request->ServicePoint->Expect100Continue = false;
	if (bhttps)
	{
		request->UseDefaultCredentials = true;
	}
	extern BOOL bNoProxy;
	if (bNoProxy)
	{
		request->Proxy = nullptr;
	}
	request->KeepAlive = true;
	request->Method = "POST";
	request->ContentType = "application/x-www-form-urlencoded";
	request->UserAgent = iactive::UserAgent;
	request->Date = DateTime::Now;
	request->Headers->Add("Accept-Language", "en-us");
	request->Headers->Add("X-Apple-Tz", "-28800");
	request->Headers->Add("X-Apple-Store-Front", "143441-1,32");
	request->Headers->Add("X-Apple-I-MD-RINFO", "17106176");//X-Apple-I-MD-RINFO: 17106176
	request->Headers->Add("X-Apple-I-MD-M", "DCwB6JfhSuOKzDezgnCq6XiMF2AolMWO8qGA6xsxdr64+XWb2/YoXqt7+cFWFujszYDMrGSxZyXYJKfA");//??  X-Apple-I-MD-M: oNm4Ph0w4VPPSglk7eCPPIXJKHTqwxbv/wh+0ZrqTlwcfG19JdXm1i8zFonJ0LaXhZLXUIFlhWsU/iWn
	request->Headers->Add("X-Apple-I-MD", "AAAABQAAABBeLqMidEAETaMd0YmS2CIEAAAAAg==");//?? X-Apple-I-MD: AAAABQAAABCGcPPHiCoyZsS01Ka8tRTQAAAAAg==
	request->Referer = referurl;
	array<byte>^ httpbody = Encoding::ASCII->GetBytes(postdata);
	request->ContentLength = httpbody->Length;

	request->AutomaticDecompression = DecompressionMethods::GZip | DecompressionMethods::Deflate;
	Stream^ s = request->GetRequestStream();
	s->Write(httpbody, 0, httpbody->Length);

	HttpWebResponse^ res = (HttpWebResponse^)request->GetResponse();
	if (res->StatusCode != HttpStatusCode::OK)
	{
		logIt(res->StatusDescription);
		return gcnew Tuple<int, String^, String^>((int)res->StatusCode, "", "");
	}

	String^ responseStr = "";
	Tuple<int, String^, String^>^ dataret = gcnew Tuple<int, String^, String^>(10061, "", "");
	try
	{

		if (res->ContentType->IndexOf("text/html") >= 0){
			logIt("Get active or redirect html ");
		}
		else{
			logIt("need research new protocol.");
		}
		String^ headerTransferEncoding = res->GetResponseHeader("Transfer-Encoding");

		StreamReader^ reader = gcnew StreamReader(res->GetResponseStream());
		if (String::IsNullOrEmpty(headerTransferEncoding))
		{
			responseStr = reader->ReadToEnd();
		}
		else{
			responseStr = iactive::transfer_encoding_chunked_recv(reader);
		}
		reader->Close();

		StringWriter^ myWriter = gcnew StringWriter();
		HttpUtility::HtmlDecode(responseStr, myWriter);
		sReferer = request->RequestUri->ToString();
		logIt(request->RequestUri->AbsoluteUri);


		Tuple<int, String^, String^, String^>^ htmlret = ParserHtml(myWriter->ToString()); //0:error, 1:plist, 2:url, 3:postdata
		String^ urlpost = htmlret->Item3;
		//url and post data 
		if (!String::IsNullOrEmpty(urlpost) && !String::IsNullOrEmpty(htmlret->Item4))
		{
			dataret = postdatatoServer(urlpost, htmlret->Item4, sReferer, headers);
		}
		//only post url
		else if (!String::IsNullOrEmpty(urlpost) && String::IsNullOrEmpty(htmlret->Item4))
		{
			res->Close();
			s->Close();
			StringBuilder^ shtml = gcnew StringBuilder();
			Sleep(5000);
			urlpost = GetNewURL(sReferer, urlpost);
			int ret = http_send_poll(urlpost, headers, shtml, verbose);
			responseStr = shtml->ToString();
			dataret =  gcnew Tuple<int, String^, String^>(ret, responseStr, "");

		}
		else if (!String::IsNullOrEmpty(htmlret->Item2))
		{
			headers->AppendLine("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
			headers->AppendLine("<!DOCTYPE plist PUBLIC \"-//Apple//DTD PLIST 1.0//EN\" \"http://www.apple.com/DTDs/PropertyList-1.0.dtd\">");
			headers->AppendLine("<plist version=\"1.0\">");
			headers->AppendLine("<dict>");
			headers->AppendLine("<key>ActivationResponseHeaders</key>");
			headers->AppendLine("<dict>");
			for (int i = 0; i < res->Headers->Count; i++)
			{
				headers->AppendLine(String::Format("<key>{0}</key>", res->Headers->GetKey(i)));
				headers->AppendLine(String::Format("<string>{0}</string>", res->Headers->Get(i)));
			}
			headers->AppendLine("</dict>");
			headers->AppendLine("</dict>");
			headers->AppendLine("</plist>");
		}
		else{
			logIt("Active protocol need research.");
		}
	}
	catch (Exception^ e)
	{
		logItcli(e->ToString());
		responseStr = "";
		int ret = iErrorNetWork;
		dataret =  gcnew Tuple<int, String^, String^>(ret, "", "");
	}

	finally
	{
		res->Close();
		s->Close();
	}
	return dataret;
}

int iactive::do_send_carrierstatus(array<byte>^ data, StringBuilder^ headers, StringBuilder^ dataList, BOOL verbose)
{
	logIt("do_send_carrierstatus ++");
	int ret = 87;
	int nTryCount = 10;
	String^ url = "https://albert.apple.com/deviceservices/deviceActivation";

	String^ boundary = Guid::NewGuid().ToString("N");
	Uri^ uri = gcnew Uri(url);//new Uri(new Uri(sHost), sUrlParam);


	HttpWebRequest^ request = (HttpWebRequest^)WebRequest::Create(uri);
	request->Timeout = RequestTimeOut;
	logItcli(url);
	//CContainer->Add(gcnew Cookie("xp_ci", "3z1xUUmVzFGUz4eBzAVvzeiqElHqI","/",request->Host->Split(':')[0]));
REDIRECT:

	bool bhttps = String::Compare(uri->Scheme, "https", true) == 0;
	if (bhttps)
	{
		ServicePointManager::SecurityProtocol = SecurityProtocolType::Tls12;
		ServicePointManager::ServerCertificateValidationCallback = gcnew RemoteCertificateValidationCallback(ValidateServerCertificate);
	}
	request->CookieContainer = CContainer;
	request->ServicePoint->Expect100Continue = false;
	if (bhttps)
	{
		request->UseDefaultCredentials = true;
	}
	extern BOOL bNoProxy;
	if (bNoProxy)
	{
		request->Proxy = nullptr;
	}
	/*如果是icloudlock, 可以发
	* Header:Content-Type: application/x-www-form-urlencoded
	* Referer: https://albert.apple.com/deviceservices/deviceActivation
	* Body中增加:
	* login=fd.qa.tester%40gmail.com&password=408Fdail&activation-info-base64=sb的base64
	* &isAuthRequired=true
	* 作为内容就可以了
	*/
	request->ContentType = String::Format("multipart/form-data; boundary={0}", boundary->ToUpper());
	request->Method = "POST";
	//if (String::Compare(uri->Host, "albert.apple.com", true) != 0)
	//{
	//	request->Headers->Add("X-FD-TARGET-URL", "https://albert.apple.com/deviceservices/deviceActivation");
	//}
	request->UserAgent = iactive::UserAgent;
	request->Date = DateTime::Now;
	request->Headers->Add("Accept-Language", "en-us");
	request->Headers->Add("X-Apple-Tz", "-28800");
	request->Headers->Add("X-Apple-Store-Front", "143441-1,32");
	request->Headers->Add("X-Apple-I-MD-RINFO", "17106176");//X-Apple-I-MD-RINFO: 17106176
	request->Headers->Add("X-Apple-I-MD-M", "DCwB6JfhSuOKzDezgnCq6XiMF2AolMWO8qGA6xsxdr64+XWb2/YoXqt7+cFWFujszYDMrGSxZyXYJKfA");//??  X-Apple-I-MD-M: oNm4Ph0w4VPPSglk7eCPPIXJKHTqwxbv/wh+0ZrqTlwcfG19JdXm1i8zFonJ0LaXhZLXUIFlhWsU/iWn
	request->Headers->Add("X-Apple-I-MD", "AAAABQAAABBeLqMidEAETaMd0YmS2CIEAAAAAg==");//?? X-Apple-I-MD: AAAABQAAABCGcPPHiCoyZsS01Ka8tRTQAAAAAg==
	request->KeepAlive = FALSE;

	array<byte>^ httpbody = getHttpBody2(data, boundary->ToUpper());
	request->ContentLength = httpbody->Length;

	try
	{
		request->AllowAutoRedirect = false;

		request->AutomaticDecompression = DecompressionMethods::GZip | DecompressionMethods::Deflate;
		Stream^ s = request->GetRequestStream();
		s->Write(httpbody, 0, httpbody->Length);

		HttpWebResponse^ res = (HttpWebResponse^)request->GetResponse();
		if (res->StatusCode == HttpStatusCode::Redirect ||
			res->StatusCode == HttpStatusCode::MovedPermanently)
		{
			String^ newUrl = res->GetResponseHeader("Location");
			res->Close();
			Sleep(100);
			logIt("Redirect6:{0}", newUrl);
			request = (HttpWebRequest^)WebRequest::Create(newUrl);
			request->Timeout = RequestTimeOut;
			if (nTryCount-- > 0)
				goto REDIRECT;
			//	ret = iWaitTry;
		}

		if (res->StatusCode != HttpStatusCode::OK)
		{
			logIt(res->StatusDescription);
			return (int)res->StatusCode;
		}


		String^ responseStr = "";
		try
		{

			if (res->ContentType->IndexOf("text/html") >= 0) {
				logIt("Get active or redirect html ");
			}
			else {
				logIt("need research new protocol.");
			}
			String^ headerTransferEncoding = res->GetResponseHeader("Transfer-Encoding");

			StreamReader^ reader = gcnew StreamReader(res->GetResponseStream());
			if (String::IsNullOrEmpty(headerTransferEncoding))
			{
				responseStr = reader->ReadToEnd();
			}
			else {
				responseStr = iactive::transfer_encoding_chunked_recv(reader);
			}
			reader->Close();

			if (verbose)
			{
				logItcli(String::Format("response: {0}", responseStr), verbose);
			}

			StringWriter^ myWriter = gcnew StringWriter();
			HttpUtility::HtmlDecode(responseStr, myWriter);
			//   //*[@id="protocol"]/text()

			//    //*[@id="IPAJingleEndPointErrorPage"]/h1   Success, your iPhone has been unlocked.
			HtmlAgilityPack::HtmlDocument^ htmldoc = gcnew HtmlAgilityPack::HtmlDocument();
			htmldoc->LoadHtml(myWriter->ToString());
			HtmlAgilityPack::HtmlNode^ htmlNodePlist = htmldoc->DocumentNode->SelectSingleNode("//*[@id=\"protocol\"]/text()");
			if (htmlNodePlist!=nullptr)
			{ 
				HtmlAgilityPack::HtmlNode^ htmlNode = htmldoc->DocumentNode->SelectSingleNode("//*[@id=\"IPAJingleEndPointErrorPage\"]/h1");
				if (htmlNode == nullptr) {
					printf("CarrierLock=true\n");
					ret = ERROR_SUCCESS;
				}
				else {
					String^ ss = htmlNode->InnerText;
					Regex^ regex = gcnew Regex("^Success.*?been unlocked");
					Match^ match = regex->Match(ss);
					if (match->Success) {
						printf("CarrierLock=false\n");
						ret = ERROR_SUCCESS;
					}
					else {
						printf("CarrierLock=unkown\n");
						ret = ERROR_SUCCESS;
					}
				}
			}
			else {
				printf("CarrierLock=failed\n");
				ret = ERROR_FAIL_FAST_EXCEPTION;
			}
		}
		catch (Exception^ e)
		{
			logItcli(e->ToString());
			responseStr = "";
			ret = 10061;
		}

		finally
		{
			res->Close();
			s->Close();
		}
	}
	catch (Exception^ ex)
	{
		logItcli(ex->ToString());
		ret = 10062;
	}

	return ret;
}


int iactive::do_send_active_Redirect(array<byte>^ data, StringBuilder^ headers, StringBuilder^ dataList, BOOL verbose)
{
	int ret = 87;
	int nTryCount = 10;
	String^ url = "https://albert.apple.com/deviceservices/deviceActivation";

	String^ boundary = Guid::NewGuid().ToString("N");
	Uri^ uri = gcnew Uri(url);//new Uri(new Uri(sHost), sUrlParam);


	HttpWebRequest^ request = (HttpWebRequest^)WebRequest::Create(uri);
	logItcli(url);
	request->Timeout = RequestTimeOut;
	//CContainer->Add(gcnew Cookie("xp_ci", "3z1xUUmVzFGUz4eBzAVvzeiqElHqI","/",request->Host->Split(':')[0]));
REDIRECT:

	bool bhttps = String::Compare(uri->Scheme, "https", true) == 0;
	if (bhttps)
	{
		ServicePointManager::SecurityProtocol = SecurityProtocolType::Tls12;
		ServicePointManager::ServerCertificateValidationCallback = gcnew RemoteCertificateValidationCallback(ValidateServerCertificate);
	}
	request->CookieContainer = CContainer;
	request->ServicePoint->Expect100Continue = false;
	if (bhttps)
	{
		request->UseDefaultCredentials = true;
	}
	extern BOOL bNoProxy;
	if (bNoProxy)
	{
		request->Proxy = nullptr;
	}
	/*如果是icloudlock, 可以发
	* Header:Content-Type: application/x-www-form-urlencoded
	* Referer: https://albert.apple.com/deviceservices/deviceActivation
	* Body中增加:
	* login=fd.qa.tester%40gmail.com&password=408Fdail&activation-info-base64=sb的base64
	* &isAuthRequired=true
	* 作为内容就可以了
	*/
	request->ContentType = String::Format("multipart/form-data; boundary={0}", boundary->ToUpper());
	request->Method = "POST";
	//if (String::Compare(uri->Host, "albert.apple.com", true) != 0)
	//{
	//	request->Headers->Add("X-FD-TARGET-URL", "https://albert.apple.com/deviceservices/deviceActivation");
	//}
	request->UserAgent = iactive::UserAgent;
	request->Date = DateTime::Now;
	request->Headers->Add("Accept-Language", "en-us");
	request->Headers->Add("X-Apple-Tz", "-28800");
	request->Headers->Add("X-Apple-Store-Front", "143441-1,32");
	request->Headers->Add("X-Apple-I-MD-RINFO", "17106176");//X-Apple-I-MD-RINFO: 17106176
	request->Headers->Add("X-Apple-I-MD-M", "DCwB6JfhSuOKzDezgnCq6XiMF2AolMWO8qGA6xsxdr64+XWb2/YoXqt7+cFWFujszYDMrGSxZyXYJKfA");//??  X-Apple-I-MD-M: oNm4Ph0w4VPPSglk7eCPPIXJKHTqwxbv/wh+0ZrqTlwcfG19JdXm1i8zFonJ0LaXhZLXUIFlhWsU/iWn
	request->Headers->Add("X-Apple-I-MD", "AAAABQAAABBeLqMidEAETaMd0YmS2CIEAAAAAg==");//?? X-Apple-I-MD: AAAABQAAABCGcPPHiCoyZsS01Ka8tRTQAAAAAg==
	request->KeepAlive = FALSE;

	array<byte>^ httpbody = getHttpBody2(data, boundary->ToUpper());
	request->ContentLength = httpbody->Length;

	try
	{
		request->AllowAutoRedirect = false;

		request->AutomaticDecompression = DecompressionMethods::GZip | DecompressionMethods::Deflate;
		Stream^ s = request->GetRequestStream();
		s->Write(httpbody, 0, httpbody->Length);

		HttpWebResponse^ res = (HttpWebResponse^)request->GetResponse();
		if (res->StatusCode == HttpStatusCode::Redirect ||
			res->StatusCode == HttpStatusCode::MovedPermanently)
		{
			String^ newUrl = res->GetResponseHeader("Location");
			res->Close();
			Sleep(100);
			logIt("Redirect6:{0}", newUrl);
			request = (HttpWebRequest^)WebRequest::Create(newUrl);
			request->Timeout = RequestTimeOut;
			if (nTryCount-->0)
				goto REDIRECT;
		//	ret = iWaitTry;
		}

		if (res->StatusCode != HttpStatusCode::OK)
		{
			logIt(res->StatusDescription);
			return (int)res->StatusCode;
		}


		String^ responseStr = "";
		try
		{

			if (res->ContentType->IndexOf("text/html") >= 0){
				logIt("Get active or redirect html ");
			}
			else{
				logIt("need research new protocol.");
			}
			String^ headerTransferEncoding = res->GetResponseHeader("Transfer-Encoding");

			StreamReader^ reader = gcnew StreamReader(res->GetResponseStream());
			if (String::IsNullOrEmpty(headerTransferEncoding))
			{
				responseStr = reader->ReadToEnd();
			}
			else{
				responseStr = iactive::transfer_encoding_chunked_recv(reader);
			}
			reader->Close();

			StringWriter^ myWriter = gcnew StringWriter();
			HttpUtility::HtmlDecode(responseStr, myWriter);
			sReferer = request->RequestUri->ToString();
			logIt(request->RequestUri->AbsoluteUri);


			Tuple<int ,String^, String^, String^>^ htmlret = ParserHtml(myWriter->ToString()); // 0:plist, 1:url, 2:postdata
			
			//String^ urlpost = ParseHTML_controllerConfig(myWriter->ToString());
			//if (!String::IsNullOrEmpty(urlpost))
			String^ urlpost = htmlret->Item3;
			int erroract = htmlret->Item1;
			if (erroract != ERROR_SUCCESS){
				logIt("Apple Server return data not for activation.");
				ret = erroract;
			}
			//url and post data 
			else if (!String::IsNullOrEmpty(urlpost) && !String::IsNullOrEmpty(htmlret->Item4))
			{
				auto dataret = postdatatoServer(urlpost, htmlret->Item4, sReferer, headers);
				ret = dataret->Item1;
				responseStr = dataret->Item2;
			}
			//only post url
			else if (!String::IsNullOrEmpty(urlpost) && String::IsNullOrEmpty(htmlret->Item4))
			{
				res->Close();
				s->Close();
				StringBuilder^ shtml = gcnew StringBuilder();
				Sleep(5000);
				ret = http_send_poll(urlpost, headers, shtml, verbose);
				responseStr = shtml->ToString();
			}
			else if (!String::IsNullOrEmpty(htmlret->Item2))
			{
				headers->AppendLine("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
				headers->AppendLine("<!DOCTYPE plist PUBLIC \"-//Apple//DTD PLIST 1.0//EN\" \"http://www.apple.com/DTDs/PropertyList-1.0.dtd\">");
				headers->AppendLine("<plist version=\"1.0\">");
				headers->AppendLine("<dict>");
				headers->AppendLine("<key>ActivationResponseHeaders</key>");
				headers->AppendLine("<dict>");
				for (int i = 0; i < res->Headers->Count; i++)
				{
					headers->AppendLine(String::Format("<key>{0}</key>", res->Headers->GetKey(i)));
					headers->AppendLine(String::Format("<string>{0}</string>", res->Headers->Get(i)));
				}
				headers->AppendLine("</dict>");
				headers->AppendLine("</dict>");
				headers->AppendLine("</plist>");
			}
			else{
				logIt("Active protocol need research.");
			}
		}
		catch (Exception^ e)
		{
			logItcli(e->ToString());
			responseStr = "";
			ret = 10061;
		}

		finally
		{
			res->Close();
			s->Close();
		}

		if (verbose)
		{
			logItcli(String::Format("response: {0}", responseStr), verbose);
		}

		if (!String::IsNullOrEmpty(responseStr))
		{  
			StringBuilder^ sbRet = gcnew StringBuilder();
			if (responseStr->StartsWith("<plist version=")){
				sbRet->Append(responseStr);
			}
			else{
				ret = CheckActiveReturnV1(responseStr, sbRet, verbose);
				if (ret == iCloudLock)
				{
					try
					{
						if (do_send_active_info_FMI(headers, dataList, verbose) == ERROR_SUCCESS)
						{
							ret = ERROR_SUCCESS;
						}
						if (ret != ERROR_SUCCESS)
						{
							ret = iCloudLock;
						}
					}
					catch (...){}
				}
				if (ret != ERROR_SUCCESS) return ret;
			}
			//iphone-activation   idevice-activation
			if (sbRet->ToString()->IndexOf("-activation") >= 0 && sbRet->ToString()->IndexOf("ack-received") >= 0)
			{
				ret = ERROR_SUCCESS;
			}
			else
			{
				if (dataList->Length == 0)
				{
					String^ retb64 = Convert::ToBase64String(Encoding::UTF8->GetBytes(sbRet->ToString()));
					dataList->AppendLine("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
					dataList->AppendLine("<!DOCTYPE plist PUBLIC \"-//Apple//DTD PLIST 1.0//EN\" \"http://www.apple.com/DTDs/PropertyList-1.0.dtd\">");
					dataList->AppendLine("<plist version=\"1.0\">");
					dataList->AppendLine("<data>");
					dataList->AppendLine(retb64);
					dataList->AppendLine("</data>");
					dataList->AppendLine("</plist>");
					ret = ERROR_SUCCESS;
				}
			}
		}
	}
	catch (Exception^ ex)
	{
		logItcli(ex->ToString());
		ret = iErrorNetWork;
	}

	return ret;
}

int iactive::http_send_poll(String^ url, StringBuilder^ headers, StringBuilder^ shtml, BOOL verbose)
{
	/*
	POST /CSCustomer/direct-fulfill/poll HTTP/1.1
	Host: activateiphone8.apple.com
	User-Agent: iTunes/12.7.5 (Windows; Microsoft Windows 7 Professional Edition Service Pack 1 (Build 7601)) AppleWebKit/7605.2007.0.27 (dt:2)
	Content-Length: 2
	Accept: *\/*
	Content-Type: application/x-json-rpc
	Origin: https://activateiphone8.apple.com
	Accept-Language: en-us
	Cookie: JSESSIONID=14F17DA98F444F80B70DAABDB609B137; NSC_JOn0w00peqe5qh0dsyo4oadqpiuk3bs=6bbea3d11da514f2f0c1bcd34449f8dd6de1a6e59fefd591d4ee9e7e12cc009bc6b8c4b3; xp_ci=3z1xUUmVzFGUz4eBzAVvzeiqElHqI
	Referer: https://activateiphone8.apple.com/CSCustomer/direct-fulfill/handoff?ubtkn=2e72d7cd85a1ecb35f2cb5ad168417c5c88e8c6f00f29644abc55d9ba6879c1bb0772946dcd6ad96337bf961db5f4a09f47924c9ccc740af5c220535c3af2050a347fdba71d4e0e77b9a809cd9f174d8aa9d32e64fc4f6062bffa051908e3d7fecab65c26ed32e1d0cab17cff67203d4efa0fee8eab309ab4e1b1ed68b494ca16b4facc1d8f05c20bf5bc05bc15ae2e42ad469d26946f830a9746a360a393ac4-81e7d697ec963967-2decf19551b116698bb7ee44a1321814778787df
	X-Apple-I-MD-RINFO: 17106176
	X-Apple-Store-Front: 143441-1,32
	X-Apple-Tz: -18000
	X-Apple-I-MD: AAAABQAAABC3wu3THryHB+KMU9AhDQW0AAAAAg==
	X-Apple-I-MD-M: xtWf6zcZrxvFcEFcwjkFZKMpQTZkQqxc91bBbNmS0iCx0N176W5ouZZ2pasH8lp8I+MePRdkvVUoUz5N
	Accept-Encoding: gzip, deflate
	Connection: keep-alive

	{}
	*/

	logIt("http_send_poll++ {0}", url);
	int ret = 87;
	int nTryCount = 10;
RETRYDO:
	Uri^ uri = gcnew Uri(url);
	logIt(url);
	bool bhttps = String::Compare(uri->Scheme, "https", true) == 0;
	if (bhttps)
	{
		ServicePointManager::SecurityProtocol = SecurityProtocolType::Tls12;
		ServicePointManager::ServerCertificateValidationCallback = gcnew RemoteCertificateValidationCallback(ValidateServerCertificate);
	}

	HttpWebRequest^ request = (HttpWebRequest^)WebRequest::Create(uri);
	request->Timeout = RequestTimeOut;
	request->CookieContainer = CContainer;
	request->ServicePoint->Expect100Continue = false;
	if (bhttps)
	{
		request->UseDefaultCredentials = true;
	}
	extern BOOL bNoProxy;
	if (bNoProxy)
	{
		request->Proxy = nullptr;
	}

	request->ContentType = "application/x-json-rpc";
	request->Method = "POST";

	request->UserAgent = UserAgent;
	request->Headers->Add("Accept-Language", "en-us");
	request->Accept = "*/*";
	request->KeepAlive = true;
	request->Referer = sReferer;
	
	//request->Headers->Add("X-Apple-Tz", "-28800");
	//request->Headers->Add("X-Apple-Store-Front", "143441-1,32");
	//request->Headers->Add("X-Apple-I-MD-RINFO", "17106176");//X-Apple-I-MD-RINFO: 17106176
	//request->Headers->Add("X-Apple-I-MD-M", "DCwB6JfhSuOKzDezgnCq6XiMF2AolMWO8qGA6xsxdr64+XWb2/YoXqt7+cFWFujszYDMrGSxZyXYJKfA");//??  X-Apple-I-MD-M: oNm4Ph0w4VPPSglk7eCPPIXJKHTqwxbv/wh+0ZrqTlwcfG19JdXm1i8zFonJ0LaXhZLXUIFlhWsU/iWn
	//request->Headers->Add("X-Apple-I-MD", "AAAABQAAABBeLqMidEAETaMd0YmS2CIEAAAAAg==");//?? X-Apple-I-MD: AAAABQAAABCGcPPHiCoyZsS01Ka8tRTQAAAAAg==
	//request->KeepAlive = TRUE;
	//String^ newUrl;
	array<byte>^ httpbody = Encoding::UTF8->GetBytes("{}");
	request->ContentLength = httpbody->Length;
	try
	{
		request->AllowAutoRedirect = false;

		request->AutomaticDecompression = DecompressionMethods::GZip | DecompressionMethods::Deflate;
		Stream^ s = request->GetRequestStream();
		s->Write(httpbody, 0, httpbody->Length);

		HttpWebResponse^ res = (HttpWebResponse^)request->GetResponse();
		/*if (res->StatusCode == HttpStatusCode::Redirect ||
			res->StatusCode == HttpStatusCode::MovedPermanently)
		{
			newUrl = res->GetResponseHeader("Location");
			ret = iWaitTry;
		}*/

		if (res->StatusCode != HttpStatusCode::OK)
		{
			logIt("request fail:{0}  {1}", url, res->StatusDescription);
			return (int)res->StatusCode;
		}

		for each(Cookie^ cook in res->Cookies)
		{
			logIt("Cookie:");
			logIt("{0} = {1}", cook->Name, cook->Value);
			CContainer->Add(cook);
		}

		//Content-Type: application/xml
		//Transfer-Encoding: chunked

	

		if (res->ContentType->IndexOf("application/x-json-rpc")>=0){
			logIt("Get x-json-rpc ");
		}
		String^ headerTransferEncoding = res->GetResponseHeader("Transfer-Encoding");

		String^ responseStr = "";
		try
		{

			StreamReader^ reader = gcnew StreamReader(res->GetResponseStream());
			if (String::IsNullOrEmpty(headerTransferEncoding))
			{
				responseStr = reader->ReadToEnd();
			}
			else{
				responseStr = iactive::transfer_encoding_chunked_recv(reader);
			}
			reader->Close();
			logIt("http_send_poll return: {0}", responseStr);
			//{"location":"https://activateiphone8.apple.com/CSCustomer/direct-fulfill/refreshAction"}
			Dictionary<String^, Object^>^ JSONObj = (gcnew JavaScriptSerializer())->Deserialize<Dictionary<String^, Object^>^>(responseStr);
			if (JSONObj->ContainsKey("location"))
			{
				res->Close();
				ret = http_get_refreshAction(JSONObj["location"]->ToString(), headers, shtml, verbose);
			}
			else if (JSONObj->ContainsKey("data-url")){
				//[{"data-url":{"url":"https://activateiphone8.apple.com/CSCustomer/direct-fulfill/poll","delay":1000},"wait-page-processing-text":{"value":"We are processing your request. The page you have requested will load automatically."}} 
				Dictionary<String^, Object^>^ dataurl = (Dictionary<String^, Object^>^)JSONObj["data-url"];
				if (dataurl->ContainsKey("delay")){
					int delay = Convert::ToInt32(dataurl["delay"]);
					Sleep(delay);
				}
				if (dataurl->ContainsKey("url")){
					url = dataurl["url"]->ToString();
					if (nTryCount-- > 0)
						goto RETRYDO;
				}
			}
			else
			{
				logIt("can not parser refreshAction");
				ret = iErrorProtocol;
			}

		}
		catch (Exception^ e)
		{
			logItcli(e->ToString());
			ret = 10061;
		}
		finally
		{
			res->Close();
			s->Close();
		}		
	}
	catch (Exception^ ex)
	{
		logIt(ex->ToString());
		ret = iErrorNetWork;
	}
	return ret;
}

int iactive::http_get_refreshAction(String^ url, StringBuilder^ headers, StringBuilder^ shtml, BOOL verbose)
{
	/*
	GET /CSCustomer/direct-fulfill/refreshAction HTTP/1.1
	Host: activateiphone8.apple.com
	User-Agent: iTunes/12.7.5 (Windows; Microsoft Windows 7 Professional Edition Service Pack 1 (Build 7601)) AppleWebKit/7605.2007.0.27 (dt:2)
	Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*\/*;q=0.8
	Referer: https://activateiphone8.apple.com/CSCustomer/direct-fulfill/handoff?ubtkn=2e72d7cd85a1ecb35f2cb5ad168417c5c88e8c6f00f29644abc55d9ba6879c1bb0772946dcd6ad96337bf961db5f4a09f47924c9ccc740af5c220535c3af2050a347fdba71d4e0e77b9a809cd9f174d8aa9d32e64fc4f6062bffa051908e3d7fecab65c26ed32e1d0cab17cff67203d4efa0fee8eab309ab4e1b1ed68b494ca16b4facc1d8f05c20bf5bc05bc15ae2e42ad469d26946f830a9746a360a393ac4-81e7d697ec963967-2decf19551b116698bb7ee44a1321814778787df
	Accept-Language: en-us
	X-Apple-Store-Front: 143441-1,32
	X-Apple-I-MD-RINFO: 17106176
	X-Apple-Tz: -18000
	X-Apple-I-MD: AAAABQAAABC3wu3THryHB+KMU9AhDQW0AAAAAg==
	X-Apple-I-MD-M: xtWf6zcZrxvFcEFcwjkFZKMpQTZkQqxc91bBbNmS0iCx0N176W5ouZZ2pasH8lp8I+MePRdkvVUoUz5N
	Accept-Encoding: gzip, deflate
	Cookie: JSESSIONID=14F17DA98F444F80B70DAABDB609B137; NSC_JOn0w00peqe5qh0dsyo4oadqpiuk3bs=6bbea3d11da514f2f0c1bcd34449f8dd6de1a6e59fefd591d4ee9e7e12cc009bc6b8c4b3; xp_ci=3z1xUUmVzFGUz4eBzAVvzeiqElHqI
	Connection: keep-alive
	*/
	/*
	"https://activateiphone8.apple.com/CSCustomer/direct-fulfill/next"
	*/
	logIt("http_get_refreshAction++ {0}", url);
	int ret = 87;
	int nTryCount = 10;

RETRYACCESS:
	Uri^ uri = gcnew Uri(url);
	logIt(url);
	bool bhttps = String::Compare(uri->Scheme, "https", true) == 0;
	if (bhttps)
	{
		ServicePointManager::SecurityProtocol = SecurityProtocolType::Tls12;
		ServicePointManager::ServerCertificateValidationCallback = gcnew RemoteCertificateValidationCallback(ValidateServerCertificate);
	}

	HttpWebRequest^ request = (HttpWebRequest^)WebRequest::Create(uri);
	request->Timeout = RequestTimeOut;
	request->CookieContainer = CContainer;
	request->ServicePoint->Expect100Continue = false;
	if (bhttps)
	{
		request->UseDefaultCredentials = true;
	}
	extern BOOL bNoProxy;
	if (bNoProxy)
	{
		request->Proxy = nullptr;
	}

	request->Method = "GET";

	request->UserAgent = UserAgent;
	request->Headers->Add("Accept-Language", "en-us");
	request->Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";
	request->KeepAlive = true;
	request->Referer = sReferer;

	//request->Headers->Add("X-Apple-Tz", "-28800");
	//request->Headers->Add("X-Apple-Store-Front", "143441-1,32");
	//request->Headers->Add("X-Apple-I-MD-RINFO", "17106176");//X-Apple-I-MD-RINFO: 17106176
	//request->Headers->Add("X-Apple-I-MD-M", "DCwB6JfhSuOKzDezgnCq6XiMF2AolMWO8qGA6xsxdr64+XWb2/YoXqt7+cFWFujszYDMrGSxZyXYJKfA");//??  X-Apple-I-MD-M: oNm4Ph0w4VPPSglk7eCPPIXJKHTqwxbv/wh+0ZrqTlwcfG19JdXm1i8zFonJ0LaXhZLXUIFlhWsU/iWn
	//request->Headers->Add("X-Apple-I-MD", "AAAABQAAABBeLqMidEAETaMd0YmS2CIEAAAAAg==");//?? X-Apple-I-MD: AAAABQAAABCGcPPHiCoyZsS01Ka8tRTQAAAAAg==
	//request->KeepAlive = TRUE;
	//String^ newUrl;
	try
	{
		//C# redirect get is ok, POST become get
		request->AllowAutoRedirect = true;
		request->AutomaticDecompression = DecompressionMethods::GZip | DecompressionMethods::Deflate;
	
		HttpWebResponse^ res = (HttpWebResponse^)request->GetResponse();
		if (res->StatusCode != HttpStatusCode::OK)
		{
			return (int)res->StatusCode;
		}

		for each(Cookie^ cook in res->Cookies)
		{
			logIt("Cookie:");
			logIt("{0} = {1}", cook->Name, cook->Value);
		}

		//Content-Type: application/xml
		//Transfer-Encoding: chunked

		if (res->ContentType->IndexOf("text/html")>=0){
			logIt("Get active html ");
		}
		String^ headerTransferEncoding = res->GetResponseHeader("Transfer-Encoding");

		String^ responseStr = "";

		try{
			StreamReader^ reader = gcnew StreamReader(res->GetResponseStream());
			if (String::IsNullOrEmpty(headerTransferEncoding))
			{
				responseStr = reader->ReadToEnd();
			}
			else{
				responseStr = iactive::transfer_encoding_chunked_recv(reader);
			}
			reader->Close();
			logIt("http_active_html:" + responseStr);

			url= ParseHTML_Refresh_Next(responseStr);
			if (String::IsNullOrEmpty(url)) {
				url = ParseHTML_controllerConfig(responseStr);
				http_send_poll(url, headers, shtml, verbose);
			}
			if (!String::IsNullOrEmpty(url)){
				Console::WriteLine("sleeping 20");
				Sleep(20000);
				if (nTryCount-- > 0)
				goto RETRYACCESS;
			}


		}
		catch (Exception^ e)
		{
			logIt("url:{0} ex:{1}", url, e->ToString());
		}

		try
		{
			headers->AppendLine("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
			headers->AppendLine("<!DOCTYPE plist PUBLIC \"-//Apple//DTD PLIST 1.0//EN\" \"http://www.apple.com/DTDs/PropertyList-1.0.dtd\">");
			headers->AppendLine("<plist version=\"1.0\">");
			headers->AppendLine("<dict>");
			headers->AppendLine("<key>ActivationResponseHeaders</key>");
			headers->AppendLine("<dict>");
			for (int i = 0; i < res->Headers->Count; i++)
			{
				headers->AppendLine(String::Format("<key>{0}</key>", res->Headers->GetKey(i)));
				headers->AppendLine(String::Format("<string>{0}</string>", res->Headers->Get(i)));
			}
			headers->AppendLine("</dict>");
			headers->AppendLine("</dict>");
			headers->AppendLine("</plist>");


			
			shtml->Clear();
			shtml->Append(responseStr);
			ret = 0;
		}
		catch (Exception^ e)
		{
			logItcli(e->ToString());
			ret = 10061;
		}
		finally
		{
			res->Close();
		}
	}
	catch (Exception^ ex)
	{
		logIt(ex->ToString());
		ret = iErrorNetWork;
	}
	return ret;
}


