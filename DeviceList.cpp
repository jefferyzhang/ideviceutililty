#include "stdafx.h"
#include <iostream>
#include "DeviceList.h"
#include "iDeviceUtil.h"

//CDeviceList===========
CDeviceList::CDeviceList()
{
	InitializeCriticalSection(&m_CriticalSection);
}

CDeviceList::~CDeviceList()
{
	DeleteCriticalSection(&m_CriticalSection);
}


void CDeviceList::PrintList()
{
	map<STRING, HANDLE>::iterator iter;
	EnterCriticalSection(&m_CriticalSection);
	for (iter = device_list.begin(); !device_list.empty() && iter != device_list.end(); iter++)
	{
		logIt("list include: %s\n", iter->first.c_str());
		std::cout << iter->first << endl;
	}
	LeaveCriticalSection(&m_CriticalSection);
}

void CDeviceList::dump_device()
{
	ENTER_FUNC_BEGIN;
	map<STRING, HANDLE>::iterator iter;
	int i = 0;
	EnterCriticalSection(&m_CriticalSection);
	for (iter = device_list.begin(); !device_list.empty() && iter != device_list.end(); iter++)
	{
		logIt("%d: %s\t%08X\n", ++i, iter->first.c_str(), iter->second);
	}
	LeaveCriticalSection(&m_CriticalSection);
	ENTER_FUNC_BEGIN;
}

void CDeviceList::add_device(STRING device, HANDLE h)
{
	if (!check_device(device))
	{
		//device_list.insert(device_list.end(), device);
		EnterCriticalSection(&m_CriticalSection);
		device_list[device] = h;
		LeaveCriticalSection(&m_CriticalSection);
	}
}

HANDLE CDeviceList::get_device(STRING udid)
{
	HANDLE h = INVALID_HANDLE_VALUE;

	map<STRING, HANDLE>::const_iterator pos = device_list.find(udid);
	if (pos == device_list.end()) {
		//handle the error
	}
	else {
		h = pos->second;
	}

	return h;
}

STRING CDeviceList::getAnyOne()
{
	STRING sRet;
	map<STRING, HANDLE>::iterator iter;
	for (iter = device_list.begin(); !device_list.empty() && iter != device_list.end(); iter++)
	{
		sRet = iter->first;
		break;
	}

	return sRet;
}

BOOL CDeviceList::check_device(STRING device)
{
	BOOL found = FALSE;
	map<STRING, HANDLE>::const_iterator pos = device_list.find(device);
	found = (pos != device_list.end());
	//map<STRING, HANDLE>::iterator iter;
	//for (iter=device_list.begin(); !found && !device_list.empty() && iter!=device_list.end(); iter++)
	//{
	//	if (device.compare(iter->first)==0)
	//	{
	//		found=TRUE;
	//	}
	//}
	return found;
}

void CDeviceList::remove_device(STRING device)
{
	map<STRING, HANDLE>::iterator iter;
	EnterCriticalSection(&m_CriticalSection);
	for (iter = device_list.begin(); !device_list.empty() && iter != device_list.end(); iter++)
	{
		if (device.compare(iter->first) == 0)
		{
			device_list.erase(iter);
			break;
		}
	}
	LeaveCriticalSection(&m_CriticalSection);
}