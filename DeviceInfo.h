#pragma once

void logItcli(System::String^ s, BOOL b = false);
void logIt(System::String^ msg);
void logIt(
	System::String^ format,
	... cli::array<System::Object^>^ arg
	);

ref class DeviceInfo
{
public:
	static int getDeviceInfoBundle(System::Configuration::Install::InstallContext^ args);
	static int getDeviceInfoDetect(System::Configuration::Install::InstallContext^ args);
	static int getDeviceRecoveryInfo(System::Configuration::Install::InstallContext^ args);
	static int getDeviceInfo(System::Configuration::Install::InstallContext^ args);
	static int getDeviceANumber(System::Configuration::Install::InstallContext^ args);
};

