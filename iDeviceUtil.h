#pragma once
#include <map>
#include <list>
#include <CoreFoundation/CFBase.h>
#include "DeviceList.h"
using namespace std;

#define SAFE_DELETE(p) do{ if(NULL!=p){delete p; p = NULL;}} while(0)
#define SAFE_FREE(p) do{ if(NULL!=p){free(p); p = NULL;}} while(0)



//iCloud Lock Error
#define iCloudLock  90
//Sim Error
#define iErrorSIM  91
//Trust
#define iErrorTrust  92
//psw lock
#define iPSWLock  86
// activation fail
#define iActiveFail  93
// 
#define iWaitTry	94  
// 
#define iWelcomeScreen	95
// 
#define iHomeScreen	96
// deactiveScreen
#define iDeactiveScreen 97
//setup ui for active
#define iSetupScreen 98
//setup ui for active
#define iWelcomActiveScreen 99
// not support idevice version
#define iVersionNotSupport	100
//need research for new Protocol
#define iErrorProtocol		101
// select carrier to active
#define iErrorSelectCarrier 102
#define iErrorDemoPhone 103
#define iErrorProblem	104
#define iErrorNetWork	105

#define  iErrorTimeout  1460

#define iErrorAlreadyActive	1242
#define iErrorUnactived	1243


//public class ActiveErrorCode
//{
//	iCloud Lock Error
//	public static int iCloudLock = 90;
//	Sim Error
//	public static int iErrorSIM = 91;
//	Trust
//	public static int iErrorTrust = 92;
//	psw lock
//	public static int iPSWLock = 86;
//	 activation fail
//	public static int iActiveFail = 93;
//	 need wait some time and try
//	public static int iWaitTry = 94;
//	active timeout
//	public static int iErrorTimeout = 1460;
//
//}

#define kAppLookupKeyCFBundleIdentifier					"CFBundleIdentifier"
#define kAppLookupKeyApplicationDSID					"ApplicationDSID"
#define kAppLookupKeyApplicationType					"ApplicationType"
#define kAppLookupKeyCFBundleExecutable					"CFBundleExecutable"
#define kAppLookupKeyCFBundleDisplayName				"CFBundleDisplayName"
#define kAppLookupKeyCFBundleIconFile					"CFBundleIconFile"
#define kAppLookupKeyCFBundleName						"CFBundleName"
#define kAppLookupKeyCFBundleShortVersionString			"CFBundleShortVersionString"
#define kAppLookupKeyCFBundleSupportedPlatforms			"CFBundleSupportedPlatforms"
#define kAppLookupKeyCFBundleURLTypes					"CFBundleURLTypes"
#define kAppLookupKeyCFBundleVersion					"CFBundleVersion"
#define kAppLookupKeyCodeInfoIdentifier					"CodeInfoIdentifier"
#define kAppLookupKeyContainer							"Container"
#define kAppLookupKeyEntitlements						"Entitlements"
#define kAppLookupKeyHasSettingsBundle					"HasSettingsBundle"
#define kAppLookupKeyIsUpgradeable						"IsUpgradeable"
#define kAppLookupKeyMinimumOSVersion					"MinimumOSVersion"
#define kAppLookupKeyPath								"Path"
#define kAppLookupKeySignerIdentity						"SignerIdentity"
#define kAppLookupKeyUIDeviceFamily						"UIDeviceFamily"
#define kAppLookupKeyUIFileSharingEnabled				"UIFileSharingEnabled"
#define kAppLookupKeyUIStatusBarHidden					"UIStatusBarHidden"
#define kAppLookupKeyUISupportedInterfaceOrientations	"UISupportedInterfaceOrientations"
#define kAppLookupKeyCFBundlePackageType				"CFBundlePackageType"
#define kAppLookupKeyBuildMachineOSBuild				"BuildMachineOSBuild"
#define kAppLookupKeyCFBundleResourceSpecification		"CFBundleResourceSpecification"
#define kAppLookupKeyDTPlatformBuild					"DTPlatformBuild"
#define kAppLookupKeyDTCompiler							"DTCompiler"
#define kAppLookupKeyCFBundleSignature					"CFBundleSignature"
#define kAppLookupKeyDTSDKName							"DTSDKName"
#define kAppLookupKeyNSBundleResolvedPath				"NSBundleResolvedPath"
#define kAppLookupKeyDTXcode							"DTXcode"
#define kAppLookupKeyCFBundleInfoDictionaryVersion		"CFBundleInfoDictionaryVersion"
#define kAppLookupKeyDTXcodeBuild						"DTXcodeBuild"
#define kAppLookupKeyUIStatusBarTintParameters			"UIStatusBarTintParameters"
#define kAppLookupKeyDTPlatformVersion					"DTPlatformVersion"
#define kAppLookupKeyDTPlatformName						"DTPlatformName"
#define kAppLookupKeyCFBundleDevelopmentRegion			"CFBundleDevelopmentRegion"
#define kAppLookupKeyDTSDKBuild							"DTSDKBuild"


typedef struct _tagPARAMETERS_T
{
	_tagPARAMETERS_T(){
		deviceinfo.clear();
		bVerbose = FALSE;
		hEevntFound = NULL;
	};
	~_tagPARAMETERS_T()
	{
		if (hEevntFound != NULL)
		{
			CloseHandle(hEevntFound);
			hEevntFound = NULL;
		}
	}
	BOOL bVerbose;
	STRING device_udid;
	int error_code;
	STRING usb_device_serialnumber;
	map<STRING, STRING> deviceinfo;
	CDeviceList deivces;
	CDeviceList deivces_dfu;
	CDeviceList deivces_recovery;
	CDeviceList deivces_restore;
	HANDLE	hEevntFound;
	STRING  m_sPathMDSupport;
	STRING  m_sPathAASupport;
public:
	void SetQueryUdid(STRING s)
	{
		device_udid = s;
		if (hEevntFound == NULL)
		{
			hEevntFound = CreateEvent(NULL, TRUE, FALSE, NULL);
		}
	}
	DWORD WaitEventFound(int nTimeOut)
	{
		if (hEevntFound == NULL)
		{
			return ERROR_INVALID_HANDLE;
		}
		return WaitForSingleObject(hEevntFound, nTimeOut);
	}
}PARAMETERS_T, *PPARAMETERS_T;

extern PARAMETERS_T g_args;
//Log print
void logIt(TCHAR* fmt, ...);


TCHAR* GetThisPath(TCHAR* dest, size_t destSize);

#define ENTER_FUNC_BEGIN	logIt(_T("%s ++\n"),__FUNCTION__)
#define ENTER_FUNC_END		logIt(_T("%s --\n"),__FUNCTION__)
#define ENTER_FUNC_ENDRET(ret)		logIt(_T("%s -- return = %d\n"),__FUNCTION__, ret)


