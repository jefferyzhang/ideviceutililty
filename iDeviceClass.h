#pragma once
#include <CoreFoundation/CFDictionary.h>
#include <CoreFoundation/CFRunLoop.h>
typedef struct _tagCurrentDevice
{

	STRING udid_current;
	HANDLE ptr_curdev;
	_tagCurrentDevice()
	{
		ptr_curdev = NULL;
	}

}CurrentDevice, *PCurrentDevice;

DWORD WINAPI MyThreadProc(LPVOID pParam);

class  iDeviceClass
{
public:
	iDeviceClass();
	~iDeviceClass();
	iDeviceClass(HANDLE h);

	static CurrentDevice curDevice;
	static std::map<STRING, HANDLE> idevices;
	static bool start(STRING udid);
	static void stop();
	static HANDLE quitlisten;
	static CFRunLoopRef currentLoopRef;
	static iDeviceClass* getDeviceInstanceByPtr(HANDLE device);
	static iDeviceClass* getDeviceInstanceByUdid(STRING udid);
	static std::vector<STRING> getUdids();
	static STRING getDeviceUdid(HANDLE device);
	static bool idDeviceReady(int nTimeOut);
	static void waitDevice(HANDLE device);
	static void addDevices(HANDLE device);
private:
	HANDLE _device;
	HANDLE hAFC;
	HANDLE hService;
	bool wasAFC2 ;
public:
	STRING getUdid();
	bool checkTrust();
	int startSession();
	int connect();
	void disconnect();
	void stopSession();
	STRING copyValue(STRING key);
	STRING copyValue(STRING domain, STRING key);
	CFTypeRef copyOnlyValue(STRING domain, STRING key);
	int LookupApplications(CFDictionaryRef options, std::map<STRING, STRING> &AppBundles);

	STRING getFMiPhoneInfo(BOOL bReadANum);
	void GetInfoWithoutTrust();
	void AddInfoToList(std::vector<STRING> &ll, STRING sDomain, STRING sKey);
	std::vector<STRING> getDeviceInfo(BOOL bReadANum, BOOL bSingle);
	std::vector<STRING> getDeviceInfoDetect();
	std::vector<STRING> getDeviceANumber();
	BOOL IsJailbreak();
	std::vector<STRING> getDeviceInfoBundle();
	void AddESIMInfo(std::vector<STRING> &ll);

	long GetFileSize(STRING path);
	std::vector<STRING> GetSysInfo();
	int deactive();
	int active(std::list<CFStringRef> keys, std::list<CFTypeRef> values);

	CFTypeRef CreateActivationSessionInfo(int *errcode);
	int ActivateWithOptions(CFTypeRef pl_p, CFTypeRef pl_header);
	CFTypeRef CreateActivationInfoWithOptions(CFTypeRef pdata, int zero, int *errcode);
	CFTypeRef CreateActivationInfo(CFTypeRef pdata, int *errcode);

};

