/********************************************************************
created:	2015/09/28
created:	28:9:2015   10:04
filename: 	J:\Works\iDeviceUtilCore\iDeviceUtilCore\logit.cpp
file path:	J:\Works\iDeviceUtilCore\iDeviceUtilCore
file base:	logit
file ext:	cpp
author:		Jeffery Zhang

purpose:	Print Logs
*********************************************************************/
#include "stdafx.h"
#include "iDeviceUtil.h"

extern PARAMETERS_T g_args;
#include <atlstr.h>

void logIt(TCHAR* fmt, ...)
{
	va_list args;

	CString sLog;
	va_start(args, fmt);
	sLog.FormatV(fmt, args);
	va_end(args);
	CString sLogpid;
	sLogpid.Format(_T("[iDeviceUtility]:%s"),sLog);
	if (g_args.bVerbose)
	{
		_tprintf(sLogpid);
		fflush(stdout);
	}
	OutputDebugString(sLogpid);
}

#include <Shlwapi.h>
#pragma comment(lib, "shlwapi.lib")

TCHAR* GetThisPath(TCHAR* dest, size_t destSize)
{
	if (!dest) return NULL;

	DWORD length = GetModuleFileName(NULL, dest, destSize);
	if (MAX_PATH > destSize) return NULL;
	PathRemoveFileSpec(dest);
	return dest;
}